-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 04 2018 г., 22:30
-- Версия сервера: 10.1.35-MariaDB
-- Версия PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sm_user_1_data`
--

-- --------------------------------------------------------

--
-- Структура таблицы `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `type` enum('context','teaser','banner','popup','other') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `advertisements_targets`
--

CREATE TABLE `advertisements_targets` (
  `target_id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `original_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories_advertisements`
--

CREATE TABLE `categories_advertisements` (
  `exact` tinyint(1) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `modelable_id` int(10) UNSIGNED NOT NULL,
  `modelable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `main` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts_advertisements`
--

CREATE TABLE `posts_advertisements` (
  `include` tinyint(1) NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts_categories`
--

CREATE TABLE `posts_categories` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `trackers`
--

CREATE TABLE `trackers` (
  `id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL,
  `tracker_id` int(10) UNSIGNED NOT NULL,
  `utm_medium` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `utm_campaign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `utm_source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sid1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sid2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sid3` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `urls`
--

CREATE TABLE `urls` (
  `id` int(10) UNSIGNED NOT NULL,
  `advertisement_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exact` tinyint(1) NOT NULL,
  `exclude` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `advertisements_site_id_group_id_advertisement_id_type_unique` (`site_id`,`group_id`,`advertisement_id`,`type`),
  ADD KEY `advertisements_group_id_foreign` (`group_id`),
  ADD KEY `advertisements_site_id_advertisement_id_index` (`site_id`,`advertisement_id`),
  ADD KEY `advertisements_site_id_group_id_index` (`site_id`,`group_id`),
  ADD KEY `advertisements_site_id_index` (`site_id`),
  ADD KEY `advertisements_advertisement_id_index` (`advertisement_id`);

--
-- Индексы таблицы `advertisements_targets`
--
ALTER TABLE `advertisements_targets`
  ADD UNIQUE KEY `advertisements_targets_target_id_advertisement_id_unique` (`target_id`,`advertisement_id`),
  ADD KEY `advertisements_targets_advertisement_id_foreign` (`advertisement_id`),
  ADD KEY `advertisements_targets_target_id_advertisement_id_index` (`target_id`,`advertisement_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_original_id_site_id_unique` (`original_id`,`site_id`),
  ADD KEY `categories_original_id_site_id_index` (`original_id`,`site_id`),
  ADD KEY `categories_url_site_id_index` (`url`,`site_id`),
  ADD KEY `categories_site_id_index` (`site_id`),
  ADD KEY `categories_url_index` (`url`);

--
-- Индексы таблицы `categories_advertisements`
--
ALTER TABLE `categories_advertisements`
  ADD UNIQUE KEY `c_a_c_id_a_id_e_u` (`category_id`,`advertisement_id`,`exact`),
  ADD KEY `categories_advertisements_advertisement_id_foreign` (`advertisement_id`),
  ADD KEY `categories_advertisements_category_id_advertisement_id_index` (`category_id`,`advertisement_id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_modelable_id_modelable_type_index` (`modelable_id`,`modelable_type`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `positions_advertisement_id_index` (`advertisement_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_url_site_id_unique` (`url`,`site_id`),
  ADD KEY `posts_url_site_id_index` (`url`,`site_id`),
  ADD KEY `posts_site_id_index` (`site_id`),
  ADD KEY `posts_url_index` (`url`);

--
-- Индексы таблицы `posts_advertisements`
--
ALTER TABLE `posts_advertisements`
  ADD UNIQUE KEY `posts_advertisements_post_id_advertisement_id_unique` (`post_id`,`advertisement_id`),
  ADD KEY `posts_advertisements_advertisement_id_foreign` (`advertisement_id`),
  ADD KEY `posts_advertisements_post_id_advertisement_id_index` (`post_id`,`advertisement_id`);

--
-- Индексы таблицы `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD UNIQUE KEY `posts_categories_post_id_category_id_unique` (`post_id`,`category_id`),
  ADD KEY `posts_categories_category_id_foreign` (`category_id`),
  ADD KEY `posts_categories_post_id_category_id_index` (`post_id`,`category_id`);

--
-- Индексы таблицы `trackers`
--
ALTER TABLE `trackers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trackers_tracker_id_foreign` (`tracker_id`),
  ADD KEY `trackers_advertisement_id_index` (`advertisement_id`);

--
-- Индексы таблицы `urls`
--
ALTER TABLE `urls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `urls_advertisement_id_url_exact_unique` (`advertisement_id`,`url`,`exact`),
  ADD KEY `urls_advertisement_id_url_index` (`advertisement_id`,`url`),
  ADD KEY `urls_advertisement_id_index` (`advertisement_id`),
  ADD KEY `urls_url_index` (`url`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `trackers`
--
ALTER TABLE `trackers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `urls`
--
ALTER TABLE `urls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `advertisements`
--
ALTER TABLE `advertisements`
  ADD CONSTRAINT `advertisements_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `sm_success-monetization`.`advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `advertisements_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `sm_success-monetization`.`groups` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `advertisements_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sm_success-monetization`.`sites` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `advertisements_targets`
--
ALTER TABLE `advertisements_targets`
  ADD CONSTRAINT `advertisements_targets_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `advertisements_targets_target_id_foreign` FOREIGN KEY (`target_id`) REFERENCES `sm_success-monetization`.`targets` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sm_success-monetization`.`sites` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `categories_advertisements`
--
ALTER TABLE `categories_advertisements`
  ADD CONSTRAINT `categories_advertisements_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_advertisements_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `positions`
--
ALTER TABLE `positions`
  ADD CONSTRAINT `positions_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sm_success-monetization`.`sites` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `posts_advertisements`
--
ALTER TABLE `posts_advertisements`
  ADD CONSTRAINT `posts_advertisements_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_advertisements_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD CONSTRAINT `posts_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`original_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_categories_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `trackers`
--
ALTER TABLE `trackers`
  ADD CONSTRAINT `trackers_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `trackers_tracker_id_foreign` FOREIGN KEY (`tracker_id`) REFERENCES `sm_success-monetization`.`trackers` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `urls`
--
ALTER TABLE `urls`
  ADD CONSTRAINT `urls_advertisement_id_foreign` FOREIGN KEY (`advertisement_id`) REFERENCES `advertisements` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
