O:39:"phpDocumentor\Descriptor\FileDescriptor":22:{s:7:" * hash";s:32:"f97abd63dd4403bfaa2ce1684f7c048c";s:7:" * path";s:43:"Sections/Advertisements/AbstractSection.php";s:9:" * source";s:6723:"<?php

namespace App\Sections\Advertisements;

use App\Models\Group;
use App\Models\Category;
use App\Models\Advertisement;
use App\Repositories\AdvertisementsRepository;
use App\Repositories\CategoriesRepository;
use App\Repositories\GroupsRepository;
use App\Sections\AbstractSection as BaseAbstractSection;

/**
 * Class AbstractSection
 * @package App\Sections\Advertisements
 */
abstract class AbstractSection extends BaseAbstractSection {

    /**
     * @var string
     */
    public $views_namespace = "dashboard::views.advertisements.";

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Advertisement::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return AdvertisementsRepository::class;

    }

    /**
     *
     * Get sub repositories configuration.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            "categories" => [
                "repo" => CategoriesRepository::class,
                "model" => Category::class
            ],
            "groups" => [
                "repo" => GroupsRepository::class,
                "model" => Group::class
            ]
        ];

    }

    /**
     *
     * Run create route action.
     *
     * @param bool $is_template
     * @return array
     */
    public function create(bool $is_template = false) {

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        if(!$is_template) {

            $this->setResponseData(
                "groups_list",
                $this->getSubRepository(
                    "groups"
                )->pluckForUser(
                    "name",
                    "id"
                )
            );

        }

        return $this->toView([
            "alias" => $this->buildAlias(),
            "categories_list" => $categories_list
        ], "create");

    }

    /**
     *
     * Run edit route action.
     *
     * @param string $identifier
     * @return array
     */
    public function edit(string $identifier) {

        $object = $this->getRepository()->findHasChildrenForUser($identifier, [
            "file",
            "categories",
            "groups",
            "comments" => function($query) {

                return $query->decorateDate()->dateOrdered();

            }
        ]);

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        $groups_list = $this->getSubRepository(
            "groups"
        )->pluckForUser("name", "id");

        return $this->toView([
            "alias" => $this->buildAlias(),
            "object" => $object,
            "groups_list" => $groups_list,
            "categories_list" => $categories_list,
            "config" => $this->getDataTableConfigForChildren()
        ]);

    }

    /**
     *
     * Run showCopy route action.
     *
     * @param string $identifier
     * @return array
     */
    public function showCopy(string $identifier) {

        $object = $this->getRepository()->findForUser($identifier, [
            "file",
            "categories",
            "groups"
        ]);

        $this->getRepository()->prepareCopy($object);

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        $groups_list = $this->getSubRepository(
            "groups"
        )->pluckForUser("name", "id");

        return $this->toView([
            "copy" => true,
            "alias" => $this->buildAlias(),
            "object" => $object,
            "groups_list" => $groups_list,
            "categories_list" => $categories_list
        ], "edit");

    }

    /**
     *
     * Run saveCopy route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function saveCopy(string $identifier, array $data) {

        $object = $this->getRepository()->findForUser($identifier, [
            "file"
        ]);

        $result = $this->getRepository()->copyForUser(
            $object,
            $data
        );

        $this->setMessage(trans("t.copied"));

        return $this->toJson([
            "status" => (bool)$result,
            "id" => $result->id
        ]);

    }

    /**
     *
     * Validate toOriginal route action.
     *
     * @return array
     */
    public function validateToOriginal() {

        return [
            'delete_parent' => 'required|boolean'
        ];

    }

    /**
     *
     * Run toOriginal route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function toOriginal(string $identifier, array $data) {

        $object = $this->getRepository()->findForUser($identifier);

        $result = $this->getRepository()->toOriginalForUser(
            $object,
            key_exists("delete_parent", $data) && $data["delete_parent"]
        );

        $this->setMessage(trans("t.successfully moved"));

        return $this->toJson([
            "status" => $result
        ]);

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            'name' => "required|max:255|unique:advertisements,name,NULL,id,user_id,{$this->user->id}",
            'html' => 'required|max:1000',
            'css' => 'required|max:1000',
            'categories' => 'nullable|array',
            'groups' => 'nullable|array',
            'image' => 'nullable|image|max:1024',
            'file_id' => 'nullable|numeric|exists:files,id',
            'is_template' => 'boolean'
        ];

    }

    /**
     *
     * Validate saveCopy route action.
     *
     * @return array
     */
    public function validateSaveCopy() {

        return $this->validateStore();

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route("identifier");

        return [
            'name' => "required|max:255|unique:advertisements,name,$id,id,user_id,{$this->user->id}",
            'html' => 'required|max:1000',
            'css' => 'required|max:1000',
            'categories' => 'nullable|array',
            'groups' => 'nullable|array',
            'image' => 'nullable|image|max:1024',
            'file_id' => 'nullable|numeric|exists:files,id',
            'is_template' => 'boolean'
        ];

    }

}
";s:19:" * namespaceAliases";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:7:{s:5:"Group";s:17:"\App\Models\Group";s:8:"Category";s:20:"\App\Models\Category";s:13:"Advertisement";s:25:"\App\Models\Advertisement";s:24:"AdvertisementsRepository";s:42:"\App\Repositories\AdvertisementsRepository";s:20:"CategoriesRepository";s:38:"\App\Repositories\CategoriesRepository";s:16:"GroupsRepository";s:34:"\App\Repositories\GroupsRepository";s:19:"BaseAbstractSection";s:29:"\App\Sections\AbstractSection";}}s:11:" * includes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:12:" * functions";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * classes";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:44:"\App\Sections\Advertisements\AbstractSection";O:40:"phpDocumentor\Descriptor\ClassDescriptor":19:{s:9:" * parent";s:29:"\App\Sections\AbstractSection";s:13:" * implements";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:11:" * abstract";b:1;s:8:" * final";b:0;s:12:" * constants";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:13:" * properties";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:15:"views_namespace";O:43:"phpDocumentor\Descriptor\PropertyDescriptor":16:{s:9:" * parent";r:22;s:8:" * types";N;s:10:" * default";s:34:""dashboard::views.advertisements."";s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:8:" * fqsen";s:61:"\App\Sections\Advertisements\AbstractSection::views_namespace";s:7:" * name";s:15:"views_namespace";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:22;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:3:"var";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:42:"phpDocumentor\Descriptor\Tag\VarDescriptor":5:{s:15:" * variableName";s:0:"";s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:7:" * name";s:3:"var";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50007";s:7:" * line";i:22;s:10:" * context";a:1:{i:0;s:16:"$views_namespace";}}}}s:19:" * inheritedElement";N;}}}s:10:" * methods";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:12:{s:13:"getModelClass";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:61:"\App\Sections\Advertisements\AbstractSection::getModelClass()";s:7:" * name";s:13:"getModelClass";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:16:"Get model class.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:30;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:18:"getRepositoryClass";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:66:"\App\Sections\Advertisements\AbstractSection::getRepositoryClass()";s:7:" * name";s:18:"getRepositoryClass";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:21:"Get repository class.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:42;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:15:"subRepositories";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:63:"\App\Sections\Advertisements\AbstractSection::subRepositories()";s:7:" * name";s:15:"subRepositories";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:35:"Get sub repositories configuration.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:54;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:6:"create";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:12:"$is_template";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:170;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:47:"phpDocumentor\Descriptor\Type\BooleanDescriptor":0:{}}}s:10:" * default";s:5:"false";s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:12:"$is_template";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:54:"\App\Sections\Advertisements\AbstractSection::create()";s:7:" * name";s:6:"create";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:24:"Run create route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:76;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:12:"$is_template";s:8:" * types";r:180;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:4:"edit";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:11:"$identifier";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:232;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$identifier";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:52:"\App\Sections\Advertisements\AbstractSection::edit()";s:7:" * name";s:4:"edit";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:22:"Run edit route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:110;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$identifier";s:8:" * types";r:242;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:8:"showCopy";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{s:11:"$identifier";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:294;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$identifier";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:56:"\App\Sections\Advertisements\AbstractSection::showCopy()";s:7:" * name";s:8:"showCopy";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:26:"Run showCopy route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:148;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$identifier";s:8:" * types";r:304;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:8:"saveCopy";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:11:"$identifier";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:356;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$identifier";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:5:"$data";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:356;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:5:"$data";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:56:"\App\Sections\Advertisements\AbstractSection::saveCopy()";s:7:" * name";s:8:"saveCopy";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:26:"Run saveCopy route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:184;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$identifier";s:8:" * types";r:366;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}i:1;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:5:"$data";s:8:" * types";r:387;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:18:"validateToOriginal";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:66:"\App\Sections\Advertisements\AbstractSection::validateToOriginal()";s:7:" * name";s:18:"validateToOriginal";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:33:"Validate toOriginal route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:210;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:10:"toOriginal";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:11:"$identifier";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:481;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:46:"phpDocumentor\Descriptor\Type\StringDescriptor":0:{}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:11:"$identifier";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:5:"$data";O:43:"phpDocumentor\Descriptor\ArgumentDescriptor":16:{s:9:" * method";r:481;s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:10:" * default";N;s:14:" * byReference";b:0;s:13:" * isVariadic";b:0;s:8:" * fqsen";s:0:"";s:7:" * name";s:5:"$data";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:8:" * fqsen";s:58:"\App\Sections\Advertisements\AbstractSection::toOriginal()";s:7:" * name";s:10:"toOriginal";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:28:"Run toOriginal route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:226;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{i:0;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:11:"$identifier";s:8:" * types";r:491;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}i:1;O:44:"phpDocumentor\Descriptor\Tag\ParamDescriptor":5:{s:15:" * variableName";s:5:"$data";s:8:" * types";r:512;s:7:" * name";s:5:"param";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:13:"validateStore";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:61:"\App\Sections\Advertisements\AbstractSection::validateStore()";s:7:" * name";s:13:"validateStore";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:28:"Validate store route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:249;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:16:"validateSaveCopy";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:64:"\App\Sections\Advertisements\AbstractSection::validateSaveCopy()";s:7:" * name";s:16:"validateSaveCopy";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:31:"Validate saveCopy route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:270;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}s:14:"validateUpdate";O:41:"phpDocumentor\Descriptor\MethodDescriptor":17:{s:9:" * parent";r:22;s:11:" * abstract";b:0;s:8:" * final";b:0;s:9:" * static";b:0;s:13:" * visibility";s:6:"public";s:12:" * arguments";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:62:"\App\Sections\Advertisements\AbstractSection::validateUpdate()";s:7:" * name";s:14:"validateUpdate";s:12:" * namespace";N;s:10:" * package";s:0:"";s:10:" * summary";s:29:"Validate update route action.";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:282;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:6:"return";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:45:"phpDocumentor\Descriptor\Tag\ReturnDescriptor":4:{s:8:" * types";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:51:"phpDocumentor\Descriptor\Type\UnknownTypeDescriptor":1:{s:7:" * name";s:5:"array";}}}s:7:" * name";s:6:"return";s:14:" * description";s:0:"";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:5:"param";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:13:" * usedTraits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:44:"\App\Sections\Advertisements\AbstractSection";s:7:" * name";s:15:"AbstractSection";s:12:" * namespace";s:28:"\App\Sections\Advertisements";s:10:" * package";s:27:"App\Sections\Advertisements";s:10:" * summary";s:21:"Class AbstractSection";s:14:" * description";s:0:"";s:17:" * fileDescriptor";r:1;s:7:" * line";i:17;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:7:"package";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:7:"package";s:14:" * description";s:27:"App\Sections\Advertisements";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:19:" * inheritedElement";N;}}}s:13:" * interfaces";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:9:" * traits";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:10:" * markers";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}s:8:" * fqsen";s:0:"";s:7:" * name";s:19:"AbstractSection.php";s:12:" * namespace";N;s:10:" * package";s:7:"Default";s:10:" * summary";s:0:"";s:14:" * description";s:0:"";s:17:" * fileDescriptor";N;s:7:" * line";i:0;s:7:" * tags";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:2:{s:7:"package";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:38:"phpDocumentor\Descriptor\TagDescriptor":3:{s:7:" * name";s:7:"package";s:14:" * description";s:7:"Default";s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}}s:10:"subpackage";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:0:{}}}}s:9:" * errors";O:35:"phpDocumentor\Descriptor\Collection":1:{s:8:" * items";a:1:{i:0;O:40:"phpDocumentor\Descriptor\Validator\Error":4:{s:11:" * severity";s:5:"error";s:7:" * code";s:13:"PPC:ERR-50000";s:7:" * line";i:0;s:10:" * context";a:2:{s:11:"{{ value }}";s:2:"""";i:0;s:0:"";}}}}s:19:" * inheritedElement";N;}