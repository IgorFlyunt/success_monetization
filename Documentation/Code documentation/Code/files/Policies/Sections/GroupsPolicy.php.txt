<?php

namespace App\Policies\Sections;

use App\Repositories\GroupsRepository;

/**
 * Class GroupsPolicy
 * @package App\Policies\Sections
 */
class GroupsPolicy extends AbstractPolicy
{

    /**
     * GroupsPolicy constructor.
     * @param GroupsRepository $repository
     */
    function __construct(GroupsRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return true;

    }

    /**
     * @return bool
     */
    public function edit() {

        return true;

    }

    /**
     * @return bool
     */
    public function update() {

        return true;

    }

    /**
     * @return bool
     */
    public function destroy() {

        return true;

    }

}

