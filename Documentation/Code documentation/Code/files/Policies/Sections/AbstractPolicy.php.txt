<?php

namespace App\Policies\Sections;

use App\Models\User;
use Illuminate\Http\Request;
use App\Interfaces\Policy\PolicyInterface;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Interfaces\Repository\RepositoryInterface;

/**
 * Class AbstractPolicy
 * @package App\Policies\Sections
 */
abstract class AbstractPolicy implements PolicyInterface
{

    use HandlesAuthorization;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     *
     * Route identifier if exists.
     *
     * @var \Illuminate\Routing\Route|object|string
     */
    protected $identifier;

    /**
     *
     * Route identifier parameter name.
     *
     * @var string
     */
    public $identifier_name = "identifier";

    /**
     * AbstractPolicy constructor.
     * @param RepositoryInterface $repository
     */
    function __construct(RepositoryInterface $repository)
    {

        $this->repository = $repository;

        $this->identifier = request()->route(
            $this->identifier_name
        );

    }

    /**
     *
     * Additional check before main check.
     *
     * @param User $user
     * @param string $action
     * @param string $section
     * @return bool
     */
    public function before(User $user, string $action, string $section) {

        $this->repository->setUser($user);

        if($this->checkExistsForUser()) {

            if(!empty($this->identifier)) {

                if(($object = $this->repository->find($this->identifier))) {

                    if($object->user_id != $user->id) {

                        return false;

                    }

                } else abort(404);

            }

        }

    }

}

