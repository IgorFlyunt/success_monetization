<?php

namespace App\Repositories\Site;

use App\Models\Site;
use App\Models\Site\Post;

/**
 * Class PostsRepository
 * @package App\Repositories\Site
 */
class PostsRepository extends AbstractRepository {

    /**
     * PostsRepository constructor.
     * @param Post $model
     * @param Site $site
     */
    function __construct(Post $model, Site $site) {

        parent::__construct($model, $site);

    }

    /**
     *
     * Get unique field name for table.
     *
     * @return string
     */
    public function getUniqueField() : string {

        return "url";

    }

    /**
     *
     * Save and update functionality.
     *
     * @param array $data
     */
    public function saveForSiteBody(array $data) {

        foreach($data as $post) {

            $model = $this->model->updateOrCreate([
                "url" => $post["url"],
                "site_id" => $this->site->id
            ], [
                "title" => $post["title"]
            ]);

            if(key_exists("categories", $post)) {

                $this->attachRelation(
                    "categories",
                    $model,
                    $post["categories"],
                    true,
                    "original_id"
                );

            }

        }

    }

}

