<?php

namespace App\Repositories;

use App\Models\Advertisement;
use Illuminate\Http\UploadedFile;
use App\Traits\Repository\CommentsTrait;
use App\Events\Advertisement\SavedEvent;
use App\Events\Advertisement\DeletedEvent;
use App\Traits\Repository\HasRelationTrait;
use App\Traits\Repository\TrashMethodsTrait;
use App\Traits\Repository\ForUserMethodsTrait;
use App\Traits\Repository\AdvertisementTypesTrait;
use App\Decorators\Advertisements\Context\TemplateListDecorator;

/**
 * Class AdvertisementsRepository
 * @package App\Repositories
 */
class AdvertisementsRepository extends AbstractRepository {

    use CommentsTrait;
    use HasRelationTrait;
    use TrashMethodsTrait;
    use ForUserMethodsTrait;
    use AdvertisementTypesTrait;

    /**
     * @var array
     */
    public $events = [
        "saved" => SavedEvent::class,
        "deleted" => DeletedEvent::class
    ];

    /**
     * AdvertisementsRepository constructor.
     * @param Advertisement $model
     */
    function __construct(Advertisement $model) {

        parent::__construct($model);

    }

    /**
     *
     * Get array of records by specified type.
     *
     * @param string $type
     * @return mixed
     */
    public function getTypedArrayForUser(string $type) {

        return $this->model->where(
            "user_id",
            $this->user->id
        )->where(
            "type",
            $type
        )->where(
            "is_template",
            false
        )->pluck(
            "name",
            "id"
        )->toArray();

    }

    /**
     *
     * Get records for DataTable for specified user.
     *
     * @param string $type
     * @param bool $original
     * @return mixed
     */
    public function getForTableForUser(string $type, bool $original = true) {

        $with = ["groups", "categories", "file", "sites"];

        $with[$original ? "parent" : "children"] = function($query) {

            return $query->where("user_id", $this->user->id);

        };

        $query = $this->model->with(
            $with
        )->where(
            "user_id",
            $this->user->id
        )->where(
            "is_template",
            false
        )->whereType(
            $type
        );

        if($original) {

            $query->doesntHave("parent");

        } else $query->has("parent");

        return $query->get();

    }

    /**
     *
     * Disable records by identifiers and specified user.
     *
     * @param array $ids
     * @return bool
     */
    public function stopActionForUser(array $ids) {

        $this->dispatchEvent(
            "deleted",
            $ids,
            $this->user
        );

        return (bool)$this->model->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->whereStatus(
            true
        )->update([
            "status" => false
        ]);

    }

    /**
     *
     * Launch records by identifiers and specified user.
     *
     * @param array $ids
     * @return bool
     */
    public function runActionForUser(array $ids) {

        return (bool)$this->model->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->whereStatus(false)->update([
            "status" => true
        ]);

    }

    /**
     *
     * Create object for specified user and save to database.
     *
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function createForUser(array $data) {

        $data["user_id"] = $this->user->id;

        return transaction_wrapper([
            "class" => $this,
            "method" => "saveChanges"
        ], [$data]);

    }

    /**
     *
     * Update passed object for specified user.
     *
     * @param $object
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function updateObjectForUser($object, array $data) {

        if($object->user_id == $this->user->id) {

            $object = transaction_wrapper([
                "class" => $this,
                "method" => "saveChanges"
            ], [$data, $object]);

            if($object) {

                $this->dispatchEvent(
                    "saved",
                    $object,
                    $this->user
                );

            }

            return $object;

        }

        return false;

    }

    /**
     *
     * Save changes after creation or updation to database.
     *
     * @param array $data
     * @param Advertisement|null $model
     * @return Advertisement
     */
    public function saveChanges(array $data, Advertisement $model = null) {

        $image = null;

        $categories = [];

        $groups = [];

        if(key_exists("image", $data)) {

            if(is_a($data["image"], UploadedFile::class)) {

                $image = $data["image"];

            } elseif(!key_exists("file_id", $data)) {

                $data["file_id"] = null;

            }

            unset($data["image"]);

        }

        if(key_exists("categories", $data) && is_array($data["categories"])) {

            $categories = $data["categories"];

        }

        unset($data["categories"]);

        if(key_exists("groups", $data) && is_array($data["groups"])) {

            $groups = $data["groups"];

        }

        unset($data["groups"]);

        if(is_null($model)) {

            $advertisement = $this->model->create($data);

        } else {

            $advertisement = $model;

            if($advertisement->file_id) {

                if($image || (key_exists("file_id", $data) && $data["file_id"] != $advertisement->file_id)) {

                    if($advertisement->checkIsDeletableFile()) {

                        delete_file($advertisement->file->path);

                        $advertisement->file->delete();

                    }

                }

            }

            $advertisement->update($data);

        }

        if($image) {

            $image = upload_file($image, "advertisements/contexts", $advertisement->id);

            $file = $advertisement->file()->create([
                "path" => $image
            ]);

            $advertisement->update([
                "file_id" => $file->id
            ]);

        }

        $this->attachRelation(
            "categories",
            $advertisement,
            $categories,
            !is_null($model)
        );

        $this->attachRelation(
            "groups",
            $advertisement,
            $groups,
            !is_null($model)
        );

        return $advertisement;

    }

    /**
     *
     * Delete records by identifiers for specified user.
     *
     * @param $ids
     * @param bool $force
     * @return bool|mixed
     * @throws \Exception
     */
    public function deleteForUser($ids, bool $force = false) {

        $closure = function() use ($ids, $force) {

            $query = $this->model->where(
                "user_id",
                $this->user->id
            )->whereIn(
                "id",
                is_array($ids) ? $ids : [$ids]
            );

            if($force) $query->onlyTrashed();

            $ids = $query->pluck(
                "id"
            )->toArray();

            if($force) {

                $objects_with_image = $this->model->withTrashed()->where(
                    "user_id",
                    $this->user->id
                )->has(
                    'file'
                )->selectIsDeletableFile()->get();

                foreach($objects_with_image->whereIn("id", $ids) as $index => $object) {

                    if($object->checkIsDeletableFile("file_id", $objects_with_image)) {

                        delete_file($object->file->path);

                        $object->file->delete();

                    }

                    $objects_with_image->forget($index);

                }

                $this->deleteComments($ids);

            }

            $this->dispatchEvent(
                "deleted",
                $ids,
                $this->user
            );

            $method = $force ? "forceDelete" : "delete";

            return $this->model->whereIn(
                "id",
                $ids
            )->$method();

        };

        return transaction_wrapper(
            $closure
        );

    }

    /**
     *
     * Prepare the object to copying.
     *
     * @param Advertisement $object
     * @param bool $template
     */
    public function prepareCopy(Advertisement &$object, bool $template = false) {

        if($template) {

            $object->name = '';

        } else {

            $object->name = "{$object->name} (Копия)";

        }

    }

    /**
     *
     * Copy object for specified user.
     *
     * @param Advertisement $object
     * @param array $data
     * @return Advertisement
     */
    public function copyForUser(Advertisement $object, array $data) {

        $parent_id = $object->id;

        if(key_exists("is_template", $data) && $data["is_template"]) {

            $parent_id = null;

        }

        $data = array_merge([
            "parent_id" => $parent_id,
            "file_id" => $object->file_id,
            "user_id" => $this->user->id,
            "status" => false,
        ], $data);

        return $this->saveChanges($data);

    }

    /**
     *
     * Get filtered identifiers that are related to user.
     *
     * @param array $ids
     * @return mixed
     */
    public function filterIdsForUser(array $ids) {

        return $this->model->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->where(
            "is_template",
            false
        )->pluck(
            "id"
        )->toArray();

    }

    /**
     *
     * Load children records by specified parent for specified user.
     *
     * @param int $parent_id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function loadChildrenForUser(int $parent_id) {

        return $this->model->with([
            "file",
            "sites",
            "groups",
            "categories"
        ])->where(
            "parent_id",
            $parent_id
        )->where(
            "user_id",
            $this->user->id
        )->get();

    }

    /**
     *
     * Move record from children to parents and delete parent if need.
     *
     * @param Advertisement $object
     * @param bool $delete_parent
     * @return bool|mixed
     * @throws \Exception
     */
    public function toOriginalForUser(Advertisement $object, bool $delete_parent) {

        if($delete_parent) {

            $this->deleteForUser($object->parent_id);

        }

        return $this->updateObjectForUser($object, [
            "parent_id" => null
        ]);


    }

    /**
     *
     * Get records with abstract field "has_children" for specified user.
     *
     * @param int $id
     * @param array $with
     * @param array $has
     * @return mixed
     */
    public function findHasChildrenForUser(int $id, array $with = array(), array $has = array()) {

        $table = $this->model->getTable();

        $where = "$table.user_id = sub_$table.user_id";

        $subquery = $this->getHasRelationSubquery(
            $table,
            "id",
            $table,
            "parent_id",
            $where
        );

        $query = $this->model->where(
            "user_id",
            $this->user->id
        )->with($with);

        $this->_buildHave($query, $has);

        return $query->selectRaw(
            "$table.*, $subquery as has_children"
        )->find($id);

    }

    /**
     *
     * Get records which are templates for specified user.
     *
     * @return mixed
     */
    public function getTemplatesForUser() {

        $elements = $this->model->where(
            "user_id",
            $this->user->id
        )->with([
            "file",
            "categories"
        ])->where(
            "is_template",
            true
        )->get();

        return $this->decorate(
            $elements,
            TemplateListDecorator::class
        );

    }

    /**
     *
     * Get array of templates which are related to specified user.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function getTemplatesForSelectForUser() {

        return $this->model->with(["file", "categories"])->where(
            "user_id",
            $this->user->id
        )->where(
            "is_template",
            true
        )->select([
            "id",
            "name",
            "css",
            "html",
            "file_id"
        ])->get()->keyBy("id");

    }

    /**
     *
     * Check if record exists and if record is parent for specified user.
     *
     * @param int $id
     * @param bool $is_template
     * @return mixed
     */
    public function existsForCopyingForUser(int $id, bool $is_template = false) {

        return $this->model->where(
            "user_id",
            $this->user->id
        )->where(
            "id",
            $id
        )->where(
            "is_template",
            $is_template
        )->doesntHave(
            "parent"
        )->exists();

    }

    /**
     *
     * Check if record exists for specified user.
     *
     * @param int $id
     * @param bool $is_template
     * @return mixed
     */
    public function existsForEditingForUser(int $id, bool $is_template = false) {

        return $this->model->where(
            "user_id",
            $this->user->id
        )->where(
            "id",
            $id
        )->where(
            "is_template",
            $is_template
        )->exists();

    }

    /**
     *
     * Get records by type for delete for specified user.
     *
     * @param string $type
     * @param array $with
     * @param array $has
     * @return mixed
     * @throws \Exception
     */
    public function getDeletedForUserByType(string $type, array $with = array(), array $has = array()) {

        $query = $this->model->where(
            "type",
            $type
        )->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->onlyTrashed()->with($with);

        $this->_buildHave($query, $has);

        return $query->get();

    }

    /**
     *
     * Get records by identifiers for delete for specified user.
     *
     *
     * @param array $ids
     * @return mixed
     */
    public function getForUserForDelete(array $ids) {

        return $this->model->with([
            "sites",
            "sites.synchronization"
        ])->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->withTrashed()->get();

    }

}

