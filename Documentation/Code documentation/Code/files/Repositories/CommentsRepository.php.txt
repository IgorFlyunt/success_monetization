<?php

namespace App\Repositories;

use App\Models\Comment;

/**
 * Class CommentsRepository
 * @package App\Repositories
 */
class CommentsRepository extends AbstractRepository {

    /**
     * CommentsRepository constructor.
     * @param Comment $model
     */
    function __construct(Comment $model) {

        parent::__construct($model);

    }

    /**
     *
     * Create new record and save changes to database.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {

        $object = parent::create($data);

        return $object->decorateDate(
            "created_at",
            "d-m-Y H:i:s",
            "date"
        );

    }

    /**
     *
     * Get records for user by related model.
     *
     * @param int $modelable_id
     * @param string $modelable_type
     * @return mixed
     */
    public function getForUser(int $modelable_id, string $modelable_type) {

        return $this->model->where(
            "modelable_id",
            $modelable_id
        )->where(
            "modelable_type",
            $modelable_type
        )->decorateDate()->get();

    }

    /**
     *
     * Make sure the linked objects exist and are allowed for the user.
     *
     * @param string $class
     * @param int $id
     * @return mixed
     */
    public function targetExistsForUser(string $class, int $id) {

        return $class::where(
            "user_id",
            $this->user->id
        )->where(
            "id",
            $id
        )->exists();

    }

}

