<?php

namespace App\Repositories;

use App\Models\Synchronization;
use App\Events\Synchronization\SavedEvent;
use App\Traits\Repository\Sites\ForSiteMethodsTrait;

/**
 * Class SynchronizationsRepository
 * @package App\Repositories
 */
class SynchronizationsRepository extends AbstractRepository {

    use ForSiteMethodsTrait;

    /**
     * @var array
     */
    public $events = [
        "saved" => SavedEvent::class
    ];

    /**
     * SynchronizationsRepository constructor.
     * @param Synchronization $model
     */
    function __construct(Synchronization $model) {

        parent::__construct($model);

    }

    /**
     *
     * Create or update functionality for specified user.
     *
     * @param array $data
     * @return mixed
     */
    public function saveForSite(array $data) {

        $old_object = $this->findForSite();

        $object = $this->updateOrCreate([
            "site_id" => $this->site->id
        ], array_merge([
            "status" => false,
            "error" => null
        ], $data));

        if($old_object && $old_object->key != $object->key) {

            $this->dispatchEvent(
                "saved",
                $old_object->key,
                $object->key,
                $this->site
            );

        }

        return $object;

    }

    /**
     *
     * Get record for specified site.
     *
     * @return mixed
     */
    public function findForSite() {

        return $this->model->where(
            "site_id",
            $this->site->id
        )->first();

    }

}

