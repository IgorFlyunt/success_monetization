<?php

namespace App\Events\Group;

use App\Models\Site\Advertisement;
use App\Models\User;
use App\Repositories\Site\AdvertisementsRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class DeletedEvent
 *
 * Event after deleting groups.
 *
 * @package App\Events\Group
 */
class DeletedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var
     */
    public $advertisements;

    /**
     * @var mixed
     */
    public $tenant_id;

    /**
     * DeletedEvent constructor.
     * @param array $ids
     * @param User $user
     */
    public function __construct(array $ids, User $user)
    {

        $this->advertisements = $this->loadAdvertisements(
            $ids,
            $user
        );

        $this->tenant_id = $user->id;

    }

    /**
     * @param array $ids
     * @param User $user
     * @return mixed
     */
    private function loadAdvertisements(array $ids, User $user) {

        $repository = new AdvertisementsRepository(
            new Advertisement
        );

        return $repository->setUser(
            $user
        )->getForStopForUser(
            "group_id",
            $ids
        );

    }

}

