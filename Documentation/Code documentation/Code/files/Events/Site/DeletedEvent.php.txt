<?php

namespace App\Events\Site;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class DeletedEvent
 *
 * Event after site is deleted.
 *
 * @package App\Events\Site
 */
class DeletedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var array
     */
    public $ids;

    /**
     * @var User
     */
    public $user;

    /**
     * DeletedEvent constructor.
     * @param array $ids
     * @param User $user
     */
    public function __construct(array $ids, User $user)
    {

        $this->ids = $ids;

        $this->user = $user;

    }

}

