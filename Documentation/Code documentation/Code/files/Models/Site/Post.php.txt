<?php

namespace App\Models\Site;

/**
 * Class Post
 * @package App\Models\Site
 */
class Post extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        "site_id",
        "title",
        "url"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories() {

        return $this->belongsToMany(
            Category::class,
            "posts_categories",
            null,
            null,
            "id",
            "original_id"
        );

    }

}

