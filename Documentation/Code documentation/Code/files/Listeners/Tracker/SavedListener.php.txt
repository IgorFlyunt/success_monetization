<?php

namespace App\Listeners\Tracker;

use App\Events\Tracker\SavedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\Redis\AdvertisementsStorage;
use App\Repositories\Site\AdvertisementsRepository;

/**
 * Class SavedListener
 *
 * Handler for App\Events\Tracker\SavedEvent.
 *
 * Change data in redis for all App\Models\Site\Advertisement which are related to passed tracker.
 *
 * @package App\Listeners\Tracker
 */
class SavedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var AdvertisementsStorage
     */
    protected $advertisementsStorage;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * SavedListener constructor.
     * @param AdvertisementsStorage $advertisementsStorage
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(
        AdvertisementsStorage $advertisementsStorage,
        AdvertisementsRepository $advertisementsRepository
    )
    {

        $this->advertisementsStorage = $advertisementsStorage;

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param SavedEvent $event
     */
    public function handle(SavedEvent $event)
    {

        $chunks = $this->advertisementsRepository->setUser(
            $event->user
        )->getForTrackersForUser(
            $event->tracker->id
        )->groupBy(
            "site_id"
        );

        foreach($chunks as $advertisements) {

            $site = $advertisements->first()->site;

            if($site && ($prefix = $site->getSyncronization("key"))) {

                foreach($advertisements as $advertisement) {

                    $this->advertisementsStorage->changeJson(
                        $prefix,
                        $advertisement->id,
                        $this->advertisementsRepository->buildJson(
                            $advertisement
                        )
                    );

                }

            }

        }

    }

}

