<?php

namespace App\Listeners\Site;

use App\Events\Site\SavedEvent;
use App\Classes\Redis\ConfigStorage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SavedListener
 *
 * Handler for App\Events\Site\SavedEvent.
 *
 * Change data in redis for site.
 *
 * @package App\Listeners\Site
 */
class SavedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * SavedListener constructor.
     * @param ConfigStorage $configStorage
     */
    public function __construct(ConfigStorage $configStorage)
    {

        $this->configStorage = $configStorage;

    }

    /**
     * @param SavedEvent $event
     */
    public function handle(SavedEvent $event)
    {

        if(($prefix = $event->site->getSyncronization("key"))) {

            $this->configStorage->setSite(
                $prefix,
                $event->site->url
            );

        }

    }

}

