<?php

namespace App\Listeners\Site\Advertisements;

use App\Events\Site\Advertisements\DeletedEvent;

/**
 * Class DeletedListener
 *
 * Handler for App\Events\Site\Advertisements\DeletedEvent;
 *
 * Remove from redis data for all deleted App\Models\Site\Advertisement.
 *
 * @package App\Listeners\Site\Advertisements
 */
class DeletedListener extends AbstractListener
{

    /**
     * @param DeletedEvent $event
     */
    public function handle(DeletedEvent $event)
    {

        $this->load(
            ($site = $event->site)
        );

        if(($prefix = $site->getSyncronization("key"))) {

            $this->advertisementsRepository->setSite(
                $site
            )->getForSiteForDelete(
                $event->ids
            )->each(function($advertisement) use ($prefix) {

                $remove_group = false;

                if($advertisement->group_id) {

                    $remove_group = !$this->advertisementsRepository->checkInGroupExists(
                        $advertisement->group_id,
                        $advertisement->id
                    );

                }

                $decorated = $this->decorateData(
                    $advertisement
                );

                if($remove_group) {

                    $this->configStorage->delete(
                        $prefix,
                        $advertisement->getRedisKey(),
                        $decorated
                    );

                }

                $this->configStorage->delete(
                    $prefix,
                    $advertisement->getRedisKey(true),
                    $decorated
                );

                $this->advertisementsStorage->delete(
                    $prefix,
                    $advertisement->id,
                    $advertisement->group_id
                );

            });

            if($event->force_delete) {

                $this->advertisementsRepository->deleteForSite(
                    $event->ids,
                    true
                );

            }

        }

    }

}

