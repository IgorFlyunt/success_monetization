<?php
/**
 * Created by PhpStorm.
 * User: romeos
 * Date: 9/9/18
 * Time: 1:00 PM
 */

namespace App\Traits\Repository;


/**
 * Trait AdvertisementPositionsTrait
 * @package App\Traits\Repository
 */
trait AdvertisementPositionsTrait
{

    /**
     * @param string $type
     * @return array
     */
    public function getPositions(string $type) {

        $result = [];

        foreach(config("advertisement.positions.$type", []) as $position => $number) {

            $result[$position] = trans("t.advertisements.positions.$position");

        }

        return $result;

    }

}
