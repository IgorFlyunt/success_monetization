<?php

namespace App\Traits\Model;

use App\Models\Comment as BaseComment;
use App\Models\Site\Comment;

/**
 * Trait CommentsTrait
 * @package App\Traits\Model
 */
trait CommentsTrait {

    /**
     * @return mixed
     */
    public function comments() {

        return $this->morphMany(
            $this->getRelationClass(),
            'modelable'
        );

    }

    /**
     *
     * Get relation model which is depends on connection.
     *
     * @return string
     */
    private function getRelationClass() {

        if($this->connection == config("database.default")) {

            return BaseComment::class;

        }

        return Comment::class;

    }

}

