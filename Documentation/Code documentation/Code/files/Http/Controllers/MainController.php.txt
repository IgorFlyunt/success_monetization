<?php

namespace App\Http\Controllers;

/**
 * Class MainController
 *
 * Layer for App\Http\Controllers\Controller which implements methods for response view.
 *
 * @package App\Http\Controllers
 */
class MainController extends Controller
{

    /**
     * @var array
     */
    protected $data = array();

    /**
     *
     * Add data to response view.
     *
     * @param string $key
     * @param $data
     * @return $this
     */
    public function setData(string $key, $data) {

        $this->data[$key] = $data;

        return $this;

    }

    /**
     *
     * Return response view with data.
     *
     * @param string $view
     * @param array $data
     * @param string $path
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render(string $view, array $data = array(), string $path = 'dashboard') {

        $data = array_merge($data, $this->data);

        return view("$path::$view")->with($data);

    }

}

