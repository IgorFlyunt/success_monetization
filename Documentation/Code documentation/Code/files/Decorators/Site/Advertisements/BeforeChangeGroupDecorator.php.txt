<?php

namespace App\Decorators\Site\Advertisements;

use Illuminate\Support\Collection;
use App\Decorators\AbstractDecorator;

/**
 * Class BeforeChangeGroupDecorator
 *
 * Decorate App\Models\Site\Advertisement before App\Repositories\Sites\AdvertisementsRepository changeGroupForSite() call
 *
 * @package App\Decorators\Site\Advertisements
 */
class BeforeChangeGroupDecorator extends AbstractDecorator {

    /**
     * @return array
     */
    public function decorate() {

        return [
            "urls" => $this->decorateUrls(
                $this->data["advertisement"]->urls
            ),
            "posts" => $this->decoratePosts(
                $this->data["advertisement"]->posts
            ),
            "categories" => $this->decorateCategories(
                $this->data["advertisement"]->categories
            )
        ];

    }

    /**
     * @param Collection $categories
     * @return array
     */
    private function decorateCategories(Collection $categories) {

        return [
            "exact" => $categories->where(
                "pivot.exact",
                true
            )->pluck(
                "id"
            )->toArray(),
            "paths" => $categories->where(
                "pivot.exact",
                false
            )->pluck(
                "id"
            )->toArray()
        ];

    }

    /**
     * @param Collection $posts
     * @return array
     */
    private function decoratePosts(Collection $posts) {

        return [
            "include" => $posts->where(
                "pivot.include",
                true
            )->pluck(
                "id"
            )->toArray(),
            "exclude" => $posts->where(
                "pivot.include",
                false
            )->pluck(
                "id"
            )->toArray()
        ];

    }

    /**
     * @param Collection $urls
     * @return array
     */
    private function decorateUrls(Collection $urls) {

        return $urls->map(function($url) {

            return [
                "exclude" => $url->exclude,
                "exact" => $url->exact,
                "url" => $url->url
            ];

        })->toArray();

    }

}

