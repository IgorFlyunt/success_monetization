<?php

namespace App\Decorators\Site\Advertisements\Redis;

use App\Decorators\AbstractDecorator;

/**
 * Class SyncCategoriesDecorator
 *
 * Decorate categories url before App\Events\Synchronization\SavedEvent call.
 *
 * @package App\Decorators\Site\Advertisements\Redis
 */
class SyncCategoriesDecorator extends AbstractDecorator {

    /**
     * @return array
     */
    public function decorate() {

        return array_merge(
            $this->data,
            array_map(function($url) {

                return $url . '*';

            }, $this->data)
        );

    }

}

