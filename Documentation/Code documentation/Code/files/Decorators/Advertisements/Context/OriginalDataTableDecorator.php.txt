<?php

namespace App\Decorators\Advertisements\Context;

use App\Models\Advertisement;
use App\Decorators\AbstractDecorator;

/**
 * Class OriginalDataTableDecorator
 *
 * Decorate parent context advertisements for DataTable.
 *
 * @package App\Decorators\Advertisements\Context
 */
class OriginalDataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return $this->decorateElement($item);

        })->toArray();

    }

    /**
     * @param Advertisement $item
     * @return array
     */
    private function decorateElement(Advertisement $item) {

        return [
            "id" => $item->id,
            "css" => $item->css,
            "html" => $item->buildHtml(),
            "name" => $item->name,
            "groups" => $item->groups->pluck("name")->toArray(),
            "children" => $this->decorateChildren($item),
            "categories" => $item->categories->pluck("name")->toArray(),
            "status" => (bool)$item->status,
            "sites" => $item->sites->pluck("url")->toArray()
        ];

    }

    /**
     * @param Advertisement $parent
     * @return array
     */
    private function decorateChildren(Advertisement $parent) {

        return [
            "title" => sprintf(trans("t.contexts duplications"), $parent->name),
            "elements" => $parent->children->map(function($child) {

                return [
                    "title" => $child->name,
                    "url" => route("resolver.edit", ["contexts", $child->id])
                ];

            })->toArray()
        ];

    }

}


