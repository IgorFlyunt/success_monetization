<?php

namespace App\Sections\Site;

use App\Models\Site;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\SitesRepository;
use App\Interfaces\Repository\RepositoryInterface;
use App\Sections\AbstractSection as BaseAbstractSection;

/**
 * Class AbstractSection
 * @package App\Sections\Site
 */
abstract class AbstractSection extends BaseAbstractSection {

    /**
     * @var \App\Models\Site
     */
    protected $site;

    /**
     * @var integer
     */
    protected $site_id;

    /**
     *
     * Route parameters which are not in use while build alias.
     *
     * @var array
     */
    public $unused_route_params = [
        "site_id"
    ];

    /**
     * AbstractSection constructor.
     * @param User|null $user
     * @param Request $request
     */
    function __construct(User $user = null, Request $request) {

        parent::__construct($user, $request);

        if($this->user) {

            $this->loadSite();

        }

    }

    /**
     *
     * Get sub repositories configuration array.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            "sites" => [
                "repo" => SitesRepository::class,
                "model" => Site::class
            ]
        ];

    }

    /**
     *
     * Custom method for seting meta title.
     *
     * @param string $action
     */
    protected function setTitle(string $action) {

        $this->setMetaData(
            sprintf(
                trans(
                    $this->buildMetaPrefix(
                        "title",
                        $action
                    )
                ), $this->site->url
            )
        );

    }

    /**
     *
     * Custom repository constructor.
     *
     * @return RepositoryInterface
     */
    public function getRepository() : RepositoryInterface {

        $repository = parent::getRepository();

        return $repository->setSite(
            $this->site
        );

    }

    /**
     *
     * Run loadSite route action.
     *
     */
    private function loadSite() {

        $this->site_id = $this->request->route(
            "site_id"
        );

        $this->site = $this->getSubRepository(
            "sites"
        )->findForUser(
            $this->site_id
        );

        $this->setResponseData(
            "site_id",
            $this->site_id
        );

    }

}

