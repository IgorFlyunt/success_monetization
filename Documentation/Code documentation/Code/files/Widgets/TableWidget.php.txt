<?php

namespace App\Widgets;

/**
 * Class TableWidget
 * @package App\Widgets
 */
class TableWidget extends AbstractWidget {

    /**
     * @var string
     */
    public $url;

    /**
     * TableWidget constructor.
     * @param string $url
     * @param array $config
     */
    function __construct(string $url, array $config) {

        $this->url = $url;

        parent::__construct($config);

    }

    /**
     *
     * Configure view before render.
     *
     * @return string
     */
    public function render() : string {

        return $this->view("table.index", [
            "url" => $this->url
        ]);

    }

}

