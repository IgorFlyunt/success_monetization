<?php

namespace App\Widgets;

use App\Interfaces\WidgetInterface;
use App\Models\User;
use App\Classes\Menu;

/**
 * Class MenuWidget
 * @package App\Widgets
 */
class MenuWidget implements WidgetInterface {

    /**
     * @var User
     */
    protected $user;

    /**
     *
     * View name.
     *
     * @var string
     */
    protected $template = 'widgets::menu.index';

    /**
     * MenuWidget constructor.
     * @param User $user
     */
    function __construct(User $user) {

        $this->user = $user;

    }

    /**
     *
     * Configure view before render.
     *
     * @return string
     */
    public function render() : string {

        $menu = (new Menu($this->user))->build();

        return view($this->template, compact('menu'));

    }

}

