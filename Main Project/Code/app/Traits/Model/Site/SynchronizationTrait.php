<?php

namespace App\Traits\Model\Site;

use App\Models\Synchronization;

/**
 * Trait SynchronizationTrait
 * @package App\Traits\Model\Site
 */
trait SynchronizationTrait {

    /**
     * @return mixed
     */
    public function synchronization() {

        return $this->hasOne(Synchronization::class);

    }

    /**
     *
     * Check if model has synchronization.
     *
     * @return bool
     */
    public function hasSyncronization() {

        return (bool)$this->synchronization;

    }

    /**
     *
     * Get synchronization value by key.
     *
     * @param string|null $key
     * @return mixed
     */
    public function getSyncronization(string $key = null) {

        if(!is_null($key) && $this->hasSyncronization()) {

            return $this->synchronization->$key;

        }

        return $this->synchronization;

    }

    /**
     *
     * Get and decorate synchronized_at field if it's exists.
     *
     * @param bool $formatted
     * @param string $format
     * @return mixed|null
     */
    public function getSynchronizedAt(bool $formatted = true, string $format = "d-m-Y H:i:s") {

        if($this->hasSyncronization()) {

            $synchronized_at = $this->getSyncronization("synchronized_at");

            if(!empty($synchronized_at)) {

                if($formatted && !empty($format)) {

                    return $synchronized_at->format($format);

                }

                return $synchronized_at;

            }

        }

        return null;

    }

}
