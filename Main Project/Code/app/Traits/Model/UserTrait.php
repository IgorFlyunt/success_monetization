<?php

namespace App\Traits\Model;

use App\Models\User;

/**
 * Trait UserTrait
 * @package App\Traits\Model
 */
trait UserTrait {

    /**
     * @return mixed
     */
    public function user() {

        return $this->belongsTo(User::class);

    }

    /**
     *
     * Query scope to get records which are related to specified user.
     *
     * @param $query
     * @param int $user_id
     * @return mixed
     */
    public function scopeForUser($query, int $user_id) {

        return $query->whereHas("user", function($query) use ($user_id) {

            return $query->where("id", $user_id);

        });

    }

}
