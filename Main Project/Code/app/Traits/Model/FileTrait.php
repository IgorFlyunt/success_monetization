<?php

namespace App\Traits\Model;

use App\Models\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait FileTrait
 * @package App\Traits\Model
 */
trait FileTrait {

    /**
     * @return mixed
     */
    public function file() {

        return $this->belongsTo(File::class);

    }

    /**
     *
     * Check if model has file.
     *
     * @param File|null $file
     * @return bool
     */
    public function hasFile(File $file = null) {

        $file = $file ?: $this->file;

        if($file) {

            return file_exists(
                public_path(
                    $file->path
                )
            );

        }

        return false;

    }

    /**
     *
     * Get related file url.
     *
     * @param File|null $file
     * @return \Illuminate\Contracts\Routing\UrlGenerator|null|string
     */
    public function getFileUrl(File $file = null) {

        $file = $file ?: $this->file;

        if($this->hasFile($file)) {

            return url($file->path);

        }

        return null;

    }

    /**
     *
     * Get related field path.
     *
     * @param File|null $file
     * @return null|string
     */
    public function getFilePath(File $file = null) {

        $file = $file ?: $this->file;

        if($this->hasFile($file)) {

            return public_path($file->path);

        }

        return null;

    }

    /**
     *
     * Get mysql sub query to check if file is deletable.
     *
     * @param string $field
     * @return string
     */
    private function _getIsDeletableFileSubquery(string $field = "file_id") {

        $table = $this->getTable();

        $sub_table = "sub_$table";

        $subquery = DB::table(
            "$table as $sub_table"
        )->selectRaw(
            "$sub_table.id"
        )->whereRaw(
            "$sub_table.$field = $table.$field"
        )->whereRaw(
            "$sub_table.id <> $table.id"
        )->limit(1)->toSql();

        return "(IF(($subquery) = NULL, 1, 0))";

    }

    /**
     *
     * Query scope to get only deletable files.
     *
     * @param $query
     * @param string $field
     */
    public function scopeWithDeletableFilesOnly($query, string $field = "file_id") {

        $query->whereRaw("{$this->_getIsDeletableFileSubquery($field)} = 1");

    }

    /**
     *
     * Query scope to get "is_deletable" field.
     *
     * @param $query
     * @param string $field
     */
    public function scopeSelectIsDeletableFile($query, string $field = "file_id") {

        $query->selectRaw("{$this->getTable()}.*, {$this->_getIsDeletableFileSubquery($field)} as {$field}_deletable");

    }

    /**
     *
     * Check if file is deletable.
     *
     * @param string $field
     * @param Collection|null $collection
     * @return bool
     */
    public function checkIsDeletableFile(string $field = 'file_id', Collection $collection = null) {

        if(!empty($this->$field)) {

            if(!is_null($collection)) {

                return !(bool)$collection->where(
                    $field,
                    $this->$field
                )->where(
                    'id',
                    '!=',
                    $this->id
                )->first();

            }

            $query = $this->getModel()->where(
                $field,
                $this->$field
            )->where(
                'id',
                '!=',
                $this->id
            );

            if(method_exists($this, "forceDelete")) {

                $query = $query->withTrashed();

            }

            return !$query->exists();

        }

        return false;

    }

}
