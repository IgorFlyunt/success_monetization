<?php

namespace App\Traits\Model;

/**
 * Trait StatusTrait
 * @package App\Traits\Model
 */
trait StatusTrait {

    /**
     *
     * Get only disabled records.
     *
     * @param $query
     * @return mixed
     */
    public function scopeDisabled($query) {

        return $query->where("status", false);

    }

    /**
     *
     * Get only active records.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query) {

        return $query->where("status", true);

    }

    /**
     *
     * Check if record is active.
     *
     * @return mixed
     */
    public function isActive() {

        return $this->status;

    }

}
