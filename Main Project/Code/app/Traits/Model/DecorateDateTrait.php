<?php

namespace App\Traits\Model;

use Carbon\Carbon;

/**
 * Trait DecorateDateTrait
 * @package App\Traits\Model
 */
trait DecorateDateTrait {

    /**
     *
     * Query decorator for datetime field.
     *
     * @param $query
     * @param string $field
     * @param string $format
     * @param string $as
     * @return mixed
     */
    public function scopeDecorateDate(
        $query,
        string $field = "created_at",
        string $format = '%d-%m-%Y %H:%i:%s',
        string $as = 'date'
    ) {

        return $query->selectRaw(
            "{$this->getTable()}.*, DATE_FORMAT($field, '$format') as $as"
        );

    }

    /**
     *
     * Get decorated date field.
     *
     * @param string $field
     * @param string $format
     * @param string|null $as
     * @return $this
     */
    public function decorateDate(string $field = "created_at", string $format = "d-m-Y H:i:s", string $as = null) {

        $date = false;

        if(!is_a($this->$field, Carbon::class)) {

            try {

                $date = Carbon::parse($this->$field);

            } catch(Exception $e) {}

        } else $date = $this->$field;

        if($date !== false) {

            $as = $as ?: $field;

            $this->$as = $date->format($format);

        }

        return $this;

    }

}
