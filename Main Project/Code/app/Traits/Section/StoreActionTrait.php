<?php

namespace App\Traits\Section;

/**
 * Trait StoreActionTrait
 * @package App\Traits\Section
 */
trait StoreActionTrait {

    /**
     *
     * Run store route action.
     *
     * @param array $data
     * @return mixed
     */
    public function store(array $data) {

        $method = $this->buildRepositoryMethod(
            __FUNCTION__,
            "create"
        );

        $result = $this->getRepository()->$method(
            $data
        );

        return $this->toJson([
            "status" => (bool)$result,
            "object" => $result
        ]);

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [];

    }

    /**
     *
     * Get message after success execution of store.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function storeSuccess() {

        return trans("t.saved");

    }

}
