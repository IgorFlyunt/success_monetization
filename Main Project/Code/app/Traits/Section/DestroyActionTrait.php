<?php

namespace App\Traits\Section;

/**
 * Trait DestroyActionTrait
 * @package App\Traits\Section
 */
trait DestroyActionTrait {

    /**
     *
     * Run destroy route action.
     *
     * @param $identifier
     * @return mixed
     */
    public function destroy($identifier) {

        $method = $this->buildRepositoryMethod(
            __FUNCTION__,
            "delete"
        );

        $result = $this->getRepository()->$method(
            $identifier
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Get message after success execution of destroy.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function destroySuccess() {

        return trans("t.deleted");

    }

}
