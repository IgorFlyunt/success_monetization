<?php

namespace App\Traits\Section;

use Gate;

/**
 * Trait PermissionsTrait
 * @package App\Traits\Section
 */
trait PermissionsTrait {

    /**
     *
     * Specify if section is for authenticated users only.
     *
     * @return bool
     */
    public function authOnly() : bool {

        return true;

    }

    /**
     *
     * Get array of actions are have \App\Traits\Repository\ForUserMethodsTrait
     *
     * @return array
     */
    public function forUserActions() : array {

        return [
            "index",
            "store",
            "update",
            "destroy",
            "actions"
        ];

    }

    /**
     *
     * Check access to action by policy.
     *
     * @param string $action
     * @return bool
     */
    public function checkAccess(string $action) : bool {

        if($this->authOnly() && !$this->user) {

            return false;

        }

        if($this->user) {

            $policies = Gate::policies();

            $class = get_class($this);

            if(key_exists($class, $policies)) {

                if(method_exists($policies[$class], $action)) {

                    return $this->user->can($action, [
                        $class,
                        $this->request
                    ]);

                }

            }

        }

        return true;

    }

    /**
     *
     * Get repository method for route action.
     *
     * @param string $action
     * @param string $method
     * @return string
     */
    public function buildRepositoryMethod(string $action, string $method) {

        if(in_array($action, $this->forUserActions())) {

            $postfix = "User";

            if(method_exists($this->getRepository(), "getForFieldPostfix")) {

                $postfix = ucfirst(
                    $this->getRepository()->getForFieldPostfix()
                );

            }

            return "{$method}For{$postfix}";

        }

        return $method;

    }

}
