<?php

namespace App\Traits\Section;

/**
 * Trait AliasTrait
 * @package App\Traits\Section
 */
trait AliasTrait
{

    /**
     * @var null
     */
    private static $alias = null;

    /**
     *
     * Get section alias.
     *
     * @return null|string
     */
    public static function getAlias(): ?string
    {

        return self::$alias;

    }

    /**
     *
     * Set section alias.
     *
     * @param string $alias
     */
    public static function setAlias(string $alias)
    {

        self::$alias = $alias;

    }

    /**
     *
     * Build alias from current request uri without disabled parameters.
     *
     * @return null|string|string[]
     */
    public function buildAlias() {

        $parameters = $this->request->route()->parameterNames;

        $index = array_search("section", $parameters);

        if($index > 0) {

            $alias = preg_replace(
                "@(.*\{section\})(.*)@",
                "$1",
                $this->request->route()->uri()
            );

            $parameters = array_slice(
                $parameters,
                0,
                $index + 1
            );

            foreach($parameters as $parameter) {

                $alias = preg_replace(
                    "@\{$parameter\}@",
                    $this->request->route($parameter),
                    $alias
                );

            }

            return $alias;

        }

        return self::$alias;

    }

}
