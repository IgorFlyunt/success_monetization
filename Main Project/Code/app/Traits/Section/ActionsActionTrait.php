<?php

namespace App\Traits\Section;

/**
 * Trait ActionsActionTrait
 * @package App\Traits\Section
 */
trait ActionsActionTrait {

    /**
     *
     * Run actions route action.
     *
     * @param $data
     * @return mixed
     */
    public function actions($data) {

        $method = $this->buildRepositoryMethod(
            __FUNCTION__,
            "doAction"
        );

        $result = $this->getRepository()->$method(
            $data['action'],
            $data['identifier']
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Validate actions route action.
     *
     * @return array
     */
    public function validateActions() {

        return [
            "action" => "required",
            "identifier" => "required"
        ];

    }

    /**
     *
     * Get message after success execution of actions.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function actionsSuccess() {

        return trans("t.success");

    }

}
