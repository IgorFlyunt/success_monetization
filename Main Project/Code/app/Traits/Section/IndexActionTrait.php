<?php

namespace App\Traits\Section;

/**
 * Trait IndexActionTrait
 * @package App\Traits\Section
 */
trait IndexActionTrait {

    /**
     *
     * Run index route action.
     *
     * @return mixed
     */
    public function index() {

        $method = $this->buildRepositoryMethod(
            __FUNCTION__,
            "get"
        );

        $elements = $this->getRepository()->$method();

        return $this->toView([
            "alias" => $this->buildAlias(),
            "elements" => $elements
        ]);

    }

    /**
     *
     * Validate index route action.
     *
     * @return array
     */
    public function validateIndex() {

        return [];

    }

}
