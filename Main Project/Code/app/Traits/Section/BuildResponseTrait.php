<?php

namespace App\Traits\Section;

/**
 * Trait BuildResponseTrait
 * @package App\Traits\Section
 */
trait BuildResponseTrait {

    /**
     *
     * Response data array.
     *
     * @var array
     */
    protected $response_data = array();

    /**
     *
     * Meta data array.
     *
     * @var array
     */
    protected $meta_data = array();

    /**
     *
     * Custom view for next response.
     *
     * @var null
     */
    protected $custom_view = null;

    /**
     *
     * Build response array data.
     *
     * @param array $result
     * @param string $action
     * @return array
     */
    public function buildResponse(array $result, string $action) : array {

        if($result["type"] != "view") {

            return array_merge($result, ["messages" => $this->messages]);

        } elseif(is_string($this->custom_view) && is_null($result["view"])) {

            $result["view"] = $this->custom_view;

        }

        return [
            "type" => "view",
            "view" => view(
                $this->_getViewPath(
                    $result["view"] ?: $action
                ),
                $result["result"]
            ),
            "meta" => $this->buildMetaData(
                $action
            ),
            "messages" => $this->messages
        ];

    }

    /**
     *
     * Decorate data for json response.
     *
     * @param array $parameters
     * @return array
     */
    public function toJson(array $parameters = array()) : array {

        return [
            "type" => "json",
            "result" => array_merge($this->response_data, $parameters)
        ];

    }

    /**
     *
     * Decorate data for view response.
     *
     * @param array $parameters
     * @param string|null $view
     * @return array
     */
    public function toView(array $parameters = array(), string $view = null) : array {

        return [
            "type" => "view",
            "result" => array_merge($this->response_data, $parameters),
            "view" => $view
        ];

    }

    /**
     *
     * Decorate data for redirect response.
     *
     * @param string|null $redirect_to
     * @param bool $error
     * @return array
     */
    public function toRedirect(string $redirect_to = null, bool $error = false) : array {

        return [
            "type" => "redirect",
            "result" => $redirect_to ?: url()->previous(),
            "error" => $error
        ];

    }

    /**
     *
     * Set data to response array by key.
     *
     * @param string $key
     * @param $value
     * @return $this
     */
    public function setResponseData(string $key, $value) {

        $this->response_data[$key] = $value;

        return $this;

    }

    /**
     *
     * Set data to meta array by key.
     *
     * @param $value
     * @param string $key
     * @return $this
     */
    public function setMetaData($value, string $key = 'title') {

        $this->meta_data[$key] = $value;

        return $this;

    }

    /**
     *
     * Build view path for next response.
     *
     * @param string $view
     * @return string
     */
    private function _getViewPath(string $view) : string {

        if($this->views_prefix === false) {

            return $this->views_namespace . $view;

        }

        if(is_null($this->views_prefix)) {

            $this->views_prefix = str_replace(
                '-',
                '_',
                $this::getAlias()
            );

        }

        return $this->views_namespace . "{$this->views_prefix}.$view";

    }

    /**
     *
     * Build meta data for next response.
     *
     * @param string $action
     * @return array
     */
    public function buildMetaData(string $action) {

        $translations = trans(
            $this->buildMetaPrefix(
                null,
                $action
            )
        );

        if(is_array($translations)) {

            return array_merge(
                $this->meta_data,
                array_diff_key(
                    $translations,
                    $this->meta_data
                )
            );

        }

        return $this->meta_data;

    }

    /**
     *
     * Set custom view for next response.
     *
     * @param string $view
     * @return $this
     */
    public function setView(string $view) {

        $this->custom_view = $view;

        return $this;

    }

    /**
     *
     * Build meta prefix if custom meta data are not exists.
     *
     * @param string|null $param
     * @param string|null $action
     * @return string
     */
    public function buildMetaPrefix(string $param = null, string $action = null) {

        $data = [
            "sections",
            str_replace(
                "\\",
                '.',
                str_replace(
                    "App\\Sections\\",
                    '',
                    get_class(
                        $this
                    )
                )
            )
        ];

        if(!is_null($action)) {

            $data[] = $action;

        } else {

            $data[] = $this->request->route()->getActionMethod();

        }

        if(!is_null($param)) {

            $data[] = $param;

        }

        return implode('.', $data);

    }

}
