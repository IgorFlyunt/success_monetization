<?php

namespace App\Traits\Section;

/**
 * Trait EditActionTrait
 * @package App\Traits\Section
 */
trait EditActionTrait {

    /**
     *
     * Run edit route action.
     *
     * @param string $identifier
     * @return mixed
     */
    public function edit(string $identifier) {

        $method = $this->buildRepositoryMethod(
            __FUNCTION__,
            "find"
        );

        $object = $this->getRepository()->$method(
            $identifier
        );

        return $this->toView([
            "alias" => $this->buildAlias(),
            "object" => $object
        ]);

    }

}
