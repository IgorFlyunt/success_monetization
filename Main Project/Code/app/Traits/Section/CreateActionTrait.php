<?php

namespace App\Traits\Section;

/**
 * Trait CreateActionTrait
 * @package App\Traits\Section
 */
trait CreateActionTrait {

    /**
     *
     * Run create route action.
     *
     * @return mixed
     */
    public function create() {

        return $this->toView([
            "alias" => $this->buildAlias()
        ]);

    }

}
