<?php

namespace App\Traits\Section;

/**
 * Trait ValidationTrait
 * @package App\Traits\Section
 */
trait ValidationTrait {

    /**
     *
     * Validate route action wrapper.
     *
     * @param string $action
     */
    public function validateAction(string $action) {

        $method = "validate" . ucfirst($action);

        if(method_exists($this, $method)) {

            $this->request->validate(
                $this->$method(),
                $this->getMessages(),
                $this->getAttributes()
            );

        }

    }

    /**
     *
     * Get custom validation messages.
     *
     * @return array
     */
    public function getMessages() : array {

        return [];

    }

    /**
     *
     * Get custom attributes names.
     *
     * @return array
     */
    public function getAttributes() : array {

        return [];

    }

}
