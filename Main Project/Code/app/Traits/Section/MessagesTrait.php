<?php

namespace App\Traits\Section;

/**
 * Trait MessagesTrait
 * @package App\Traits\Section
 */
trait MessagesTrait {

    /**
     *
     * Set new string to response messages array.
     *
     * @param string $message
     * @param string $type
     * @return $this
     */
    public function setMessage(string $message, string $type = "success") {

        if(!key_exists($type, $this->messages)) {

            $this->messages[$type] = [];

        }

        $this->messages[$type][] = $message;

        return $this;

    }

    /**
     *
     * Get result action message if custom is not exists.
     *
     * @param string $action
     * @param bool $result
     */
    public function loadResultMessage(string $action, bool $result) {

        $type = $result ? "success" : "error";

        $method = $action . ucfirst($type);

        if(method_exists($this, $method)) {

            $this->setMessage(
                $this->$method(),
                $type
            );

        } elseif($type === "error") {

            $this->setMessage(
                trans("t.error"),
                $type
            );

        }

    }

}
