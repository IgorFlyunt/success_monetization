<?php

namespace App\Traits\Section;

/**
 * Trait RepositoriesTrait
 * @package App\Traits\Section
 */
trait RepositoriesTrait {

    /**
     *
     * Sub repositories configuration array.
     *
     * @var array
     */
    private $sub_repositories = [];

    /**
     *
     * Get sub repositories configuration.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            // "key" => [
            //     "repo" => "class",
            //     "model" => "class"
            // ]
        ];

    }

    /**
     *
     * Get sub repository instance by key.
     *
     * @param string $key
     * @return mixed
     */
    public function getSubRepository(string $key) {

        if(!key_exists($key, $this->sub_repositories)) {

            $config = $this->subRepositories()[$key];

            $method = "getSub" . ucfirst(
                strtolower(
                    $key
                )
            ) . "Repository";

            if(method_exists($this, $method)) {

                $this->sub_repositories[$key] = $this->$method(
                    $config["repo"],
                    $config["model"]
                );

            } else {

                $this->sub_repositories[$key] = (new $config["repo"](
                    new $config["model"]
                ));

            }

            $this->sub_repositories[$key]->setUser(
                $this->user
            );

        }

        return $this->sub_repositories[$key];

    }

}
