<?php

namespace App\Traits\Section;

/**
 * Trait UpdateActionTrait
 * @package App\Traits\Section
 */
trait UpdateActionTrait {

    /**
     *
     * Run update route action.
     *
     * @param string $identifier
     * @param array $data
     * @return mixed
     */
    public function update(string $identifier, array $data) {

        $object = $this->getRepository()->find($identifier);

        $method = $this->buildRepositoryMethod(
            __FUNCTION__,
            "updateObject"
        );

        $result = $this->getRepository()->$method(
            $object,
            $data
        );

        return $this->toJson([
            "status" => (bool)$result,
            "object" => $result
        ]);

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        return [];

    }

    /**
     *
     * Get message after success execution of update.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function updateSuccess() {

        return trans("t.updated");

    }

}
