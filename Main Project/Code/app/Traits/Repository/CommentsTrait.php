<?php

namespace App\Traits\Repository;

use App\Models\Comment;

/**
 * Trait CommentsTrait
 * @package App\Traits\Repository
 */
trait CommentsTrait {

    /**
     *
     * Delete comments for related model.
     *
     * @param $ids
     * @return mixed
     */
    public function deleteComments($ids) {

        $ids = is_array($ids) ? $ids : [$ids];

        return Comment::whereIn(
            "modelable_id",
            $ids
        )->where(
            "modelable_type",
            get_class($this->model)
        )->delete();

    }

}
