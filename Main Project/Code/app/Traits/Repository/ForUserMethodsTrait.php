<?php

namespace App\Traits\Repository;

use Exception;

/**
 * Trait ForUserMethodsTrait
 * @package App\Traits\Repository
 */
trait ForUserMethodsTrait {

    use ForFieldMethodsTrait;

    /**
     *
     * Get field name for relation with user.
     *
     * @return string
     */
    public function getForFieldField() : string {

        return "user_id";

    }

    /**
     *
     * Get relation name for user.
     *
     * @return string
     */
    public function getForFieldPostfix() : string {

        return "user";

    }

    /**
     *
     * Get user id if it exists.
     *
     * @return mixed
     * @throws Exception
     */
    public function getForFieldValue() {

        if(!$this->user) {

            throw new Exception("User doesn't exists in this instance!");

        }

        return $this->user->id;

    }

}
