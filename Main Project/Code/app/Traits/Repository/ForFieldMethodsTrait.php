<?php

namespace App\Traits\Repository;

use Closure;
use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait ForFieldMethodsTrait
 * @package App\Traits\Repository
 */
trait ForFieldMethodsTrait {

    /**
     *
     * Get field name for relation.
     *
     * @return string
     */
    abstract public function getForFieldField() : string;

    /**
     *
     * Get relation name for model.
     *
     * @return string
     */
    abstract public function getForFieldPostfix() : string;

    /**
     *
     * Get field value.
     *
     * @return mixed
     */
    abstract public function getForFieldValue();

    /**
     *
     * Get for method postfix.
     *
     * @return string
     */
    private function getPostfix() {

        return ucfirst(
            $this->getForFieldPostfix()
        );

    }

    /**
     *
     * Decorate method name.
     *
     * @param string $method
     * @return mixed
     */
    private function getMethod(string $method) {

        return str_replace(
            $this->getPostfix(),
            '',
            $method
        );

    }

    /**
     *
     * Call action if it's exists.
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments) {

        $method = $this->getMethod($name);

        if(!method_exists($this, $method)) {

            throw new Exception("Method [$method{$this->getPostfix()}] doesn't exists!");

        }

        return call_user_func_array([
            $this,
            $method
        ], $arguments);

    }

    /**
     *
     * Check if record exists for specified relation.
     *
     * @param int $id
     * @return mixed
     */
    private function existsFor(int $id) {

        return $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->where(
            "id",
            $id
        )->exists();

    }

    /**
     *
     * Get one record by id with relations and conditions for specified relation.
     *
     * @param int $id
     * @param array $with
     * @param array $has
     * @return mixed
     */
    private function findFor(int $id, array $with = array(), array $has = array()) {

        $query = $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->with($with);

        $this->_buildHave($query, $has);

        return $query->find($id);

    }

    /**
     *
     * Get records with relations and conditions for specified relation.
     *
     * @param array $with
     * @param array $has
     * @return mixed
     */
    private function getFor(array $with = array(), array $has = array()) {

        $query = $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->with($with);

        $this->_buildHave($query, $has);

        return $query->get();

    }

    /**
     *
     * Delete records by identifiers for specified relation.
     *
     * @param $ids
     * @param bool $force
     * @return mixed
     */
    private function deleteFor($ids, bool $force = false) {

        $method = $force ? "forceDelete" : "delete";

        $ids = is_array($ids) ? $ids : [$ids];

        $this->dispatchEvent(
            "deleted",
            $ids,
            $this->{$this->getForFieldPostfix()},
            $force
        );

        return $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->whereIn("id", $ids)->$method();

    }

    /**
     *
     * Update or create record for relation and save changes to database.
     *
     * @param array $data
     * @param array $selector
     * @return mixed
     */
    private function updateOrCreateFor(array $data, array $selector = []) {

        return $this->model->updateOrCreate(array_merge([
            $this->getForFieldField() => $this->getForFieldValue()
        ], $selector), $data);

    }

    /**
     *
     * Create record for relation and save changes to database.
     *
     * @param array $data
     * @return mixed
     */
    private function createFor(array $data) {

        $data[$this->getForFieldField()] = $this->getForFieldValue();

        $object = $this->model->create($data);

        if($this->hasEvent("saved")) {

            $this->dispatchEvent(
                "saved",
                $object,
                $this->{$this->getForFieldPostfix()}
            );

        }

        return $object;

    }

    /**
     *
     * Update record by identifier for relation and save changes to database.
     *
     * @param $identifier
     * @param array $data
     * @return mixed
     */
    private function updateFor($identifier, array $data) {

        $identifier = is_array($identifier) ? $identifier : [$identifier];

        return $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->whereIn(
            "id",
            $identifier
        )->update($data);

    }

    /**
     *
     * Update object for relation and save changes to database.
     *
     * @param Model $object
     * @param array $data
     * @return bool|Model
     */
    private function updateObjectFor(Model $object, array $data) {

        if($object->{$this->getForFieldField()} == $this->getForFieldValue()) {

            if($object->fill($data)->save()) {

                $this->dispatchEvent(
                    "saved",
                    $object,
                    $this->{$this->getForFieldPostfix()}
                );

            }

            return $object;

        }

        return false;

    }

    /**
     *
     * Wrapper for calling "actions" methods by action name and identifiers for relation.
     *
     * @param string $action
     * @param $identifier
     * @return mixed
     */
    private function doActionFor(string $action, $identifier) {

        $identifier = is_array($identifier) ? $identifier : [$identifier];

        if(in_array($action, ["delete", "destroy"])) {

            $method = "deleteFor{$this->getPostfix()}";

        } else {

            $method = "{$action}ActionFor{$this->getPostfix()}";

        }

        return $this->$method($identifier);

    }

    /**
     *
     * Pluck records by fields and condition for relation.
     *
     * @param string $column
     * @param string|null $key
     * @param Closure|null $closure
     * @return mixed
     */
    private function pluckFor(string $column, string $key = null, Closure $closure = null) {

        $query = $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        );

        if(is_callable($closure)) {

            $query->where($closure);

        }

        return $query->pluck($column, $key);

    }

    /**
     *
     * Get only deleted records with relations and conditions for relation.
     *
     * @param array $with
     * @param array $has
     * @return mixed
     */
    private function getDeletedFor(array $with = array(), array $has = array()) {

        $query = $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->onlyTrashed()->with($with);

        $this->_buildHave($query, $has);

        return $query->get();

    }

    /**
     *
     * Recover records by identifiers for relation.
     *
     * @param $ids
     * @return mixed
     */
    private function recoverActionFor($ids) {

        return $this->model->where(
            $this->getForFieldField(),
            $this->getForFieldValue()
        )->whereIn(
            "id",
            $ids
        )->onlyTrashed()->restore();

    }

    /**
     *
     * Load relation for model object for relation.
     *
     * @param Model $object
     * @param string $relation
     */
    private function loadRelationFor(Model $object, string $relation) {

        if($object->{$this->getForFieldField()} == $this->getForFieldValue()) {

            $object->load($relation);

        }

    }

}
