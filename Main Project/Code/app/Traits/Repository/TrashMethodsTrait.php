<?php

namespace App\Traits\Repository;

/**
 * Trait TrashMethodsTrait
 * @package App\Traits\Repository
 */
trait TrashMethodsTrait {

    /**
     *
     * Force delete records for user.
     *
     * @return bool
     */
    public function emptyTrashForUser() {

        $ids = $this->model->where(
            "user_id",
            $this->user->id
        )->onlyTrashed()->pluck(
            "id"
        )->toArray();

        if(sizeof($ids)) {

            return $this->deleteForUser(
                $ids,
                true
            );

        }

        return true;

    }

    /**
     *
     * Check if soft deleted records exists for user.
     *
     * @param int $id
     * @return mixed
     */
    public function existsTrashedForUser(int $id) {

        return $this->model->onlyTrashed()->where(
            "user_id",
            $this->user->id
        )->where(
            "id",
            $id
        )->exists();

    }

}
