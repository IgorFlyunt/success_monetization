<?php

namespace App\Traits\Repository;

/**
 * Trait AdvertisementTypesTrait
 * @package App\Traits\Repository
 */
trait AdvertisementTypesTrait {

    /**
     *
     * Get array of advertisements types.
     *
     * @return array|\Illuminate\Config\Repository|mixed|null
     */
    public function getAdvertisementsTypes() {

        $types = config("advertisement.types", []);

        $types = array_flip($types);

        foreach($types as $type => &$label) {

            $label = trans("t.advertisements.types.$type");

        }

        return $types;

    }

}
