<?php

namespace App\Traits\Repository\Site\Advertisements;

use App\Models\Site\Advertisement;

/**
 * Trait SaveTrait
 * @package App\Traits\Repository\Site\Advertisements
 */
trait SaveTrait {

    /**
     *
     * Check if Advertisement has any url.
     *
     * @param array $advertisements
     * @return bool
     */
    public function checkTargetIsNotEmpty(array $advertisements) {

        extract( //get posts, categories and urls arrays here
            reset(
                $advertisements
            )
        );

        $urls = array_filter($urls, function($url) {

            return $url["exclude"] == false;

        });

        if(empty($posts["include"]) &&
           empty($categories["exact"]) &&
           empty($categories["paths"]) &&
           empty($urls)) {

            return false;

        }

        return true;

    }

    /**
     *
     * Attach new relations.
     *
     * @param string $relation
     * @param array $data
     * @param Advertisement $object
     * @param string $field
     * @param bool $update
     */
    private function attachAdvertisementRelation(
        string $relation,
        array $data,
        Advertisement $object,
        string $field,
        bool $update = false
    ) {

        if($update) {

            $passed = array_merge(
                ...array_values(
                    $data
                )
            );

            $ids = $object->$relation()->pluck(
                "id"
            )->toArray();

            $detach = array_diff($ids, $passed);

            if(sizeof($detach)) {

                $object->$relation()->detach($detach);

            }

        }

        $sync_data = [];

        foreach($data as $compare => $values) {

            foreach($values as $value) {

                $sync_data[$value] = [$field => $compare == $field];

            }

        }

        if(sizeof($sync_data)) {

            $object->$relation()->sync($sync_data);

        }

    }

    /**
     *
     * Save urls list for Advertisement.
     *
     * @param array $urls
     * @param Advertisement $object
     * @param bool $update
     */
    private function saveUrls(array $urls, Advertisement $object, bool $update = false) {

        if($update) {

            foreach([true, false] as $exclude) {

                $passed = array_map(function($url) {

                    return $url["url"];

                }, array_filter($urls, function($url) use ($exclude) {

                    return $url["exclude"] == $exclude;

                }));

                $object->urls()->whereNotIn(
                    "url",
                    $passed
                )->where(
                    "exclude",
                    $exclude
                )->delete();

            }

        }

        foreach($urls as $url) {

            $object->urls()->updateOrCreate([
                "advertisement_id" => $object->id,
                "exact" => $url["exact"],
                "url" => $url["url"]
            ], ["exclude" => $url["exclude"]]);

        }

    }

    /**
     *
     * Save position for Advertisement.
     *
     * @param array $position
     * @param Advertisement $object
     */
    private function savePosition(array $position, Advertisement $object) {

        $object->position()->updateOrCreate([
            "advertisement_id" => $object->id
        ], array_intersect_key($position, array_flip([
            "main",
            "sub"
        ])));

    }

    /**
     *
     * Save tracker for Advertisement.
     *
     * @param array $tracker
     * @param Advertisement $object
     */
    private function saveTracker(array $tracker, Advertisement $object) {

        if(empty($tracker)) {

            $object->tracker()->delete();

        } else {

            $object->tracker()->updateOrCreate([
                "advertisement_id" => $object->id
            ], [
                "utm_campaign" => $tracker["utm_campaign"],
                "tracker_id" => $tracker["id"],
                "utm_medium" => $tracker["utm_medium"],
                "utm_source" => $tracker["utm_source"],
                "sid1" => $tracker["sid1"],
                "sid2" => $tracker["sid2"],
                "sid3" => $tracker["sid3"]
            ]);

        }

    }

}
