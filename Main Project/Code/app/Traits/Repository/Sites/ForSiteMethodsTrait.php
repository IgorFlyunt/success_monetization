<?php

namespace App\Traits\Repository\Sites;

use Exception;
use App\Models\Site;
use App\Traits\Repository\ForFieldMethodsTrait;

/**
 * Trait ForSiteMethodsTrait
 * @package App\Traits\Repository\Sites
 */
trait ForSiteMethodsTrait {

    use ForFieldMethodsTrait;

    /**
     * @var Site
     */
    protected $site;

    /**
     *
     * Set new Site instance.
     *
     * @param Site $site
     * @return $this
     */
    public function setSite(Site $site) {

        $this->site = $site;

        if($site->exists) {

            app("tenant")->initializeDatabase(
                $site->user_id
            );

        }

        return $this;

    }

    /**
     *
     * Get field name for relation with site.
     *
     * @return string
     */
    public function getForFieldField() : string {

        return "site_id";

    }

    /**
     *
     * Get relation name for site.
     *
     * @return string
     */
    public function getForFieldPostfix() : string {

        return "site";

    }

    /**
     *
     * Get site id if it exists.
     *
     * @return mixed
     * @throws Exception
     */
    public function getForFieldValue() {

        if(!$this->site) {

            throw new Exception("Site doesn't exists in this instance!");

        }

        return $this->site->id;

    }

}
