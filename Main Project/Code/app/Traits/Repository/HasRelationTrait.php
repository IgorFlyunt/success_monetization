<?php

namespace App\Traits\Repository;

use Illuminate\Support\Facades\DB;

/**
 * Trait HasRelationTrait
 * @package App\Traits\Repository
 */
trait HasRelationTrait {

    /**
     *
     * Get subquery to check if record has specified relation.
     *
     * @param string $current_table
     * @param string $current_table_field
     * @param string $target_table
     * @param string $target_table_field
     * @param string|null $where
     * @return string
     */
    public function getHasRelationSubquery(
        string $current_table,
        string $current_table_field,
        string $target_table,
        string $target_table_field,
        string $where = null
    ) {

        $as = "sub_$target_table";

        $query = DB::table("$target_table as $as")->selectRaw(
            "COUNT($as.$target_table_field)"
        )->whereRaw(
            "$as.$target_table_field  = $current_table.$current_table_field"
        );

        if(is_string($where)) {

            $query = $query->whereRaw($where);

        }

        $query = $query->toSql();

        return "(IF(($query) > 0, 1, 0))";

    }

}
