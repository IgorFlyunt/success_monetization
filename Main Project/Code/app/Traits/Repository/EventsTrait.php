<?php

namespace App\Traits\Repository;

/**
 * Trait EventsTrait
 * @package App\Traits\Repository
 */
trait EventsTrait {

    /**
     *
     * List of available events.
     *
     * @var array
     */
    public $events = [
        // "key" => "event_class"
    ];

    /**
     *
     * Dispatch event by key.
     *
     * @param string $key
     */
    public function dispatchEvent(string $key) {

        if($this->hasEvent($key)) {

            $class = $this->events[$key];

            event(
                new $class(
                    ...array_slice(
                        func_get_args(),
                        1
                    )
                )
            );

        }

    }

    /**
     *
     * Check if event is exists for repository.
     *
     * @param string $key
     * @return bool
     */
    public function hasEvent(string $key) {

        return key_exists($key, $this->events);

    }

}
