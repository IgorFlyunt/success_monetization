<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\User\DeletedEvent::class => [
            \App\Listeners\User\DeletedListener::class
        ],
        \App\Events\Site\Advertisements\SavedEvent::class => [
            \App\Listeners\Site\Advertisements\SavedListener::class
        ],
        \App\Events\Site\Advertisements\DeletedEvent::class => [
            \App\Listeners\Site\Advertisements\DeletedListener::class
        ],
        \App\Events\Site\DeletedEvent::class => [
            \App\Listeners\Site\DeletedListener::class
        ],
        \App\Events\Group\DeletedEvent::class => [
            \App\Listeners\Group\DeletedListener::class
        ],
        \App\Events\Site\SynchronizedEvent::class => [
            \App\Listeners\Site\SynchronizedListener::class
        ],
        \App\Events\Synchronization\SavedEvent::class => [
            \App\Listeners\Synchronization\SavedListener::class
        ],
        \App\Events\Advertisement\SavedEvent::class => [
            \App\Listeners\Advertisement\SavedListener::class
        ],
        \App\Events\Advertisement\DeletedEvent::class => [
            \App\Listeners\Advertisement\DeletedListener::class
        ],
        \App\Events\Site\RunnedEvent::class => [
            \App\Listeners\Site\RunnedListener::class
        ],
        \App\Events\Site\Advertisements\RunnedEvent::class => [
            \App\Listeners\Site\Advertisements\RunnedListener::class
        ],
        \App\Events\Site\SavedEvent::class => [
            \App\Listeners\Site\SavedListener::class
        ],
        \App\Events\Site\Advertisements\RemovedFromGroupEvent::class => [
            \App\Listeners\Site\Advertisements\RemovedFromGroupListener::class
        ],
        \App\Events\Tracker\SavedEvent::class => [
            \App\Listeners\Tracker\SavedListener::class
        ],
        \App\Events\Tracker\DeletedEvent::class => [
            \App\Listeners\Tracker\DeletedListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
