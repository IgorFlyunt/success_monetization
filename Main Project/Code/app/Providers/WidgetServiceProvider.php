<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

/**
 * Class WidgetServiceProvider
 * @package App\Providers
 */
class WidgetServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * Add new blade directive to render widget instances.
     *
     * @return void
     */
    public function boot()
    {

        if(($directive = config("widget.blade_directive"))) {

            view()->addNamespace("widgets", config("widget.views", ''));

            Blade::directive($directive, function($data) {

                return "<?php echo \App\Classes\Widgets\Loader::load($data); ?>";

            });

        }

    }

}
