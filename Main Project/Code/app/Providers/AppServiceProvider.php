<?php

namespace App\Providers;

use App\Classes\Helpers\Meta;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use App\Classes\MultiTenant\Loader as TenantLoader;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->configureViews();

        $this->configureMeta();

        $this->configureTenant();

        $this->configureQueue();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {}

    /**
     *
     * Add additional namespace to views provider.
     *
     */
    private function configureViews() {

        view()->addNamespace(
            'dashboard',
            resource_path("themes/success-monetization")
        );

    }

    /**
     *
     * Add App\Classes\Helpers\Meta instance to service container.
     *
     */
    private function configureMeta() {

        $this->app->singleton(
            "meta",
            function ($app) {

                return new Meta();

            }
        );

    }

    /**
     *
     * Add App\Classes\MultiTenant\Loader instance to service container.
     *
     */
    private function configureTenant() {

        $this->app->singleton(
            "tenant",
            function ($app) {

                return new TenantLoader();

            }
        );

    }

    /**
     *
     * Add pre handle hook to queued listeners which must have loaded tenant configuration.
     * The "tenant_id" field must be in a public scope in the event class.
     *
     */
    private function configureQueue() {

        Queue::before(function (JobProcessing $event) {

            preg_match("/\"tenant_id\";i:(\d+);/", $event->job->payload()["data"]["command"], $matches);

            if(key_exists(1, $matches)) {

                app("tenant")->initializeDatabase(
                    $matches[1]
                );

            }

        });

    }

}
