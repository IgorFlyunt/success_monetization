<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Sections\SitesSection::class => \App\Policies\Sections\SitesPolicy::class,
        \App\Sections\GroupsSection::class => \App\Policies\Sections\GroupsPolicy::class,
        \App\Sections\TrackersSection::class => \App\Policies\Sections\TrackersPolicy::class,
        \App\Sections\CommentsSection::class => \App\Policies\Sections\CommentsPolicy::class,
        \App\Sections\Site\SynchronizationSection::class => \App\Policies\Sections\Site\SynchronizationPolicy::class,
        \App\Sections\CategoriesSection::class => \App\Policies\Sections\CategoriesPolicy::class,
        \App\Sections\Trash\SitesSection::class => \App\Policies\Sections\Trash\SitesPolicy::class,
        \App\Sections\Trash\AdvertisementsSection::class => \App\Policies\Sections\Trash\AdvertisementsPolicy::class,
        \App\Sections\Advertisements\ContextsSection::class => \App\Policies\Sections\Advertisements\ContextsPolicy::class,
        \App\Sections\Site\Advertisements\ContextsSection::class => \App\Policies\Sections\Site\Advertisements\ContextsPolicy::class,
        \App\Sections\Site\CommentsSection::class => \App\Policies\Sections\Site\CommentsPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
