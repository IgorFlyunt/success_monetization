<?php

namespace App\Providers;

use App\Exceptions\DuplicateSectionAliasException;
use Illuminate\Support\ServiceProvider;

/**
 * Class SectionServiceProvider
 * @package App\Providers
 */
class SectionServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Sections\SitesSection::class,
        \App\Sections\GroupsSection::class,
        \App\Sections\CommentsSection::class,
        \App\Sections\TrackersSection::class,
        \App\Sections\CategoriesSection::class,
        \App\Sections\Trash\SitesSection::class,
        \App\Sections\Site\CommentsSection::class,
        \App\Sections\Site\SynchronizationSection::class,
        \App\Sections\Trash\AdvertisementsSection::class,
        \App\Sections\Advertisements\ContextsSection::class,
        \App\Sections\Site\Advertisements\ContextsSection::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * Add sections instance to the app service container.
     *
     * @return void
     */
    public function boot()
    {


        $this->app->singleton("sections", function() {

            return $this->_loadSections();

        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        //

    }

    /**
     *
     * Build array of sections with their aliases as a keys.
     *
     * @return array
     * @throws DuplicateSectionAliasException
     */
    private function _loadSections() {

        $sections = array();

        foreach($this->sections as $section) {

            $alias = $section::getAlias();

            if(!is_string($alias)) {

                $alias = snake_case(
                    class_basename(
                        $section
                    )
                );

                $alias = explode('_', $alias);

                $alias = array_splice($alias, 0, -1);

                $alias = implode('-', $alias);

            }

            if(key_exists($alias, $sections)) {

                throw new DuplicateSectionAliasException(
                    $alias,
                    $section
                );

            }

            $sections[$alias] = $section;

        }

        return $sections;

    }

}
