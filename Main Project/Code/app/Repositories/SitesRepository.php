<?php

namespace App\Repositories;

use App\Models\Site;
use Illuminate\Support\Facades\DB;
use App\Jobs\Site\SynchronizationJob;
use App\Traits\Repository\TrashMethodsTrait;
use App\Traits\Repository\ForUserMethodsTrait;
use App\Events\Site\SavedEvent as SiteSavedEvent;
use App\Events\Site\RunnedEvent as SiteRunnedEvent;
use App\Events\Site\DeletedEvent as SiteDeletedEvent;

/**
 * Class SitesRepository
 * @package App\Repositories
 */
class SitesRepository extends AbstractRepository
{

    use TrashMethodsTrait;
    use ForUserMethodsTrait;

    /**
     * @var array
     */
    public $events = [
        "deleted" => SiteDeletedEvent::class,
        "runned" => SiteRunnedEvent::class,
        "saved" => SiteSavedEvent::class
    ];

    /**
     * SitesRepository constructor.
     * @param Site $model
     */
    function __construct(Site $model)
    {

        parent::__construct($model);

    }

    /**
     *
     * Get records with advertisement status for DataTable for specified user.
     *
     * @return mixed
     */
    public function getForTableForUser()
    {

        $table = get_tenant_db_name(
            "advertisements"
        );

        $subquery = DB::table(
            $table
        )->selectRaw(
            "COUNT($table.id)"
        )->whereRaw(
            "$table.site_id = sites.id"
        )->whereExists(function ($query) {

            return $query->selectRaw('main_advertisements.id')->from(
                get_main_db_name("advertisements") . ' as main_advertisements'
            )->whereRaw(
                "advertisements.advertisement_id = main_advertisements.id"
            )->whereRaw(
                "main_advertisements.status = 1"
            )->whereRaw(
                "ISNULL(main_advertisements.deleted_at)"
            );

        })->where(function($query) {

            return $query->whereNotNull("url")->orWhereExists(function($query) {

                return $query->selectRaw('trackers.id')->from(
                    get_tenant_db_name("trackers")
                )->whereRaw(
                    "trackers.advertisement_id = advertisements.id"
                );

            });

        })->limit(1)->toSql();

        return $this->model->selectRaw(
            "{$this->model->getTable()}.*, ($subquery) as has_advertisements"
        )->with([
            "user",
            "synchronization"
        ])->where(
            "user_id",
            $this->user->id
        )->withStatus()->get();

    }

    /**
     *
     * Update advertisements by site identifiers for specified user.
     *
     * @param array $site_ids
     * @param array $data
     * @return int
     */
    public function updateAdvertisementsForUser(array $site_ids, array $data)
    {

        return DB::table(
            get_tenant_db_name(
                "advertisements"
            )
        )->whereNUll(
            "deleted_at"
        )->whereIn(
            "site_id",
            $site_ids
        )->whereExists(function ($query) use ($site_ids) {

            return $query->selectRaw('sites.id')->from(
                'sites'
            )->whereRaw(
                "sites.id IN (" . implode(',', $site_ids) . ")"
            )->whereRaw(
                "sites.user_id = {$this->user->id}"
            );

        })->whereExists(function ($query) {

            return $query->selectRaw('main_advertisements.id')->from(
                get_main_db_name("advertisements") . ' as main_advertisements'
            )->whereRaw(
                "advertisements.advertisement_id = main_advertisements.id"
            )->whereRaw(
                "main_advertisements.status = 1"
            )->whereRaw(
                "ISNULL(main_advertisements.deleted_at)"
            );

        })->where(function($query) {

            return $query->whereNotNull("url")->orWhereExists(function($query) {

                return $query->selectRaw('trackers.id')->from(
                    get_tenant_db_name("trackers")
                )->whereRaw(
                    "trackers.advertisement_id = advertisements.id"
                );

            });

        })->update($data);

    }

    /**
     *
     * Launch advertisements by site identifiers for specified user.
     *
     * @param array $ids
     * @return int
     */
    public function runActionForUser(array $ids)
    {

        $this->dispatchEvent(
            "runned",
            $ids,
            $this->user
        );

        $result = $this->updateAdvertisementsForUser($ids, [
            "status" => true
        ]);

        return $result;

    }

    /**
     *
     * Disable advertisements by site identifiers for specified user.
     *
     * @param array $ids
     * @return int
     */
    public function stopActionForUser(array $ids)
    {

        $result = $this->updateAdvertisementsForUser($ids, [
            "status" => false
        ]);

        $this->dispatchEvent(
            "deleted",
            $ids,
            $this->user
        );

        return $result;

    }

    /**
     *
     * Launch queued jobs for synchronization by site identifiers for specified user.
     *
     * @param array $ids
     * @return bool
     */
    public function syncActionForUser(array $ids)
    {

        $objects = $this->model->with(["synchronization"])->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->has(
            "synchronization"
        )->get();

        if (sizeof($objects)) {

            foreach ($objects as $object) {

                SynchronizationJob::dispatch($object);

            }

            return true;

        }

        return false;

    }

    /**
     *
     * Cron command to launch synchronization job.
     *
     */
    public function runSynchronization()
    {

        $oldest_than = function ($query) {

            foreach (config("synchronization.query_intervals", []) as $key => $interval) {

                $method = $key > 0 ? "orWhereRaw" : "whereRaw";

                $query->$method(
                    "synchronized_at <= DATE_SUB(NOW(), INTERVAL $interval)"
                );

            }

            return $query;

        };

        $this->model->with(["synchronization"])->whereHas("synchronization", function ($query) use ($oldest_than) {

            return $query->where(function ($query) use ($oldest_than) {

                return $query->whereNull(
                    "synchronized_at"
                )->orWhere(
                    $oldest_than
                );

            });

        })->each(function ($object) {

            SynchronizationJob::dispatch($object);

        });

    }

    /**
     *
     * Delete records by identifiers for specified user.
     *
     * @param $ids
     * @param bool $force
     * @return mixed
     */
    public function deleteForUser($ids, bool $force = false)
    {

        $method = $force ? "forceDelete" : "delete";

        $ids = is_array($ids) ? $ids : [$ids];

        if (!$force) {

            $this->updateAdvertisementsForUser($ids, [
                "status" => false
            ]);

        }

        $this->dispatchEvent(
            "deleted",
            $ids,
            $this->user,
            $force
        );

        return $this->model->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->$method();

    }

    /**
     *
     * Get records to delete by identifiers for specified user.
     *
     * @param array $ids
     * @return mixed
     */
    public function getForUserForDelete(array $ids)
    {

        return $this->model->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->withTrashed()->get();

    }

    /**
     *
     * Get records to launch by identifiers for specified user.
     *
     * @param array $ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function getForUserForRunned(array $ids)
    {

        return $this->model->with([
            "synchronization"
        ])->where(
            "user_id",
            $this->user->id
        )->whereIn(
            "id",
            $ids
        )->get();

    }

}
