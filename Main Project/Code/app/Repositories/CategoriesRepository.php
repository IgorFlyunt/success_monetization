<?php

namespace App\Repositories;

use App\Models\Category;
use App\Traits\Repository\ForUserMethodsTrait;

/**
 * Class CategoriesRepository
 * @package App\Repositories
 */
class CategoriesRepository extends AbstractRepository {

    use ForUserMethodsTrait;

    /**
     * CategoriesRepository constructor.
     * @param Category $model
     */
    function __construct(Category $model) {

        parent::__construct($model);

    }

}
