<?php

namespace App\Repositories;

use App\Models\Group;
use App\Traits\Repository\ForUserMethodsTrait;
use App\Traits\Repository\AdvertisementTypesTrait;
use App\Events\Group\DeletedEvent as GroupDeletedEvent;

/**
 * Class GroupsRepository
 * @package App\Repositories
 */
class GroupsRepository extends AbstractRepository
{

    use ForUserMethodsTrait;
    use AdvertisementTypesTrait;

    /**
     * @var array
     */
    public $events = [
        "deleted" => GroupDeletedEvent::class
    ];

    /**
     * GroupsRepository constructor.
     * @param Group $model
     */
    function __construct(Group $model) {

        parent::__construct($model);

    }

    /**
     *
     * Create new record and save changes to database for specified user.
     *
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function createForUser(array $data) {

        $data["user_id"] = $this->user->id;

        return transaction_wrapper([
            "class" => $this,
            "method" => "saveChanges"
        ], [$data]);

    }

    /**
     *
     * Save and update functionality.
     *
     * @param array $data
     * @param Group|null $model
     * @return Group
     */
    public function saveChanges(array $data, Group $model = null) {

        $advertisements = [];

        if(key_exists("advertisements", $data) && is_array($data["advertisements"])) {

            $advertisements = $data["advertisements"];

        }

        unset($data["advertisements"]);

        if(is_null($model)) {

            $group = $this->model->create($data);

        } else {

            $group = $model;

            $group->update($data);

        }

        $this->attachRelation(
            "advertisements",
            $group,
            $advertisements,
            !is_null($model)
        );

        return $group;

    }

    /**
     *
     * Update specified object and save changes to database for user.
     *
     * @param $object
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function updateObjectForUser($object, array $data) {

        if($object->user_id == $this->user->id) {

            return transaction_wrapper([
                "class" => $this,
                "method" => "saveChanges"
            ], [$data, $object]);

        }

        return false;

    }

}
