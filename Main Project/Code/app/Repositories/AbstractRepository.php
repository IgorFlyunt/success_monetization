<?php

namespace App\Repositories;

use Closure;
use App\Models\User;
use App\Traits\Repository\EventsTrait;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Repository\RepositoryInterface;

/**
 * Class AbstractRepository
 * @package App\Repositories
 */
abstract class AbstractRepository implements RepositoryInterface {

    use EventsTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var
     */
    protected $user;

    /**
     * AbstractRepository constructor.
     * @param Model $model
     */
    function __construct(Model $model) {

        $this->model = $model;

    }

    /**
     * @return string
     */
    public function getModelClass() {

        return get_class(
            $this->model
        );

    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(User $user = null) {

        $this->user = $user;

        if($this->user) {

            app("tenant")->initializeDatabase(
                $this->user->id
            );

        }

        return $this;

    }

    /**
     *
     * Builds query with "has" condition.
     *
     * @param $query
     * @param array $has
     */
    protected function _buildHave(&$query, array $has = array()) {

        foreach ($has as $relation => $must_have) {

            if (!is_callable($must_have)) {

                $query->has($must_have);

            } else {

                $query->whereHas($relation, $must_have);

            }

        }

    }

    /**
     *
     * Check if record exists.
     *
     * @param int $id
     * @return mixed
     */
    public function exists(int $id) {

        return $this->model->where("id", $id)->exists();

    }

    /**
     *
     * Returns one record with the specified relations and specified conditions.
     *
     * @param int $id
     * @param array $with
     * @param array $has
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|Model[]|null
     */
    public function find(int $id, array $with = array(), array $has = array()) {

        $query = $this->model->with($with);

        $this->_buildHave($query, $has);

        return $query->find($id);

    }


    /**
     *
     * Returns all records with the specified relations and specified conditions.
     *
     * @param array $with
     * @param array $has
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function get(array $with = array(), array $has = array()) {

        $query = $this->model->with($with);

        $this->_buildHave($query, $has);

        return $query->get();

    }

    /**
     *
     * Delete records by identifiers.
     *
     * @param $ids
     * @param bool $force
     * @return mixed
     */
    public function delete($ids, bool $force = false) {

        $method = $force ? "forceDelete" : "delete";

        $ids = is_array($ids) ? $ids : [$ids];

        $this->dispatchEvent(
            "deleted",
            $ids,
            $force
        );

        return $this->model->whereIn("id", $ids)->$method();

    }

    /**
     *
     * Create and save to database new record.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {

        $object = $this->model->create($data);

        if($this->hasEvent("saved")) {

            $this->dispatchEvent(
                "saved",
                $object
            );

        }

        return $object;

    }

    /**
     *
     * Update if exists or create new record.
     *
     * @param array $selector
     * @param array $data
     * @return mixed
     */
    public function updateOrCreate(array $selector, array $data) {

        return $this->model->updateOrCreate($selector, $data);

    }

    /**
     *
     * Update new record by identifier and data.
     *
     * @param $identifier
     * @param array $data
     * @return mixed
     */
    public function update($identifier, array $data) {

        $identifier = is_array($identifier) ? $identifier : [$identifier];

        return $this->model->whereIn(
            "id",
            $identifier
        )->update($data);

    }

    /**
     *
     * Update passed object and save changes to database.
     *
     * @param $object
     * @param array $data
     * @return mixed
     */
    public function updateObject($object, array $data) {

        $object->fill($data)->save();

        return $object;

    }

    /**
     *
     * Wrapper for calling "actions" methods by action name and identifiers.
     *
     * @param string $action
     * @param $identifier
     * @return mixed
     */
    public function doAction(string $action, $identifier) {

        $identifier = is_array($identifier) ? $identifier : [$identifier];

        if(in_array($action, ["delete", "destroy"])) {

            $method = "delete";

        } else {

            $method = "{$action}Action";

        }

        return $this->$method($identifier);

    }

    /**
     *
     * Pluck records by fields and condition.
     *
     * @param string $column
     * @param string|null $key
     * @param Closure|null $closure
     * @return mixed
     */
    public function pluck(string $column, string $key = null, Closure $closure = null) {

        $query = $this->model;

        if(is_callable($closure)) {

            $query->where($closure);

        }

        return $query->pluck($column, $key);

    }

    /**
     *
     * Get only deleted records with relations and conditions.
     *
     * @param array $with
     * @param array $has
     * @return mixed
     */
    public function getDeleted(array $with = array(), array $has = array()) {

        $query = $this->model->with(
            $with
        )->onlyTrashed();

        $this->_buildHave($query, $has);

        return $query->get();

    }

    /**
     *
     * Recover records by identifiers.
     *
     * @param $ids
     * @return mixed
     */
    public function recoverAction($ids) {

        return $this->model->whereIn(
            "id",
            $ids
        )->onlyTrashed()->restore();

    }

    /**
     *
     * Decorate passed data by specified decorator class.
     *
     * @param $data
     * @param string $decorator
     * @return mixed
     */
    public function decorate($data, string $decorator) {

        return (new $decorator($data))->decorate();

    }

    /**
     *
     * Attach relation to model object.
     *
     * @param string $relation
     * @param Model $model
     * @param array $values
     * @param bool $update
     * @param string $field
     * @param array $additional_values
     */
    public function attachRelation(
        string $relation,
        Model $model,
        array $values,
        bool $update,
        string $field = "id",
        array $additional_values = []
    ) {

        if($update) {

            $ids = $model->$relation()->pluck(
                $field
            )->toArray();

            $detach = array_diff($ids, $values);

            $values = array_diff($values, $ids);

            if(sizeof($detach)) {

                $model->$relation()->detach($detach);

            }

        }

        $model->$relation()->attach($values, $additional_values);

    }

    /**
     *
     * Load relation for model object.
     *
     * @param Model $object
     * @param $relation
     */
    public function loadRelation(Model $object, $relation) {

        $object->load($relation);

    }

}
