<?php

namespace App\Repositories;

use App\Models\User;
use App\Events\User\DeleteEvent;

/**
 * Class UsersRepository
 * @package App\Repositories
 */
class UsersRepository extends AbstractRepository {

    /**
     * UsersRepository constructor.
     * @param User $model
     */
    function __construct(User $model) {

        parent::__construct($model);

    }

    /**
     *
     * Delete records by identifiers.
     *
     * @param $ids
     * @param bool $force
     * @return mixed
     */
    public function delete($ids, bool $force = false) {

        event(
            new DeleteEvent(
                $this->pluck("id", null, function($query) use ($ids) {

                    return $query->whereIn("id", $ids);

                })->toArray()
            )
        );

        return parent::delete($ids, $force);

    }

}
