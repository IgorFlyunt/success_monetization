<?php

namespace App\Repositories\Site;

use App\Models\Site\Advertisement;
use App\Events\Site\Advertisements\SavedEvent;
use App\Events\Site\Advertisements\RunnedEvent;
use App\Events\Site\Advertisements\DeletedEvent;
use App\Traits\Repository\AdvertisementPositionsTrait;
use App\Traits\Repository\Sites\ForSiteMethodsTrait;
use App\Events\Site\Advertisements\RemovedFromGroupEvent;
use App\Repositories\AbstractRepository as BaseAbstractRepository;
use App\Traits\Repository\Site\Advertisements\SaveTrait as SaveAdvertisementsTrait;

/**
 * Class AdvertisementsRepository
 * @package App\Repositories\Site
 */
class AdvertisementsRepository extends BaseAbstractRepository {

    use AdvertisementPositionsTrait;
    use SaveAdvertisementsTrait;
    use ForSiteMethodsTrait;

    /**
     * @var array
     */
    public $events = [
        "saved" => SavedEvent::class,
        "deleted" => DeletedEvent::class,
        "runned" => RunnedEvent::class,
        "removedFromGroup" => RemovedFromGroupEvent::class
    ];

    /**
     * AdvertisementsRepository constructor.
     * @param Advertisement $model
     */
    function __construct(Advertisement $model) {

        parent::__construct($model);

    }

    /**
     *
     * Returns records for DataTable which are related to specified site.
     *
     * @param string $type
     * @param array $filter
     * @return mixed
     */
    public function getForTableForSite(string $type, array $filter = []) {

        $query = $this->model->getParentId()->hasTracker()->with([
            "group",
            "advertisement",
            "advertisement.categories"
        ])->where(
            "type",
            $type
        )->where(
            "site_id",
            $this->site->id
        );

        if(key_exists("type", $filter)) {

            if($filter["type"] == "positions") {

                $query->whereHas("position", function($query) use ($filter) {

                    return $query->whereIn(
                        "main",
                        $filter["value"]
                    );

                });

            } else $query->whereHas($filter["type"], function($query) use ($filter) {

                return $query->whereIn(
                    "id",
                    $filter["value"]
                );

            });

        }

        return $query->get();

    }

    /**
     *
     * Disable records which are related to specified site.
     *
     * @param array $ids
     * @return bool
     */
    public function stopActionForSite(array $ids) {

        $this->dispatchEvent(
            "deleted",
            $ids,
            $this->site
        );

        return (bool)$this->model->where(
            "site_id",
            $this->site->id
        )->whereIn(
            "id",
            $ids
        )->active()->update([
            "status" => false
        ]);

    }

    /**
     *
     * Enable records which are related to specified site.
     *
     * @param array $ids
     * @return bool
     */
    public function runActionForSite(array $ids) {

        $this->dispatchEvent(
            "runned",
            $ids,
            $this->site
        );

        return (bool)$this->model->where(
            "site_id",
            $this->site->id
        )->whereIn(
            "id",
            $ids
        )->disabled()->whereAdvertisementIsActive()->update([
            "status" => true
        ]);

    }

    /**
     *
     * Create new records with relation to specified site.
     *
     * @param array $advertisements
     * @return bool|mixed
     * @throws \Exception
     */
    public function storeForSite(array $advertisements) {

        $closure = function($advertisements) {

            foreach($advertisements as $advertisement) {

                $object = $this->model->create(array_merge([
                    "site_id" => $this->site->id
                ], $advertisement));

                if(key_exists("posts", $advertisement)) {

                    $this->attachAdvertisementRelation(
                        "posts",
                        $advertisement["posts"],
                        $object,
                        "include"
                    );

                }

                if(key_exists("categories", $advertisement)) {

                    $this->attachAdvertisementRelation(
                        "categories",
                        $advertisement["categories"],
                        $object,
                        "exact"
                    );

                }

                if(key_exists("urls", $advertisement)) {

                    $this->saveUrls(
                        $advertisement["urls"],
                        $object
                    );

                }

                if(key_exists("targets", $advertisement)) {

                    $this->attachRelation(
                        "targets",
                        $object,
                        $advertisement["targets"],
                        false
                    );

                }

                if(key_exists("position", $advertisement)) {

                    $this->savePosition(
                        $advertisement["position"],
                        $object
                    );

                }

                if(key_exists("tracker", $advertisement)) {

                    $this->saveTracker(
                        $advertisement["tracker"],
                        $object
                    );

                }

                $this->dispatchEvent(
                    "saved",
                    $object,
                    $this->site
                );

            }

            return true;

        };

        return transaction_wrapper($closure, $advertisements);

    }

    /**
     *
     * Return records which are related to specified site and specified advertisement.
     *
     * @param int $advertisement_id
     * @return mixed
     */
    public function getForSiteByAdvertisement(int $advertisement_id) {

        return $this->model->where(
            "site_id",
            $this->site->id
        )->where(
            "advertisement_id",
            $advertisement_id
        )->active()->get();

    }

    /**
     *
     * Return json string for save in redis.
     *
     * @param Advertisement $object
     * @return false|string
     */
    public function buildJson(Advertisement $object) {

        $advertisement = $object->advertisement;

        $data = [
            "css" => $advertisement->css,
            "html" => $advertisement->buildHtml(),
            "url" => $object->url,
            "params" => []
        ];

        if(($targets = $object->targets)->count()) {

            $data["params"]["targeting"] = [
                "country" => $targets->pluck(
                    "key"
                )->toArray()
            ];

        }

        if(($position = $object->position)) {

            $data["position"] = $position->getPosition(
                $object->type
            );

            if(($sub_position = $position->sub)) {

                $data["params"]["sub_position"] = $sub_position;

            }

        }

        if(($tracker = $object->tracker)) {

            $data["url"] = $tracker->buildUrl();

        }

        return json_encode(
            $data
        );

    }

    /**
     *
     * Get record identifiers which are not related to any post or category on wordpress site.
     *
     * @return mixed
     */
    public function getEmptyAdvertisementsIdsForSite() {

        return $this->pluckForSite(
            "id",
            null,
            function($query) {

                return $query->whereDoesntHave("urls", function($query) {

                    return $query->where(
                        "exclude",
                        false
                    );

                })->whereDoesntHave("posts", function($query) {

                    return $query->where(
                        "include",
                        true
                    );

                })->doesntHave("categories");

            }
        )->toArray();

    }

    /**
     *
     * Get records for delete from database which are related to specified site.
     *
     * @param array $ids
     * @return mixed
     */
    public function getForSiteForDelete(array $ids) {

        return $this->model->with([
            "categories",
            "posts",
            "urls"
        ])->where(
            "site_id",
            $this->site->id
        )->whereIn(
            "id",
            $ids
        )->withTrashed()->get();

    }

    /**
     *
     * Get records which are related to specified site by identifiers for launch.
     *
     * @param array|null $ids
     * @return mixed
     */
    public function getForSiteForRunned(array $ids = null) {

        $query = $this->model->where(
            "site_id",
            $this->site->id
        )->whereAdvertisementIsActive();

        if(is_array($ids)) {

            $query->whereIn(
                "id",
                $ids
            );

        }

        return $query->get();

    }

    /**
     *
     * Get records which are related to specified group and site.
     *
     * @param int $group_id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findForGroupForSite(int $group_id) {

        return $this->model->with([
            "categories",
            "posts",
            "urls"
        ])->where(
            "site_id",
            $this->site->id
        )->where(
            "group_id",
            $group_id
        )->first();

    }

    /**
     *
     * Updating func for App\Models\Site\Advertisement.
     *
     * @param Advertisement $object
     * @param array $data
     * @return Advertisement|bool
     */
    private function updateObjectBody(Advertisement $object, array $data) {

        $values = array_diff_key(
            $data,
            array_flip([
                "categories",
                "position",
                "targets",
                "posts",
                "urls"
            ])
        );

        $group_id = $object->group_id;

        if($this->updateObjectForSite($object, $values)) {

            if(key_exists("posts", $data)) {

                $this->attachAdvertisementRelation(
                    "posts",
                    $data["posts"],
                    $object,
                    "include",
                    true
                );

            }

            if(key_exists("categories", $data)) {

                $this->attachAdvertisementRelation(
                    "categories",
                    $data["categories"],
                    $object,
                    "exact",
                    true
                );

            }

            if(key_exists("urls", $data)) {

                $this->saveUrls(
                    $data["urls"],
                    $object,
                    true
                );

            }

            if(key_exists("targets", $data)) {

                $this->attachRelation(
                    "targets",
                    $object,
                    $data["targets"],
                    true
                );

            }

            if(key_exists("position", $data)) {

                $this->savePosition(
                    $data["position"],
                    $object
                );

            }

            if(key_exists("tracker", $data)) {

                $this->saveTracker(
                    $data["tracker"],
                    $object
                );

            }

            if($group_id != $object->group_id) {

                if(!empty($group_id)) {

                    $this->dispatchEvent(
                        "removedFromGroup",
                        $object,
                        $group_id,
                        $this->site
                    );

                }

            }

            $this->dispatchEvent(
                "saved",
                $object,
                $this->site
            );

            return $object;

        }

        return false;

    }

    /**
     *
     * Change related group for specified advertisements which are related to specified site.
     *
     * @param int $id
     * @param array $data
     * @param array $advertisement
     * @return bool|mixed
     * @throws \Exception
     */
    public function changeGroupForSite(int $id, array $data, array $advertisement) {

        $closure = function() use ($id, $data, $advertisement) {

            $this->dispatchEvent(
                "deleted", [
                    $id
                ],
                $this->site
            );

            $object = $this->findForSite($id);

            $data = array_merge(
                $advertisement,
                $data
            );

            return $this->updateObjectBody(
                $object,
                $data
            );

        };

        return transaction_wrapper($closure);

    }

    /**
     *
     * Get records which are related to specified group and site.
     *
     * @param int $group_id
     * @return mixed
     */
    public function getForGroupForSite(int $group_id) {

        return $this->model->where(
            "site_id",
            $this->site->id
        )->with([
            "categories",
            "posts",
            "urls"
        ])->where(
            "group_id",
            $group_id
        )->get();

    }

    /**
     *
     * Update several objects and save changes to database and redis.
     *
     * @param array $data
     * @return bool|mixed
     * @throws \Exception
     */
    public function updateObjectsForSite(array $data) {

        $objects = $this->model->with([
            "categories",
            "posts",
            "urls"
        ])->where(
            "site_id",
            $this->site->id
        )->whereIn(
            "id",
            array_keys($data)
        )->get();

        $closure = function() use ($objects, $data) {

            $this->dispatchEvent(
                "deleted",
                $objects->pluck(
                    "id"
                )->toArray(),
                $this->site
            );

            foreach($objects as $object) {

                $this->updateObjectBody(
                    $object,
                    $data[$object->id]
                );

            }

            return true;

        };

        return transaction_wrapper($closure);

    }

    /**
     *
     * Get records which are related to specified tracker and user.
     *
     * @param $ids
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     */
    public function getForTrackersForUser($ids) {

        $ids = is_array($ids) ? $ids : [$ids];

        return $this->model->with([
            "advertisement",
            "position",
            "targets",
            "tracker",
            "site"
        ])->whereHas("tracker", function($query) use ($ids) {

            return $query->whereIn(
                "tracker_id",
                $ids
            );

        })->whereHas("site", function($query) {

            return $query->where(
                "user_id",
                $this->user->id
            );

        })->get();

    }

    /**
     *
     * Check if records with specified type exists for specified site.
     *
     * @param int $id
     * @param string $type
     * @return mixed
     */
    public function existsByTypeForSite(int $id, string $type) {

        return $this->model->where(
            "site_id",
            $this->site->id
        )->where(
            "type",
            $type
        )->where(
            "id",
            $id
        )->exists();

    }

    /**
     *
     * Check if active records except specified identifier are exists in specified group and site.
     *
     * @param int $group_id
     * @param int $advertisement_id
     * @return mixed
     */
    public function checkInGroupExists(int $group_id, int $advertisement_id) {

        return $this->model->where(
            "site_id",
            $this->site->id
        )->where(
            "group_id",
            $group_id
        )->where(
            "id",
            "!=",
            $advertisement_id
        )->active()->exists();


    }

    /**
     *
     * Get records by condition which are related to specified user for disable.
     *
     * @param string|null $field
     * @param array $ids
     * @return mixed
     */
    public function getForStopForUser(string $field = null, $ids = []) {

        return $this->model->with([
            "site"
        ])->whereHas("site", function($query) {

            return $query->where(
                "user_id",
                $this->user->id
            );

        })->forRemoveFromRedis(
            $field,
            $ids
        )->get();

    }

    /**
     *
     * Remove specified records for specified site.
     *
     * @param $ids
     * @param bool $force
     * @return mixed
     */
    public function deleteForSite($ids, bool $force = false) {

        $method = $force ? "forceDelete" : "delete";

        $ids = is_array($ids) ? $ids : [$ids];

        if(!$force) {

            $this->dispatchEvent(
                "deleted",
                $ids,
                $this->{$this->getForFieldPostfix()},
                true
            );

        }

        return $this->model->where(
            "site_id",
            $this->site->id
        )->whereIn("id", $ids)->$method();

    }

}
