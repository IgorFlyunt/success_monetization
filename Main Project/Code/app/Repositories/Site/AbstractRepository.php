<?php

namespace App\Repositories\Site;

use App\Models\Site;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Repository\Sites\ForSiteMethodsTrait;
use App\Repositories\AbstractRepository as BaseAbstractRepository;

/**
 * Class AbstractRepository
 *
 * Abstract repository for models which are stored in tenant databases.
 *
 * @package App\Repositories\Site
 */
abstract class AbstractRepository extends BaseAbstractRepository {

    use ForSiteMethodsTrait;

    /**
     * AbstractRepository constructor.
     * @param Model $model
     * @param Site $site
     */
    function __construct(Model $model, Site $site) {

        parent::__construct($model);

        $this->setSite($site);

    }

    /**
     *
     * Used for saving App\Models\Site\Post and App\Models\Site\Category in App\Jobs\Site\SynchronizationJob
     *
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function saveForSite(array $data) : bool {

        return transaction_wrapper(
            function() use ($data) {

                $this->deleteWhileSync(
                    $this->getUniqueField(),
                    $data
                );

                $this->saveForSiteBody(
                    $data
                );

                return true;

            }
        );

        return false;

    }

    /**
     *
     * Used in saveForSite method in current scope for deleting difference from database.
     *
     * @param string $field
     * @param array $data
     */
    private function deleteWhileSync(string $field, array $data) {

        $elements = $this->pluckForSite(
            "id", null, function($query) use ($field, $data) {

                return $query->whereNotIn(
                    $field, array_map(function($element) use ($field) {

                        return $element[$field];

                    }, $data)
                );

            }
        )->toArray();

        if(sizeof($elements)) {

            $this->deleteForSite($elements);

        }

    }

}
