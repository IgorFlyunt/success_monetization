<?php

namespace App\Repositories\Site;

use App\Models\Site;
use App\Models\Site\Category;

/**
 * Class CategoriesRepository
 * @package App\Repositories\Site
 */
class CategoriesRepository extends AbstractRepository {

    /**
     * CategoriesRepository constructor.
     * @param Category $model
     * @param Site $site
     */
    function __construct(Category $model, Site $site) {

        parent::__construct($model, $site);

    }

    /**
     *
         * Get unique field name for table.
     *
     * @return string
     */
    public function getUniqueField() : string {

        return "original_id";

    }

    /**
     *
     * Save and update functionality for site.
     *
     * @param array $data
     */
    public function saveForSiteBody(array $data) {

        foreach($data as $category) {

            $this->model->updateOrCreate([
                "site_id" => $this->site->id,
                "original_id" => $category["original_id"]
            ], [
                "url" => $category["url"],
                "title" => $category["title"]
            ]);

        }

    }

}
