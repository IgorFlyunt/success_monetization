<?php

namespace App\Repositories\Site;

use App\Models\Site\Comment;
use App\Repositories\CommentsRepository as BaseCommentsRepository;

/**
 * Class CommentsRepository
 * @package App\Repositories\Site
 */
class CommentsRepository extends BaseCommentsRepository {

    /**
     * CommentsRepository constructor.
     * @param Comment $model
     */
    function __construct(Comment $model) {

        parent::__construct($model);

    }

    /**
     *
     * Make sure the linked objects exist and are allowed for the user.
     *
     * @param string $class
     * @param int $id
     * @return mixed
     */
    public function targetExistsForUser(string $class, int $id) {

        return $class::whereHas("site", function($query) {

            return $query->where(
                "user_id",
                $this->user->id
            );

        })->where(
            "id",
            $id
        )->exists();

    }

}
