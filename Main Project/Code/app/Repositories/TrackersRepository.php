<?php

namespace App\Repositories;

use App\Models\Tracker;
use App\Events\Tracker\SavedEvent;
use App\Events\Tracker\DeletedEvent;
use App\Traits\Repository\ForUserMethodsTrait;

/**
 * Class TrackersRepository
 * @package App\Repositories
 */
class TrackersRepository extends AbstractRepository {

    use ForUserMethodsTrait;

    /**
     * @var array
     */
    public $events = [
        "deleted" => DeletedEvent::class,
        "saved" => SavedEvent::class
    ];

    /**
     * TrackersRepository constructor.
     * @param Tracker $model
     */
    function __construct(Tracker $model) {

        parent::__construct($model);

    }

    /**
     *
     * Create object and save changes to database for specified user.
     *
     * @param array $data
     * @return mixed
     */
    public function createForUser(array $data) {

        return $this->model->create(array_merge($data, [
            "user_id" => $this->user->id
        ]));

    }

}
