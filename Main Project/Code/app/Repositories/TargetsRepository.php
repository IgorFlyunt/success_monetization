<?php

namespace App\Repositories;

use App\Models\Target;

/**
 * Class TargetsRepository
 * @package App\Repositories
 */
class TargetsRepository extends AbstractRepository {

    /**
     * TargetsRepository constructor.
     * @param Target $model
     */
    function __construct(Target $model) {

        parent::__construct($model);

    }

}
