<?php

namespace App\Jobs\Site;

use Carbon\Carbon;
use App\Models\Site;
use GuzzleHttp\Client;
use App\Models\Site\Post;
use App\Models\Site\Category;
use Illuminate\Bus\Queueable;
use App\Models\Synchronization;
use App\Events\Site\SynchronizedEvent;
use Illuminate\Queue\SerializesModels;
use App\Decorators\Site\PostsDecorator;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Site\SynchronizationEvent;
use App\Repositories\Site\PostsRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Decorators\Site\CategoriesDecorator;
use App\Repositories\Site\CategoriesRepository;
use App\Repositories\SynchronizationsRepository;

/**
 * Class SynchronizationJob
 *
 * Queued job which is used for synchronize site with WordPress.
 *
 * @package App\Jobs\Sites
 */
class SynchronizationJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Site
     */
    protected $site;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Site $site)
    {

        $this->site = $site;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if(($token = $this->site->getSyncronization("key"))) {

            $synchronizationsRepository = (new SynchronizationsRepository(
                new Synchronization
            ))->setSite($this->site);

            $error = null;

            $status = false;

            $synchronized_at = null;

            $client = new Client();

            $url = trim(
                $this->site->url,
                '/'
            ) . "/wp-json/successMonetization/v1/sync";

            $response = $client->post($url, [
                'connect_timeout' => 25,
                'form_params' => [
                    'token' => $token
                ]
            ]);

            if($response->getStatusCode() === 200) {

                $body = json_decode(
                    $response->getBody(),
                    true
                );

                if(!key_exists("code", $body)) {

                    $status = true;

                    $categoriesRepository = new CategoriesRepository(
                        new Category,
                        $this->site
                    );

                    $postsRepository = new PostsRepository(
                        new Post,
                        $this->site
                    );

                    $categories = [];

                    if(key_exists("categories", $body)) {

                        $categories = $body["categories"];

                    }

                    $categoriesRepository->saveForSite(
                        $categoriesRepository->decorate(
                            $categories,
                            CategoriesDecorator::class
                        )
                    );

                    $posts = [];

                    if(key_exists("posts", $body)) {

                        $posts = $body["posts"];

                    }

                    $postsRepository->saveForSite(
                        $postsRepository->decorate(
                            $posts,
                            PostsDecorator::class
                        )
                    );

                    $synchronized_at = Carbon::now()->toDateTimeString();

                    $synchronizationsRepository->updateOrCreateForSite([
                        "error" => null,
                        "status" => $status,
                        "synchronized_at" => $synchronized_at
                    ]);

                    event(
                        new SynchronizedEvent(
                            $this->site
                        )
                    );

                } else {

                    $error = key_exists("message", $body) ? $body["message"] : $body["code"];

                    $synchronizationsRepository->updateOrCreateForSite([
                        "status" => $status,
                        "error" => mb_substr(
                            $error, 0, 255
                        )
                    ]);

                }

            } else {

                $synchronizationsRepository->updateOrCreateForSite([
                    "status" => $status,
                    "error" => null
                ]);

            }

            event(
                new SynchronizationEvent($this->site, $status, [
                    "error" => $error,
                    "synchronized_at" => $synchronized_at
                ])
            );

        }

    }

}
