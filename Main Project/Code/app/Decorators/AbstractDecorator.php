<?php

namespace App\Decorators;

use App\Interfaces\Decorator\DecoratorInterface;

/**
 * Class AbstractDecorator
 * @package App\Decorators
 */
abstract class AbstractDecorator implements DecoratorInterface {

    /**
     * @var
     */
    public $data;

    /**
     * AbstractDecorator constructor.
     * @param $data
     */
    function __construct($data) {

        $this->data = $data;

    }

}

