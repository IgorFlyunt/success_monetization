<?php

namespace App\Decorators\Site;

use App\Decorators\AbstractDecorator;

/**
 * Class PostsDecorator
 *
 * Decorate posts from WordPress before save in database.
 *
 * @package App\Decorators\Site
 */
class PostsDecorator extends AbstractDecorator {

    /**
     * @return array
     */
    public function decorate() {

        $data = [];

        foreach($this->data as $post) {

            $element = [
                "url" => $post["url"],
                "title" => $post["title"]
            ];

            if(key_exists("categories", $post)) {

                $element["categories"] = $post["categories"];

            }

            $data[] = $element;

        }

        return $data;

    }

}
