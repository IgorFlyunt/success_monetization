<?php

namespace App\Decorators\Site;

use App\Decorators\AbstractDecorator;

/**
 * Class CategoriesDecorator
 *
 * Decorate categories from WordPress before save in database.
 *
 * @package App\Decorators\Site
 */
class CategoriesDecorator extends AbstractDecorator {

    /**
     * @return array
     */
    public function decorate() {

        $data = [];

        foreach($this->data as $category) {

            $data[] = [
                "url" => $category["url"],
                "title" => $category["title"],
                "original_id" => $category["id"]
            ];

        }

        return $data;

    }

}
