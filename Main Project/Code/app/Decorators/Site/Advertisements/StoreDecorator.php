<?php

namespace App\Decorators\Site\Advertisements;

use App\Decorators\AbstractDecorator;

/**
 * Class StoreDecorator
 *
 * Decorate data before App\Models\Site\Advertisement creation.
 *
 * Логіка - перші по пріорітетності пости та категорії, а потім урли, тому якщо в урлах є дублі з постами чи категоріями - вони видаляються. Але якщо в урлах є виключення по шляху під який підпадає певний пост, то він видаляється.
 *
 * @package App\Decorators\Site\Advertisements
 */
class StoreDecorator extends AbstractDecorator {

    private $domain  = null;

    /**
     * @return array
     */
    public function decorate() {

        $data = array_filter($this->data["data"], function($key) {

            return !in_array($key, [
                "identifier",
                "tracker",
                "model",
                "posts",
                "urls"
            ]);

        }, ARRAY_FILTER_USE_KEY);

        $data["posts"] = $this->decoratePosts();

        $data["urls"] = $this->decorateUrls(
            $this->getAllUsedUrls(
                $data["posts"]
            )
        );

        $data["tracker"] = $this->decorateTracker(
            $data
        );

        return $this->load($data);

    }

    /**
     * @param array $data
     * @return array
     */
    private function load(array $data) {

        $result = [];

        if(!key_exists("identifier", $this->data)) {

            if(!empty($this->data["data"]["model"]["advertisement_id"])) {

                $data["advertisement_id"] = $this->data["data"]["model"]["advertisement_id"];

                $result[] = $data;

            } elseif(!empty($this->data["data"]["model"]["group_id"])) {

                $group = $this->data["section"]->getSubRepository(
                    "groups"
                )->findForUser(
                    $this->data["data"]["model"]["group_id"],
                    ["advertisements"],
                    ["advertisements"]
                );

                foreach($group->advertisements as $advertisement) {

                    $result[] = array_merge([
                        "group_id" => $this->data["data"]["model"]["group_id"],
                        "status" => $advertisement->status,
                        "advertisement_id" => $advertisement->id
                    ], $data);

                }

            }

        } else {

            $result[$this->data["identifier"]] = $data;

            if(!empty($data["group_id"])) {

                $advertisements = $this->data["section"]->getRepository()->getForGroupForSite(
                    $data["group_id"],
                    $this->data["identifier"]
                );

                foreach($advertisements as $advertisement) {

                    $result[$advertisement->id] = array_merge($data, [
                        "advertisement_id" => $advertisement->advertisement_id
                    ]);

                }

            }

        }

        return $result;

    }

    /**
     * @param array $posts
     * @return array
     */
    private function decorateUrls(array $posts) {

        $include = $this->getAllGroupUrls(
            "include",
            $posts
        );

        $exclude = $this->getAllGroupUrls(
            "exclude",
            $posts
        );

        return array_merge(
            $include,
            $exclude
        );

    }

    /**
     * @param string $group
     * @param array $posts
     * @return array
     */
    private function getAllGroupUrls(string $group, array $posts) {

        $result = [];

        foreach($this->data["data"]["urls"][$group] as $type => $urls) {

            foreach($urls as $url) {

                if(($url = $this->decorateUrl($url)) !== false) {

                    if(!in_array($url, $posts)) {

                        $result[] = [
                            "exclude" => $group == "exclude",
                            "exact" => $type == "exact",
                            "url" => rtrim($url, '/')
                        ];

                    }

                }

            }

        }

        return $result;

    }

    /**
     * @param string $url
     * @return bool|null|string|string[]
     */
    private function decorateUrl(string $url) {

        if(preg_match("@^{$this->getDomain()}/.+@i", $url)) {

            return preg_replace(
                "@{$this->getDomain()}@i",
                '',
                $url
            );

        }

        return false;

    }

    /**
     * @return null|string|string[]
     */
    private function getDomain() {

        if(is_null($this->domain)) {

            $this->domain = preg_replace(
                "@\W@",
                "\\\\$0",
                trim(
                    $this->data["domain"],
                    '/'
                )
            );

        }

        return $this->domain;

    }

    /**
     * @return mixed
     */
    private function decoratePosts() {

        $posts = $this->data["data"]["posts"];

        $exclude_paths = array_filter(
            array_map(function($path) {

                return $this->decorateUrl(
                    $path
                );

            }, $this->data["data"]["urls"]["exclude"]["paths"])
        );

        if(sizeof($exclude_paths)) {

            $posts["include"] = $this->getUsedPosts(
                $posts["include"]
            )->filter(function($post) use ($exclude_paths) {

                foreach($exclude_paths as $exclude_path) {

                    if(preg_match("@^{$exclude_path}.*@", $post)) {

                        return false;

                    }

                }

                return true;

            })->keys()->toArray();

        }

        if(sizeof($posts["exclude"])) {

            $posts["exclude"] = array_diff(
                $posts["exclude"],
                $posts["include"]
            );

        }

        return $posts;

    }

    /**
     * @param array $posts
     * @return mixed
     */
    private function getAllUsedUrls(array $posts) {

        return $this->data["section"]->getSubRepository("posts")->pluckForSite(
            "url",
            null,
            function($query) use ($posts) {

                return $query->whereIn(
                    "id",
                    array_merge(
                        ...array_values(
                            $posts
                        )
                    )
                );

            }
        )->merge(
            $this->data["section"]->getSubRepository("categories")->pluckForSite(
                "url",
                null,
                function($query) {

                    return $query->whereIn(
                        "id",
                        array_merge(
                            ...array_values(
                                $this->data["data"]["categories"]
                            )
                        )
                    );

                }
            )
        )->toArray();

    }

    /**
     * @param array $posts
     * @return mixed
     */
    private function getUsedPosts(array $posts) {

        return $this->data["section"]->getSubRepository("posts")->pluckForSite(
            "url",
            "id",
            function($query) use ($posts) {

                return $query->whereIn(
                    "id",
                    $posts
                );

            }
        );

    }

    /**
     * @param array $data
     * @return array
     */
    private function decorateTracker(array &$data) {

        if(!empty($this->data["data"]["tracker"]["url"])) {

            $data["url"] = $this->data["data"]["tracker"]["url"];

            return [];

        }

        $data["url"] = null;

        return $this->data["data"]["tracker"]["tracker"];

    }

}
