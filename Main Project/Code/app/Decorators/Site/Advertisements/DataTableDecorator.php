<?php

namespace App\Decorators\Site\Advertisements;

use App\Models\Site\Advertisement;
use App\Decorators\AbstractDecorator;

/**
 * Class DataTableDecorator
 *
 * Decorate App\Models\Site\Advertisement for DataTable.
 *
 * @package App\Decorators\Site\Advertisements
 */
class DataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return $this->decorateElement($item);

        })->toArray();

    }

    /**
     * @param Advertisement $item
     * @return array
     */
    private function decorateElement(Advertisement $item) {

        return [
            "id" => $item->id,
            "name" => $item->advertisement->name,
            "groups" => $this->decorateGroup($item),
            "parent" => $this->decorateParent($item),
            "categories" => $item->advertisement->categories->pluck(
                "name"
            )->toArray(),
            "status" => $this->decorateStatus($item)
        ];

    }

    /**
     * @param Advertisement $item
     * @return array
     */
    private function decorateGroup(Advertisement $item) {

        $result = [];

        if($item->group) {

            $result[] = $item->group->name;

        }

        return $result;

    }

    /**
     * @param Advertisement $item
     * @return bool|null
     */
    private function decorateStatus(Advertisement $item) {

        if($item->advertisement->status && !$item->advertisement->trashed()) {

            if(!empty($item->url) || $item->has_tracker) {

                return (bool)$item->status;

            }

        }

        return null;

    }

    /**
     * @param Advertisement $item
     * @return array
     */
    private function decorateParent(Advertisement $item) {

        if(!empty($item->parent_id)) {

            return [
                "title" => $item->parent_id,
                "url" => route("site.advertisements.edit", [
                    "site_id" => $item->site_id,
                    "section" => $item->type,
                    "identifier" => $item->parent_id
                ])
            ];

        }

        return [];

    }

}
