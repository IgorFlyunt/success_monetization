<?php

namespace App\Decorators\Site\Advertisements;

use App\Models\Site\Tracker;
use App\Models\Site\Position;
use Illuminate\Support\Collection;
use App\Decorators\AbstractDecorator;

/**
 * Class BeforeEditDecorator
 *
 * Decorate App\Models\Site\Advertisement before edit.
 *
 * @package App\Decorators\Site\Advertisements
 */
class BeforeEditDecorator extends AbstractDecorator {

    /**
     * @return array
     */
    public function decorate() {

        return [
            "id" => $this->data["advertisement"]->id,
            "url" => $this->data["advertisement"]->url,
            "urls" => $this->decorateUrls(
                $this->data["advertisement"]->urls
            ),
            "tracker" => $this->decorateTracker(
                $this->data["advertisement"]->tracker
            ),
            "position" => $this->decoratePosition(
                $this->data["advertisement"]->position
            ),
            "targets" => $this->decorateTargets(
                $this->data["advertisement"]->targets
            ),
            "posts" => $this->decoratePosts(
                $this->data["advertisement"]->posts
            ),
            "group_id" => $this->data["advertisement"]->group_id,
            "comments" => $this->data["advertisement"]->comments,
            "categories" => $this->decorateCategories(
                $this->data["advertisement"]->categories
            ),
            "advertisement_id" => $this->data["advertisement"]->advertisement_id
        ];

    }

    /**
     * @param Collection $categories
     * @return array
     */
    private function decorateCategories(Collection $categories) {

        return [
            "exact" => $categories->where(
                "pivot.exact",
                true
            )->pluck(
                "id"
            )->toArray(),
            "paths" => $categories->where(
                "pivot.exact",
                false
            )->pluck(
                "id"
            )->toArray()
        ];

    }

    /**
     * @param Collection $posts
     * @return array
     */
    private function decoratePosts(Collection $posts) {

        return [
            "include" => $posts->where(
                "pivot.include",
                true
            )->pluck(
                "id"
            )->toArray(),
            "exclude" => $posts->where(
                "pivot.include",
                false
            )->pluck(
                "id"
            )->toArray()
        ];

    }

    /**
     * @param Collection $urls
     * @return array
     */
    private function decorateUrls(Collection $urls) {

        return [
            "include" => [
                "exact" => $this->decorateUrl(
                    $urls->where(
                        "exclude",
                        false
                    )->where(
                        "exact",
                        true
                    )->pluck(
                        "url"
                    )
                ),
                "paths" => $this->decorateUrl(
                    $urls->where(
                        "exclude",
                        false
                    )->where(
                        "exact",
                        false
                    )->pluck(
                        "url"
                    )
                )
            ],
            "exclude" => [
                "exact" => $this->decorateUrl(
                    $urls->where(
                        "exclude",
                        true
                    )->where(
                        "exact",
                        true
                    )->pluck(
                        "url"
                    )
                ),
                "paths" => $this->decorateUrl(
                    $urls->where(
                        "exclude",
                        true
                    )->where(
                        "exact",
                        false
                    )->pluck(
                        "url"
                    )
                )
            ]
        ];

    }

    /**
     * @param Collection $urls
     * @return string
     */
    private function decorateUrl(Collection $urls) {

        return $urls->map(function($url) {

            return $this->data["site"]->url . $url;

        })->implode("\n");

    }

    /**
     * @param Collection $targets
     * @return array
     */
    private function decorateTargets(Collection $targets) {

        return $targets->pluck(
            "id"
        )->toArray();

    }

    /**
     * @param Position|null $position
     * @return array|null
     */
    private function decoratePosition(Position $position = null) {

        if(!is_null($position)) {

            return [
                "main" => $position->main,
                "sub" => $position->sub
            ];

        }

        return null;

    }

    /**
     * @param Tracker|null $tracker
     * @return array|null
     */
    private function decorateTracker(Tracker $tracker = null) {

        if(!is_null($tracker)) {

            return [
                "id" => $tracker->tracker_id,
                "utm_medium" => $tracker->utm_medium,
                "utm_campaign" => $tracker->utm_campaign,
                "utm_source" => $tracker->utm_source,
                "sid1" => $tracker->sid1,
                "sid2" => $tracker->sid2,
                "sid3" => $tracker->sid3
            ];

        }

        return null;

    }

}
