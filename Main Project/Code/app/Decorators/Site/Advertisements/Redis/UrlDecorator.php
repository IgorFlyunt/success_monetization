<?php

namespace App\Decorators\Site\Advertisements\Redis;

use App\Decorators\AbstractDecorator;

/**
 * Class UrlDecorator
 *
 * Decorate all urls before App\Events\Site\Advertisements\SavedEvent call.
 *
 * @package App\Decorators\Site\Advertisements\Redis
 */
class UrlDecorator extends AbstractDecorator {

    /**
     * @return array
     */
    public function decorate() {

        return [
            "include" => array_merge(
                $this->data["posts"]->where(
                    "pivot.include",
                    true
                )->pluck(
                    "url"
                )->toArray(),
                $this->data["categories"]->map(function($category) {

                    return $category["url"] . (!$category->pivot->exact ? '*' : '');

                })->toArray(),
                $this->data["urls"]->where(
                    "exclude",
                    false
                )->map(function($url) {

                    return $url["url"] . (!$url->exact ? '*' : '');

                })->toArray()
            ),
            "exclude" => array_merge(
                $this->data["posts"]->where(
                    "pivot.include",
                    false
                )->pluck(
                    "url"
                )->toArray(),
                $this->data["urls"]->where(
                    "exclude",
                    true
                )->map(function($url) {

                    return $url["url"] . (!$url->exact ? '*' : '');

                })->toArray()
            )
        ];

    }

}
