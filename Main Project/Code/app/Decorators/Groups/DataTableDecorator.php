<?php

namespace App\Decorators\Groups;

use App\Models\Group;
use App\Decorators\AbstractDecorator;

/**
 * Class DataTableDecorator
 *
 * Decorate groups for DataTable.
 *
 * @package App\Decorators\Groups
 */
class DataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return [
                "id" => $item->id,
                "name" => $item->name,
                "type" => trans("t.advertisements.types.{$item->type}"),
                "advertisements" => $this->buildAdvertisements($item),
                "sites" => $item->sites->pluck("url")->unique()->toArray()
            ];

        })->toArray();

    }

    /**
     * @param Group $item
     * @return mixed
     */
    private function buildAdvertisements(Group $item) {

        return $item->advertisements->map(function($advertisement) {

            return [
                "title" => "({$advertisement->id}) {$advertisement->name}",
                "url" => route("resolver.edit", [
                    str_plural(
                        $advertisement->type
                    ), $advertisement->id
                ])
            ];

        });

    }

}
