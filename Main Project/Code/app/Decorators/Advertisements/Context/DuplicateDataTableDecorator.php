<?php

namespace App\Decorators\Advertisements\Context;

use App\Models\Advertisement;
use App\Decorators\AbstractDecorator;

/**
 * Class DuplicateDatatableDecorator
 *
 * Decorate children context advertisements for DataTable in parent edit page.
 *
 * @package App\Decorators\Advertisements\Context
 */
class DuplicateDatatableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return $this->decorateElement($item);

        })->toArray();

    }

    /**
     * @param Advertisement $item
     * @return array
     */
    private function decorateElement(Advertisement $item) {

        return [
            "id" => $item->id,
            "css" => $item->css,
            "html" => $item->buildHtml(),
            "name" => $item->name,
            "parent_id" => $item->parent_id,
            "groups" => $item->groups->pluck("name")->toArray(),
            "parent" => [
                'title' => $item->parent->name,
                'url' => route("resolver.edit", ["contexts", $item->parent_id])
            ],
            "categories" => $item->categories->pluck("name")->toArray(),
            "status" => (bool)$item->status,
            "sites" => $item->sites->pluck("url")->toArray()
        ];

    }

}
