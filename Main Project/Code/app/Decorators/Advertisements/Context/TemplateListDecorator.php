<?php

namespace App\Decorators\Advertisements\Context;

use App\Models\Advertisement;
use App\Decorators\AbstractDecorator;

/**
 * Class TemplateListDecorator
 *
 * Decorate template context advertisements for DataTable.
 *
 * @package App\Decorators\Advertisements\Context
 */
class TemplateListDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return [
                "id" => $item->id,
                "name" => $item->name,
                "html" => $item->buildHtml(),
                "css" => $item->css,
                "is_original" => $item->isOriginal(),
                "groups" => $item->groups->pluck("name", "id")->toArray(),
                "categories" => $item->categories->pluck("name", "id")->toArray()
            ];

        })->toArray();

    }

}
