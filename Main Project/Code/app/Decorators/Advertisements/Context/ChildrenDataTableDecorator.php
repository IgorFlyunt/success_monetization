<?php

namespace App\Decorators\Advertisements\Context;

use App\Models\Advertisement;
use App\Decorators\AbstractDecorator;

/**
 * Decorate children context advertisements for DataTable.
 *
 * Class ChildrenDataTableDecorator
 * @package App\Decorators\Advertisements\Context
 */
class ChildrenDataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return $this->decorateElement($item);

        })->toArray();

    }

    /**
     * @param Advertisement $item
     * @return array
     */
    private function decorateElement(Advertisement $item) {

        return [
            "id" => $item->id,
            "css" => $item->css,
            "html" => $item->buildHtml(),
            "name" => $item->name,
            "groups" => $item->groups->pluck("name")->toArray(),
            "categories" => $item->categories->pluck("name")->toArray(),
            "status" => (bool)$item->status,
            "sites" => $item->sites->pluck("url")->toArray()
        ];

    }

}


