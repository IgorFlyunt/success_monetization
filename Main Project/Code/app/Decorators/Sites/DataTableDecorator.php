<?php

namespace App\Decorators\Sites;

use App\Models\Site;
use App\Decorators\AbstractDecorator;

/**
 * Class DataTableDecorator
 *
 * Decorate posts for DataTable.
 *
 * @package App\Decorators\Sites
 */
class DataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return [
                "id" => $item->id,
                "url" => [
                    'title' => $item->url,
                    'url' => $item->url
                ],
                "user_id" => $item->user->name,
                "status" => $this->buildStatusField($item),
                "api_status" => $this->buildApiStatusField($item),
                "synchronized_at" => $this->buildSyncronizedAtField($item),
                "advertisements_actions" => route("site.index", $item->id)
            ];

        })->toArray();

    }

    /**
     * @param Site $site
     * @return bool|null
     */
    private function buildStatusField(Site $site) {

        if($site->has_advertisements) {

            return (bool)$site->status;

        }

        return null;

    }

    /**
     * @param Site $site
     * @return bool|null
     */
    private function buildApiStatusField(Site $site) {

        if(($status = $site->getSyncronization("status")) !== null) {

            return (bool)$status;

        }

        return null;

    }

    /**
     * @param Site $site
     * @return bool|null
     */
    private function buildSyncronizedAtField(Site $site) {

        if($site->hasSyncronization()) {

            return $site->getSynchronizedAt() ?: false;

        }

        return null;

    }

}
