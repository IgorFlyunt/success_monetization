<?php

namespace App\Decorators\Trash;

use App\Decorators\AbstractDecorator;

/**
 * Class AdvertisementsDataTableDecorator
 *
 * Decorate trashed advertisements for DataTable.
 *
 * @package App\Decorators\Trash
 */
class AdvertisementsDataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return [
                "id" => $item->id,
                "name" => $item->name,
                "categories" => $item->categories->pluck("name")->toArray(),
                "deleted_at" => $item->deleted_at->format("d-m-Y H:i:s")
            ];

        })->toArray();

    }

}
