<?php

namespace App\Decorators\Trash;

use App\Decorators\AbstractDecorator;

/**
 * Class SitesDataTableDecorator
 *
 * Decorate trashed sites for DataTable.
 *
 * @package App\Decorators\Trash
 */
class SitesDataTableDecorator extends AbstractDecorator {

    /**
     * @return mixed
     */
    public function decorate() {

        return $this->data->map(function($item) {

            return [
                "id" => $item->id,
                "url" => [
                    'title' => $item->url,
                    'url' => $item->url
                ],
                "user_id" => $item->user->name,
                "deleted_at" => $item->deleted_at->format("d-m-Y H:i:s")
            ];

        })->toArray();

    }

}
