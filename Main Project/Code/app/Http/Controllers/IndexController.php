<?php

namespace App\Http\Controllers;

/**
 * Class IndexController
 *
 * Render base dashboard for user.
 *
 * @package App\Http\Controllers
 */
class IndexController extends MainController
{

    /**
     * @return IndexController
     */
    public function show()
    {

        return $this->render("views.dashboard");

    }

}
