<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use App\Classes\Sections\Loader as SectionLoader;

/**
 * Class ResolverController
 *
 * Controller is used for handling sections routes.
 *
 * @package App\Http\Controllers
 */
class ResolverController extends Controller
{

    /**
     * @var SectionLoader
     */
    protected $loader;

    /**
     * ResolverController constructor.
     * @param SectionLoader $loader
     */
    function __construct(SectionLoader $loader) {

        $this->loader = $loader;

        parent::__construct();

    }

    /**
     *
     * Default Laravel method which is called before action handling.
     *
     * @param string $method
     * @param array $parameters
     * @return mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters) {

        $filtered_params = [];

        $parameter_names = array_diff(
            request()->route()->parameterNames,
            ["section"]
        );

        foreach($parameter_names as $parameter) {

            if(key_exists($parameter, $parameters)) {

                $filtered_params[] = $parameters[$parameter];

            } else {

                $filtered_params[] = null;

            }

        }

        $section = $this->loader->loadSection($this->user);

        abort_if(is_int($section), $section);

        $result = call_user_func_array([
            $section,
            "callAction"
        ], [
            "action" => $method,
            "parameters" => $filtered_params
        ]);

        $method = "return" . ucfirst($result["type"]);

        unset($result["type"]);

        return call_user_func_array([
            $this,
            $method
        ], $result);

    }

    /**
     *
     * Called when response from section is "view".
     *
     * @param View $view
     * @param array $meta
     * @param array $messages
     * @return View
     */
    public function returnView(View $view, array $meta, array $messages) {

        flash_messages($messages);

        foreach($meta as $key => $value) {

            app("meta")->set($key, $value);

        }

        return $view;

    }

    /**
     *
     * Called when response from section is "json".
     *
     * @param array $data
     * @param array $messages
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnJson(array $data, array $messages) {

        return response()->json(array_merge($data, [
            "messages" => $messages
        ]));

    }

    /**
     *
     * Called when response from section is "redirect".
     *
     * @param string $to
     * @param bool $error
     * @param array $messages
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function returnRedirect(string $to, bool $error, array $messages) {

        flash_messages($messages);

        return redirect($to)->withInput($error ? request()->all() : []);

    }

}
