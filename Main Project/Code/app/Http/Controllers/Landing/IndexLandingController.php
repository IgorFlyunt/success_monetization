<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;

/**
 * Class IndexLandingController
 *
 * Render default landing page.
 *
 * @package App\Http\Controllers\Landing
 */
class IndexLandingController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(){
        return view(env('LANDING_BACKEND').'.index');
    }

}
