<?php

namespace App\Console\Commands\Sites;

use Illuminate\Console\Command;
use App\Repositories\SitesRepository;

/**
 * Class SynchronizeCommand
 *
 * Queued job for synchronize site with wordpress.
 *
 * @package App\Console\Commands\Sites
 */
class SynchronizeCommand extends Command
{

    /**
     * @var SitesRepository
     */
    protected $repository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sites:synchronization';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize sites categories and posts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SitesRepository $repository)
    {

        parent::__construct();

        $this->repository = $repository;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->repository->runSynchronization();

    }

}
