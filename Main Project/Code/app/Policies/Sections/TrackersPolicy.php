<?php

namespace App\Policies\Sections;

use App\Repositories\TrackersRepository;

/**
 * Class TrackersPolicy
 * @package App\Policies\Sections
 */
class TrackersPolicy extends AbstractPolicy
{

    /**
     * TrackersPolicy constructor.
     * @param TrackersRepository $repository
     */
    function __construct(TrackersRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return true;

    }

    /**
     * @return bool
     */
    public function update() {

        return true;

    }

    /**
     * @return bool
     */
    public function destroy() {

        return true;

    }

}
