<?php

namespace App\Policies\Sections;

use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\CommentsRepository;

/**
 * Class CommentsPolicy
 * @package App\Policies\Sections
 */
class CommentsPolicy extends AbstractPolicy
{

    /**
     * CommentsPolicy constructor.
     * @param CommentsRepository $repository
     */
    function __construct(CommentsRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return false;

    }

    /**
     * @param User $user
     * @param Request $request
     * @return mixed
     */
    public function store(User $user, Request $request) {

        return $this->checkForUser($request);

    }

    /**
     * @param User $user
     * @param Request $request
     * @return mixed
     */
    public function load(User $user, Request $request) {

        return $this->checkForUser($request);

    }

    /**
     * @param User $user
     * @param Request $request
     * @return mixed
     */
    public function destroy(User $user, Request $request) {

        return $this->checkForUser($request);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function checkForUser(Request $request) {

        return $this->repository->targetExistsForUser(
            $request->get("modelable_type"),
            $request->get("modelable_id")
        );

    }

}
