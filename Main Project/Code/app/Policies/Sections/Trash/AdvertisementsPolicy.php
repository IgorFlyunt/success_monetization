<?php

namespace App\Policies\Sections\Trash;

use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\AdvertisementsRepository;
use App\Policies\Sections\AbstractPolicy;

/**
 * Class AdvertisementsPolicy
 * @package App\Policies\Sections\Trash
 */
class AdvertisementsPolicy extends AbstractPolicy {

    /**
     * AdvertisementsPolicy constructor.
     * @param AdvertisementsRepository $repository
     */
    function __construct(AdvertisementsRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return false;

    }

    /**
     * @return mixed
     */
    public function destroy() {

        return $this->repository->existsTrashedForUser(
            $this->identifier
        );

    }

    /**
     * @param User $user
     * @param Request $request
     * @return bool
     */
    public function load(User $user, Request $request) {

        return (bool)$request->route("identifier");

    }

}
