<?php

namespace App\Policies\Sections\Trash;

use App\Repositories\SitesRepository;
use App\Policies\Sections\AbstractPolicy;

/**
 * Class SitesPolicy
 * @package App\Policies\Sections\Trash
 */
class SitesPolicy extends AbstractPolicy
{

    /**
     * SitesPolicy constructor.
     * @param SitesRepository $repository
     */
    function __construct(SitesRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return false;

    }

    /**
     * @return mixed
     */
    public function destroy() {

        return $this->repository->existsTrashedForUser(
            $this->identifier
        );

    }

}
