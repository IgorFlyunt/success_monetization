<?php

namespace App\Policies\Sections\Site\Advertisements;

use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\SitesRepository;
use App\Policies\Sections\AbstractPolicy;
use App\Repositories\Site\AdvertisementsRepository;

/**
 * Class ContextsPolicy
 * @package App\Policies\Sections\Site\Advertisements
 */
class ContextsPolicy extends AbstractPolicy
{

    /**
     * @var
     */
    protected $site;

    /**
     * @var string
     */
    public $identifier_name = "site_id";

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * ContextsPolicy constructor.
     * @param SitesRepository $sitesRepository
     * @param AdvertisementsRepository $advertisementsRepository
     */
    function __construct(
        SitesRepository $sitesRepository,
        AdvertisementsRepository $advertisementsRepository
    )
    {

        parent::__construct($sitesRepository);

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @return bool
     */
    public function checkExistsForUser(): bool
    {

        return true;

    }

    /**
     * @param User $user
     * @param string $action
     * @param string $section
     * @return bool
     */
    public function before(User $user, string $action, string $section)
    {

        $this->repository->setUser($user);

        if ($this->checkExistsForUser()) {

            if (!empty($this->identifier)) {

                $this->site = $this->repository->find(
                    $this->identifier, [
                        "synchronization"
                    ]
                );

                if ($this->site) {

                    if ($this->site->user_id != $user->id || !$this->site->hasSyncronization()) {

                        return false;

                    }

                } else abort(404);

            }

        }

    }

    /**
     * @return bool
     */
    public function index()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadCategories()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadPosts()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadTargets()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadPositions()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadTrackers()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadForSelect()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function loadGroups()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function load()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function create()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function store()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function actions()
    {

        return true;

    }

    /**
     * @return bool
     */
    public function destroy()
    {

        return true;

    }

    /**
     * @param User $user
     * @param Request $request
     * @return mixed
     */
    public function edit(User $user, Request $request)
    {

        return $this->advertisementsRepository->setSite(
            $this->site
        )->existsByTypeForSite(
            $request->route("identifier"),
            str_singular(
                $request->route("section")
            )
        );

    }

    /**
     * @param User $user
     * @param Request $request
     * @return mixed
     */
    public function update(User $user, Request $request)
    {

        return $this->advertisementsRepository->setSite(
            $this->site
        )->existsByTypeForSite(
            $request->route("identifier"),
            str_singular(
                $request->route("section")
            )
        );

    }

    /**
     * @param User $user
     * @param Request $request
     * @return mixed
     */
    public function changeGroup(User $user, Request $request)
    {

        return $this->advertisementsRepository->setSite(
            $this->site
        )->existsByTypeForSite(
            $request->route("identifier"),
            str_singular(
                $request->route("section")
            )
        );

    }

}
