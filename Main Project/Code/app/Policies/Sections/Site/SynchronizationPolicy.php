<?php

namespace App\Policies\Sections;

use App\Repositories\SitesRepository;
use App\Policies\Sections\AbstractPolicy;

/**
 * Class SynchronizationPolicy
 * @package App\Policies\Sections
 */
class SynchronizationPolicy extends AbstractPolicy
{

    /**
     * @var string
     */
    public $identifier_name = "site_id";

    /**
     * SynchronizationPolicy constructor.
     * @param SitesRepository $repository
     */
    function __construct(SitesRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return true;

    }

    /**
     * @return bool
     */
    public function index() {

        return true;

    }

    /**
     * @return bool
     */
    public function updateSync() {

        return true;

    }

    /**
     * @return bool
     */
    public function syncAction() {

        return true;

    }

}
