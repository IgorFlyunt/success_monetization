<?php

namespace App\Policies\Sections\Site;

use Illuminate\Http\Request;
use App\Repositories\Site\CommentsRepository;
use App\Policies\Sections\CommentsPolicy as BaseCommentsPolicy;

/**
 * Class CommentsPolicy
 * @package App\Policies\Sections\Site
 */
class CommentsPolicy extends BaseCommentsPolicy
{

    /**
     * CommentsPolicy constructor.
     * @param CommentsRepository $repository
     */
    function __construct(CommentsRepository $repository) {

        parent::__construct($repository);

    }

    /**
     *
     * Check if user can work with specified entity.
     *
     * @param Request $request
     * @return mixed
     */
    public function checkForUser(Request $request) {

        return $this->repository->targetExistsForUser(
            $request->get("modelable_type"),
            $request->get("modelable_id")
        );

    }

}
