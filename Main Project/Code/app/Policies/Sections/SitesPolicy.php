<?php

namespace App\Policies\Sections;

use App\Repositories\SitesRepository;

/**
 * Class SitesPolicy
 * @package App\Policies\Sections
 */
class SitesPolicy extends AbstractPolicy
{

    /**
     * SitesPolicy constructor.
     * @param SitesRepository $repository
     */
    function __construct(SitesRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return true;

    }

    /**
     * @return bool
     */
    public function edit() {

        return true;

    }

    /**
     * @return bool
     */
    public function update() {

        return true;

    }

    /**
     * @return bool
     */
    public function destroy() {

        return true;

    }

}
