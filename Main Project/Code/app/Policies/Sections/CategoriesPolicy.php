<?php

namespace App\Policies\Sections;

use App\Repositories\CategoriesRepository;

/**
 * Class CategoriesPolicy
 * @package App\Policies\Sections
 */
class CategoriesPolicy extends AbstractPolicy
{

    /**
     * CategoriesPolicy constructor.
     * @param CategoriesRepository $repository
     */
    function __construct(CategoriesRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return true;

    }

    /**
     * @return bool
     */
    public function destroy() {

        return true;

    }

    /**
     * @return bool
     */
    public function update() {

        return true;

    }

}
