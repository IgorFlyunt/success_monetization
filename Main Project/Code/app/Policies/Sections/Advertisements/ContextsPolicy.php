<?php

namespace App\Policies\Sections\Advertisements;

use App\Policies\Sections\AbstractPolicy;
use App\Repositories\AdvertisementsRepository;

/**
 * Class ContextsPolicy
 * @package App\Policies\Sections\Advertisements
 */
class ContextsPolicy extends AbstractPolicy
{

    /**
     * ContextsPolicy constructor.
     * @param AdvertisementsRepository $repository
     */
    function __construct(AdvertisementsRepository $repository) {

        parent::__construct($repository);

    }

    /**
     * @return bool
     */
    public function checkExistsForUser() : bool {

        return true;

    }

    /**
     * @return mixed
     */
    public function edit() {

        return $this->repository->existsForEditingForUser(
            $this->identifier
        );

    }

    /**
     * @return mixed
     */
    public function editTemplate() {

        return $this->repository->existsForEditingForUser(
            $this->identifier,
            true
        );

    }

    /**
     * @return mixed
     */
    public function showCopy() {

        return $this->repository->existsForCopyingForUser(
            $this->identifier
        );

    }

    /**
     * @return mixed
     */
    public function copyTemplate() {

        return $this->repository->existsForCopyingForUser(
            $this->identifier,
            true
        );

    }

    /**
     * @return bool
     */
    public function saveCopy() {

        return true;

    }

    /**
     * @return bool
     */
    public function toOriginal() {

        return true;

    }

    /**
     * @return bool
     */
    public function createTemplate() {

        return true;

    }

    /**
     * @return bool
     */
    public function update() {

        return true;

    }

    /**
     * @return bool
     */
    public function destroy() {

        return true;

    }

}
