<?php

namespace App\Sections;

use App\Models\Comment;
use App\Repositories\CommentsRepository;

/**
 * Class CommentsSection
 * @package App\Sections
 */
class CommentsSection extends AbstractSection {

    /**
     *
     * Array of disabled route actions.
     *
     * @var array
     */
    public $disabled = [
        "index",
        "edit",
        "create",
        "actions",
        "update"
    ];

    /**
     *
     * Array of actions which are allowed only for authenticated users.
     *
     * @return array
     */
    public function forUserActions() : array {

        return [];

    }

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Comment::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return CommentsRepository::class;

    }

    /**
     *
     * Run load route action.
     *
     * @param array $data
     * @return array
     */
    public function load(array $data) {

        $elements = $this->getRepository()->getForUser(
            $data["modelable_id"],
            $data["modelable_type"]
        );

        return $this->toJson([
            "status" => "success",
            "elements" => $elements
        ]);

    }

    /**
     *
     * Validate load route action.
     *
     * @return array
     */
    public function validateLoad() {

        return [
            "modelable_id" => "required|numeric|min:1",
            "modelable_type" => "required|max:255"
        ];

    }

    /**
     *
     * Validate destroy route action.
     *
     * @return array
     */
    public function validateDestroy() {

        return $this->validateLoad();

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return $this->validateLoad();

    }

}
