<?php

namespace App\Sections\Site\Advertisements;

use App\Decorators\Site\Advertisements\DataTableDecorator;

/**
 * Class ContextsSection
 * @package App\Sections\Site\Advertisements
 */
class ContextsSection extends AbstractSection
{

    /**
     *
     * Get custom alias.
     *
     * @return null|string
     */
    public static function getAlias(): ?string
    {

        return "site/contexts";

    }

    /**
     *
     * Get advertisement type.
     *
     * @return string
     */
    public function getAdvertisementType(): string
    {

        return "context";

    }

    /**
     *
     * Run load route action.
     *
     * @param array $data
     * @return array
     */
    public function load(array $data = [])
    {

        $elements = $this->getRepository()->getForTableForSite(
            $this->getAdvertisementType(),
            $data
        );

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                DataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Get DataTable configuration for load route action.
     *
     * @return array
     */
    protected function getDataTableConfig()
    {

        return [
            "id" => "site-advertisements",
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "name" => [
                    "title" => trans("t.fields.name")
                ],
                "parent" => [
                    "title" => trans("t.fields.parent")
                ],
                "groups" => [
                    "title" => trans("t.fields.groups"),
                    "sortable" => false
                ],
                "categories" => [
                    "title" => trans("t.fields.categories"),
                    "sortable" => false
                ],
                "status" => [
                    "title" => trans("t.fields.status")
                ],
                "comments" => [
                    "title" => trans("t.fields.comments"),
                    "sortable" => false
                ]
            ],
            "actions" => [
                "run",
                "stop",
                "edit",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ]
            ],
            "decorators" => [
                "status" => [
                    "type" => "status",
                    "value" => trans("t.advertisement is not active")
                ],
                "groups" => "array",
                "parent" => "link",
                "categories" => "array",
                "comments" => [
                    "type" => "comments",
                    "alias" => "site/{$this->site_id}",
                    "modelable_type" => $this->getModelClass()
                ]
            ]
        ];

    }

}
