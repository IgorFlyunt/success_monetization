<?php

namespace App\Sections\Site\Advertisements;

use App\Models\Site;
use App\Models\Group;
use App\Models\Target;
use App\Models\Tracker;
use App\Models\Site\Post;
use App\Models\Site\Category;
use App\Models\Site\Advertisement;
use App\Repositories\SitesRepository;
use App\Repositories\GroupsRepository;
use App\Repositories\TargetsRepository;
use App\Repositories\TrackersRepository;
use App\Repositories\Site\PostsRepository;
use App\Repositories\Site\CategoriesRepository;
use App\Models\Advertisement as BaseAdvertisement;
use App\Repositories\Site\AdvertisementsRepository;
use App\Decorators\Site\Advertisements\StoreDecorator;
use App\Decorators\Site\Advertisements\BeforeEditDecorator;
use App\Sections\Site\AbstractSection as BaseAbstractSection;
use App\Decorators\Site\Advertisements\BeforeChangeGroupDecorator;
use App\Repositories\AdvertisementsRepository as BaseAdvertisementsRepository;

/**
 * Class AbstractSection
 * @package App\Sections\Site\Advertisements
 */
abstract class AbstractSection extends BaseAbstractSection {

    /**
     *
     * Get advertisement type.
     *
     * @return string
     */
    abstract public function getAdvertisementType() : string;

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass(): string
    {

        return Advertisement::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass(): string
    {

        return AdvertisementsRepository::class;

    }

    /**
     *
     * Get sub repositories configuration.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            "sites" => [
                "repo" => SitesRepository::class,
                "model" => Site::class
            ],
            "groups" => [
                "repo" => GroupsRepository::class,
                "model" => Group::class
            ],
            "advertisements" => [
                "repo" => BaseAdvertisementsRepository::class,
                "model" => BaseAdvertisement::class
            ],
            "trackers" => [
                "repo" => TrackersRepository::class,
                "model" => Tracker::class
            ],
            "posts" => [
                "repo" => PostsRepository::class,
                "model" => Post::class
            ],
            "categories" => [
                "repo" => CategoriesRepository::class,
                "model" => Category::class
            ],
            "targets" => [
                "repo" => TargetsRepository::class,
                "model" => Target::class
            ]
        ];

    }

    /**
     *
     * Run index route action.
     *
     * @return array
     */
    public function index() {

        $this->setTitle(
            __FUNCTION__
        );

        return $this->toView([
            "alias" => $this->buildAlias(),
            "config" => $this->getDataTableConfig()
        ]);

    }

    /**
     *
     * Validate load route action.
     *
     * @return array
     */
    public function validateLoad() {

        return [
            "type" => "string|in:posts,categories,positions,groups|required_with:value",
            "value" => "array"
        ];

    }

    /**
     *
     * Run edit route action.
     *
     * @param string $identifier
     * @return array
     */
    public function edit(string $identifier) {

        $this->setTitle(
            __FUNCTION__
        );

        return $this->toView([
            "alias" => $this->buildAlias(),
            "object" => $this->getRepository()->decorate([
                "advertisement" => $this->getRepository()->findForSite($identifier, [
                    "categories",
                    "comments" => function($query) {

                        return $query->decorateDate()->dateOrdered();

                    },
                    "position",
                    "tracker",
                    "targets",
                    "posts",
                    "urls"
                ]),
                "site" => $this->site
            ], BeforeEditDecorator::class),
            "positions" => config(
                "advertisement.positions.{$this->getAdvertisementType()}",
                []
            )
        ]);

    }

    /**
     *
     * Run create route action.
     *
     * @return array
     */
    public function create() {

        $this->setTitle(
            __FUNCTION__
        );

        return $this->toView([
            "alias" => $this->buildAlias(),
            "positions" => config(
                "advertisement.positions.{$this->getAdvertisementType()}",
                []
            )
        ]);

    }

    /**
     *
     * Run store route action.
     *
     * @param array $data
     * @return array
     */
    public function store(array $data) {

        $result = false;

        $data = $this->getRepository()->decorate([
            "data" => array_merge($data, [
                "type" => $this->getAdvertisementType()
            ]),
            "domain" => $this->site->url,
            "section" => $this
        ], StoreDecorator::class);

        if($this->getRepository()->checkTargetIsNotEmpty($data)) {

            $result = $this->getRepository()->storeForSite(
                $data
            );

        } else $this->setMessage(
            trans("t.site advertisement urls error"),
            "error"
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            "model.group_id" => "nullable|required_without:model.advertisement_id|exists:groups,id,user_id,{$this->site->user_id}|unique:site_data.advertisements,group_id,NULL,id,type,{$this->getAdvertisementType()}",
            "model.advertisement_id" => "nullable|required_without:model.group_id|exists:advertisements,id,user_id,{$this->site->user_id},type,{$this->getAdvertisementType()}",
            "targets" => "array",
            "position" => "array",
            "position.main" => "required|in:" . join(',', array_keys(
                config(
                    "advertisement.positions.{$this->getAdvertisementType()}",
                    []
                )
            )),
            "tracker" => "array",
            "tracker.url" => "nullable|url|max:255|required_without:tracker.tracker.id",
            "tracker.tracker.id" => "nullable|exists:trackers,id,user_id,{$this->site->user_id}|required_without:tracker.url",
            "tracker.tracker.utm_medium" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.utm_campaign" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.utm_source" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.sid1" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.sid2" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.sid3" => "max:255|required_with:tracker.tracker.id",
            "posts.*" => "array",
            "posts.*.*" => "numeric|exists:site_data.posts,id,site_id,{$this->site->id}",
            "posts.include" => "required_without_all:categories.exact,categories.paths,urls.include.exact,urls.include.paths",
            "categories.*" => "array",
            "categories.*.*" => "numeric|exists:site_data.categories,id,site_id,{$this->site->id}",
            "categories.exact" => "required_without_all:categories.paths,posts.include,urls.include.exact,urls.include.paths",
            "categories.paths" => "required_without_all:categories.exact,posts.include,urls.include.exact,urls.include.paths",
            "urls.*.*" => "array",
            "urls.*.*.*" => "url",
            "urls.include.exact" => "required_without_all:urls.include.paths,posts.include,categories.exact,categories.paths",
            "urls.include.paths" => "required_without_all:urls.include.exact,posts.include,categories.exact,categories.paths"
        ];

    }

    /**
     *
     * Run changeGroup route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function changeGroup(string $identifier, array $data) {

        $advertisement = $this->getRepository()->findForGroupForSite(
            $data["group_id"]
        );

        $result = $this->getRepository()->changeGroupForSite(
            $identifier,
            $data,
            $this->getRepository()->decorate([
                "advertisement" => $advertisement,
                "site" => $this->site
            ], BeforeChangeGroupDecorator::class)
        );

        $this->setMessage(
            trans("t.updated")
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Validate changeGroup route action.
     *
     * @return array
     */
    public function validateChangeGroup() {

        $id = $this->request->route(
            "identifier"
        );

        $group_id = $this->request->get(
            "group_id"
        );

        return [
            "group_id" => "nullable|exists:groups,id,user_id,{$this->site->user_id}",
            "advertisement_id" => "required|numeric|exists:advertisements,id,user_id,{$this->site->user_id}|unique:site_data.advertisements,advertisement_id,{$id},id,group_id,{$group_id},type,{$this->getAdvertisementType()}"
        ];

    }

    /**
     *
     * Run update route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function update(string $identifier, array $data) {

        $result = false;

        $data = $this->getRepository()->decorate([
            "data" => $data,
            "identifier" => $identifier,
            "domain" => $this->site->url,
            "section" => $this
        ], StoreDecorator::class);

        if($this->getRepository()->checkTargetIsNotEmpty($data)) {

            $result = $this->getRepository()->updateObjectsForSite(
                $data
            );

        } else $this->setMessage(
            trans("t.site advertisement urls error"),
            "error"
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route(
            "identifier"
        );

        $group_id = $this->request->get(
            "group_id"
        );

        return [
            "group_id" => "nullable|exists:groups,id,user_id,{$this->site->user_id}",
            "advertisement_id" => "required|numeric|exists:advertisements,id,user_id,{$this->site->user_id}|unique:site_data.advertisements,advertisement_id,{$id},id,group_id,{$group_id},type,{$this->getAdvertisementType()}",
            "targets" => "array",
            "position" => "array",
            "position.main" => "required|in:" . join(',', array_keys(
                config(
                    "advertisement.positions.{$this->getAdvertisementType()}",
                    []
                )
            )),
            "tracker" => "array",
            "tracker.url" => "nullable|url|max:255|required_without:tracker.tracker.id",
            "tracker.tracker.id" => "nullable|exists:trackers,id,user_id,{$this->site->user_id}|required_without:tracker.url",
            "tracker.tracker.utm_medium" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.utm_campaign" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.utm_source" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.sid1" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.sid2" => "max:255|required_with:tracker.tracker.id",
            "tracker.tracker.sid3" => "max:255|required_with:tracker.tracker.id",
            "posts.*" => "array",
            "posts.*.*" => "numeric|exists:site_data.posts,id,site_id,{$this->site->id}",
            "posts.include" => "required_without_all:categories.exact,categories.paths,urls.include.exact,urls.include.paths",
            "categories.*" => "array",
            "categories.*.*" => "numeric|exists:site_data.categories,id,site_id,{$this->site->id}",
            "categories.exact" => "required_without_all:categories.paths,posts.include,urls.include.exact,urls.include.paths",
            "categories.paths" => "required_without_all:categories.exact,posts.include,urls.include.exact,urls.include.paths",
            "urls.*.*" => "array",
            "urls.*.*.*" => "url",
            "urls.include.exact" => "required_without_all:urls.include.paths,posts.include,categories.exact,categories.paths",
            "urls.include.paths" => "required_without_all:urls.include.exact,posts.include,categories.exact,categories.paths"
        ];

    }

    /**
     *
     * Run actions route action.
     *
     * @param $data
     * @return array
     */
    public function actions($data) {

        $result = $this->getRepository()->doActionForSite(
            $data['action'],
            $data['identifier']
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Run destroy route action.
     *
     * @param $identifier
     * @return array
     */
    public function destroy($identifier) {

        $result = $this->getRepository()->deleteForSite(
            $identifier
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Run loadForSelect route action.
     *
     * @param array $data
     * @return array
     */
    public function loadForSelect(array $data) {

        $repository = $this->getSubRepository(
            $data["relation"]
        );

        $closure = function($query) use ($data) {

            $query->where(
                "type",
                $this->getAdvertisementType()
            );

            if($data["relation"] == "groups") {

                $query->where(function($query) use ($data) {

                    $query->whereHas("advertisements", function($query) use ($data) {

                        return $query->active();

                    });

                    if(key_exists("update", $data) && $data["update"]) {

                        return $query->whereHas("sites", function($query) {

                            return $query->where(
                                "sites.id",
                                $this->site->id
                            );

                        });

                    }

                    if(key_exists("id", $data) && !empty($data["id"])) {

                        $query->orWhere(
                            "id",
                            $data["id"]
                        );

                    }

                    return $query;

                });

            } else {

                $query->where(function ($query) use ($data) {

                    $query->active();

                    if(key_exists("id", $data) && !empty($data["id"])) {

                        $query->orWhere(
                            "id",
                            $data["id"]
                        );

                    }

                    return $query;

                });

            }

            return $query;

        };

        $elements = $repository->pluckForUser(
            "name",
            "id",
            $closure
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements,
            "type" => $repository->getModelClass()
        ]);

    }

    /**
     *
     * Validate loadForSelect route action.
     *
     * @return array
     */
    public function validateLoadForSelect() {

        return [
            "relation" => "required|in:groups,advertisements",
            "id" => "nullable|numeric"
        ];

    }

    /**
     *
     * Run loadGroups route action.
     *
     * @return array
     */
    public function loadGroups() {

        $elements = $this->getSubRepository(
            "groups"
        )->pluckForUser(
            "name",
            "id",
            function($query) {

                return $query->where(
                    "type",
                    $this->getAdvertisementType()
                );

            }
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements
        ]);

    }

    /**
     *
     * Run loadTrackers route action.
     *
     * @return array
     */
    public function loadTrackers() {

        $elements = $this->getSubRepository(
            "trackers"
        )->pluckForUser(
            "url",
            "id"
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements
        ]);

    }

    /**
     *
     * Run loadPosts route action.
     *
     * @return array
     */
    public function loadPosts() {

        $elements = $this->getSubRepository(
            "posts"
        )->pluckForSite(
            "title",
            "id"
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements
        ]);

    }

    /**
     *
     * Run loadCategories route action.
     *
     * @return array
     */
    public function loadCategories() {

        $elements = $this->getSubRepository(
            "categories"
        )->pluckForSite(
            "title",
            "id"
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements
        ]);

    }

    /**
     *
     * Run loadTargets route action.
     *
     * @return array
     */
    public function loadTargets() {

        $elements = $this->getSubRepository(
            "targets"
        )->pluck(
            "value",
            "id"
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements
        ]);

    }

    /**
     * @return array
     */
    public function loadPositions() {

        $elements = $this->getRepository()->getPositions(
            $this->getAdvertisementType()
        );

        return $this->toJson([
            "status" => true,
            "elements" => $elements
        ]);

    }

    /**
     *
     * Decorate posts repository constructor.
     *
     * @param string $repository
     * @param string $model
     * @return mixed
     */
    public function getSubPostsRepository(string $repository, string $model) {

        return $this->loadRepostioryWithSite(
            $repository,
            $model
        );

    }

    /**
     *
     * Decorate categories repository constructor.
     *
     * @param string $repository
     * @param string $model
     * @return mixed
     */
    public function getSubCategoriesRepository(string $repository, string $model) {

        return $this->loadRepostioryWithSite(
            $repository,
            $model
        );

    }

    /**
     *
     * Decorate repository constructor.
     *
     * @param string $repository
     * @param string $model
     * @return mixed
     */
    private function loadRepostioryWithSite(string $repository, string $model) {

        return new $repository(
            new $model,
            $this->site
        );

    }

    /**
     *
     * Get validation messages.
     *
     * @return array
     */
    public function getMessages() : array {

        return [
            "model.group_id.required_without" => "Выберите группу или объявление",
            "model.group_id.unique" => "Данная группа уже настроена для сайта",
            "model.advertisement_id.required_without" => "Выберите группу или объявление",
            "position.main.*" => "Укажите корректную позицию",
            "tracker.url.required_without_all" => "Укажите URL объявления",
            "tracker.tracker.id.required_without_all" => "Укажите трекер для объявления",
            "advertisement_id.unique" => "Данное объявление уже находится в указанной группе",
            "urls.include.exact.*.url" => "Один из URL в списке \"Страницы (точный URL)\" имеет ошибочный формат",
            "urls.include.paths.*.url" => "Один из URL в списке \"Пути страниц\" имеет ошибочный формат",
            "urls.exclude.exact.*.url" => "Один из URL в списке \"Исключить страницы (точный URL)\" имеет ошибочный формат",
            "urls.exclude.paths.*.url" => "Один из URL в списке \"Исключить пути страниц\" имеет ошибочный формат"
        ];

    }

}
