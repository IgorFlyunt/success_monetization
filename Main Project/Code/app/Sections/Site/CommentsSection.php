<?php

namespace App\Sections\Site;

use App\Models\Site\Comment;
use App\Sections\CommentsSection as BaseCommentsSection;

/**
 * Class CommentsSection
 * @package App\Sections\Site
 */
class CommentsSection extends BaseCommentsSection {

    /**
     *
     * Route parameters which are not in use while build alias.
     *
     * @var array
     */
    public $unused_route_params = [
        "site_id"
    ];

    /**
     *
     * Get custom alias.
     *
     * @return null|string
     */
    public static function getAlias(): ?string
    {

        return "site/comments";

    }

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Comment::class;

    }

}
