<?php

namespace App\Sections\Site;

use App\Models\Synchronization;
use App\Repositories\SynchronizationsRepository;

/**
 * Class SynchronizationSection
 * @package App\Sections\Site
 */
class SynchronizationSection extends AbstractSection {

    /**
     *
     * Array of disabled route actions.
     *
     * @var array
     */
    public $disabled = [
        "create",
        "store",
        "actions",
        "edit",
        "update",
        "destroy"
    ];

    /**
     *
     * Get custom alias.
     *
     * @return null|string
     */
    public static function getAlias(): ?string
    {

        return "site";

    }

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Synchronization::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return SynchronizationsRepository::class;

    }

    /**
     *
     * Run index route action.
     *
     * @return array
     */
    public function index() {

        $this->setTitle(
            __FUNCTION__
        );

        $this->getSubRepository(
            "sites"
        )->loadRelation(
            $this->site,
            "synchronization"
        );

        return $this->toView([
            "default_periodicity" => config("synchronization.default_periodicity"),
            "periodicities" => config("synchronization.periodicities", []),
            "alias" => $this->buildAlias(),
            "object" => $this->site
        ]);

    }

    /**
     *
     * Run updateSync route action.
     *
     * @param array $data
     * @return array
     */
    public function updateSync(array $data) {

        $result = $this->getRepository()->saveForSite(
            $data
        );

        $this->setMessage(
            trans("t.updated")
        );

        return $this->toJson([
            "status" => (bool)$result,
            "object" => $result
        ]);

    }

    /**
     *
     * Validate updateSync route action.
     *
     * @return array
     */
    public function validateUpdateSync() {

        return [
            'key' => 'required|string|min:20|max:20'
        ];

    }

    /**
     *
     * Run syncAction route action.
     *
     * @return array
     */
    public function syncAction() {

        $result = $this->getSubRepository(
            "sites"
        )->doActionForUser(
            "sync",
            $this->site_id
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Get message after success execution of syncAction.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function syncActionSuccess() {

        return trans("t.success");

    }

}
