<?php

namespace App\Sections;

use App\Models\Group;
use App\Models\Advertisement;
use App\Repositories\GroupsRepository;
use App\Repositories\AdvertisementsRepository;
use App\Decorators\Groups\DataTableDecorator;

/**
 * Class GroupsSection
 * @package App\Sections
 */
class GroupsSection extends AbstractSection {

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Group::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return GroupsRepository::class;

    }

    /**
     *
     * Get sub repositories configuration.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            "advertisements" => [
                "repo" => AdvertisementsRepository::class,
                "model" => Advertisement::class
            ]
        ];

    }

    /**
     *
     * Run index route action.
     *
     * @return array
     */
    public function index() {

        return $this->toView([
            "config" => $this->getDataTableConfig()
        ]);

    }

    /**
     *
     * Run load route action.
     *
     * @return array
     */
    public function load() {

        $elements = $this->getRepository()->getForUser();

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                DataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Get DataTable configuration for load route action.
     *
     * @return array
     */
    private function getDataTableConfig() {

        return [
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "name" => [
                    "title" => trans("t.fields.name")
                ],
                "type" => [
                    "title" => trans("t.fields.type")
                ],
                "advertisements" => [
                    "title" => trans("t.fields.advertisements"),
                    "sortable" => false
                ],
                "sites" => [
                    "title" => trans("t.fields.sites"),
                    "sortable" => false
                ]
            ],
            "actions" => [
                "edit",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ]
            ],
            "decorators" => [
                "advertisements" => "links-array",
                "sites" => "array"
            ]
        ];

    }

    /**
     *
     * Run create route action.
     *
     * @return array
     */
    public function create() {

        return $this->toView([
            "alias" => $this->buildAlias(),
            "types" => $this->getRepository()->getAdvertisementsTypes()
        ]);

    }

    /**
     *
     * Run edit route action.
     *
     * @param string $identifier
     * @return array
     */
    public function edit(string $identifier) {

        $object = $this->getRepository()->findForUser($identifier, [
            "advertisements"
        ]);

        $advertisements = $this->getSubRepository(
            "advertisements"
        )->getTypedArrayForUser(
            $object->type
        );

        return $this->toView([
            "alias" => $this->buildAlias(),
            "object" => $object,
            "advertisements" => $advertisements,
            "types" => $this->getRepository()->getAdvertisementsTypes()
        ]);

    }

    /**
     *
     * Run advertisements route action.
     *
     * @param array $data
     * @return array
     */
    public function advertisements(array $data) {

        $advertisements = $this->getSubRepository(
            "advertisements"
        )->getTypedArrayForUser(
            $data["type"]
        );

        return $this->toJson([
            "advertisements" => $advertisements,
            "status" => true
        ]);

    }

    /**
     *
     * Validate advertisements route action.
     *
     * @return array
     */
    public function validateAdvertisements() {

        return [
            "type" => "required|in:" . implode(',', config("advertisement.types", []))
        ];

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            'name' => "required|max:255|unique:groups,name,NULL,id,user_id,{$this->user->id}",
            'type' => 'required|in:' . implode(',', config("advertisement.types", [])),
            'advertisements' => 'array'
        ];

    }

    /**
     *
     * Run store route action.
     *
     * @param array $data
     * @return mixed
     */
    public function store(array $data) {

        $data = $this->filterAdvertisementsIds($data);

        return parent::store($data);

    }

    /**
     *
     * Run update route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function update(string $identifier, array $data) {

        $object = $this->getRepository()->findForUser($identifier);

        $data = $this->filterAdvertisementsIds($data);

        $result = $this->getRepository()->updateObjectForUser(
            $object,
            $data
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route("identifier");

        return [
            'name' => "required|max:255|unique:groups,name,$id,id,user_id,{$this->user->id}",
            'type' => 'required|in:' . implode(',', config("advertisement.types", [])),
            'advertisements' => 'array'
        ];

    }

    /**
     *
     * Get filtered identifiers for specified user.
     *
     * @param array $data
     * @return array
     */
    private function filterAdvertisementsIds(array $data) {

        if(key_exists("advertisements", $data) && sizeof($data["advertisements"])) {

            $data["advertisements"] = $this->getSubRepository(
                "advertisements"
            )->filterIdsForUser(
                $data["advertisements"]
            );

        }

        return $data;

    }

}
