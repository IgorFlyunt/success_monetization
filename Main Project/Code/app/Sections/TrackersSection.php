<?php

namespace App\Sections;

use App\Models\Tracker;
use App\Repositories\TrackersRepository;

/**
 * Class TrackersSection
 * @package App\Sections
 */
class TrackersSection extends AbstractSection {

    /**
     *
     * Array of disabled route actions.
     *
     * @var array
     */
    public $disabled = [
        "edit",
        "create",
        "actions"
    ];

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Tracker::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return TrackersRepository::class;

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            "url" => "required|url|max:255|unique:trackers,url,NULL,id,user_id,{$this->user->id}"
        ];

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route("identifier");

        return [
            "url" => "required|url|max:255|unique:trackers,url,$id,id,user_id,{$this->user->id}"
        ];

    }

}
