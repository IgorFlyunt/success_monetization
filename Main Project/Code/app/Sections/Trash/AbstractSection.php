<?php

namespace App\Sections\Trash;

use App\Sections\AbstractSection as BaseAbstractSection;

/**
 * Class AbstractSection
 * @package App\Sections\Trash
 */
abstract class AbstractSection extends BaseAbstractSection {

    /**
     *
     * Array of disabled route actions.
     *
     * @var array
     */
    public $disabled = [
        "edit",
        "create",
        "store",
        "update"
    ];

    /**
     *
     * Run destroy route action.
     *
     * @param $identifier
     * @return array
     */
    public function destroy($identifier) {

        $result = $this->getRepository()->deleteForUser(
            $identifier,
            true
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Run actions route action.
     *
     * @param $data
     * @return array
     */
    public function actions($data) {

        if(in_array($data["action"], ["delete", "destroy"])) {

            return $this->destroy($data["identifier"]);

        } else {

            $result = $this->getRepository()->doActionForUser(
                $data['action'],
                $data['identifier']
            );

        }

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

    /**
     *
     * Run empty route action.
     *
     * @return array
     */
    public function empty() {

        $result = $this->getRepository()->emptyTrashForUser();

        $this->setMessage(
            trans(
                "t.deleted"
            )
        );

        return $this->toJson([
            "status" => (bool)$result
        ]);

    }

}
