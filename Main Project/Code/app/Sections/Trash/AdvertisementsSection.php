<?php

namespace App\Sections\Trash;

use App\Models\Advertisement;
use App\Repositories\AdvertisementsRepository;
use App\Decorators\Trash\AdvertisementsDataTableDecorator;

/**
 * Class AdvertisementsSection
 * @package App\Sections\Trash
 */
class AdvertisementsSection extends AbstractSection {

    /**
     *
     * Get custom alias.
     *
     * @return null|string
     */
    public static function getAlias(): ?string
    {

        return "trash/advertisements";

    }

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Advertisement::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return AdvertisementsRepository::class;

    }

    /**
     *
     * Run index route action.
     *
     * @return array
     */
    public function index() {

        return $this->toView([
            "alias" => $this->buildAlias(),
            "tabs" => $this->getTabsConfig(),
            "config" => $this->getDataTableConfig()
        ]);

    }

    /**
     *
     * Run load route action.
     *
     * @param string $type
     * @return array
     */
    public function load(string $type) {

        $elements = $this->getRepository()->getDeletedForUserByType($type, [
            "categories"
        ]);

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                AdvertisementsDataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Load advertisement types.
     *
     * @return mixed
     */
    private function getTabsConfig() {

        return $this->getRepository()->getAdvertisementsTypes();

    }

    /**
     *
     * Get DataTable configuration for load route action.
     *
     * @return array
     */
    private function getDataTableConfig() {

        return [
            "id" => "originals",
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "name" => [
                    "title" => trans("t.fields.name")
                ],
                "categories" => [
                    "title" => trans("t.fields.categories")
                ],
                "deleted_at" => [
                    "title" => trans("t.fields.deleted at")
                ]
            ],
            "actions" => [
                "recover",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ],
                "recover" => [
                    "label" => trans("t.recover"),
                    "type" => "warning",
                    "text" => trans("t.rows will be recovered")
                ]
            ],
            "decorators" => [
                "categories" => "array"
            ]
        ];

    }

}
