<?php

namespace App\Sections\Trash;

use App\Models\Site;
use App\Repositories\SitesRepository;
use App\Decorators\Trash\SitesDataTableDecorator;

/**
 * Class SitesSection
 * @package App\Sections\Trash
 */
class SitesSection extends AbstractSection
{

    /**
     *
     * Get custom alias.
     *
     * @return null|string
     */
    public static function getAlias(): ?string
    {

        return "trash/sites";

    }

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass(): string
    {

        return Site::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass(): string
    {

        return SitesRepository::class;

    }

    /**
     *
     * Run index route action.
     *
     * @return array
     */
    public function index()
    {

        return $this->toView([
            "alias" => $this->buildAlias(),
            "config" => $this->getDataTableConfig()
        ]);

    }

    /**
     *
     * Run load route action.
     *
     * @return array
     */
    public function load()
    {

        $elements = $this->getRepository()->getDeletedForUser([
            "user"
        ]);

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                SitesDataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Get DataTable configuration for load route action.
     *
     * @return array
     */
    private function getDataTableConfig()
    {

        return [
            "id" => "originals",
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "url" => [
                    "title" => trans("t.fields.url")
                ],
                "user_id" => [
                    "title" => trans("t.fields.owner")
                ],
                "deleted_at" => [
                    "title" => trans("t.fields.deleted at")
                ]
            ],
            "actions" => [
                "recover",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ],
                "recover" => [
                    "label" => trans("t.recover"),
                    "type" => "warning",
                    "text" => trans("t.rows will be recovered")
                ]
            ],
            "decorators" => [
                "url" => "link"
            ]
        ];

    }

}
