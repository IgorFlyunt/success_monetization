<?php

namespace App\Sections;

use App\Models\Category;
use App\Repositories\CategoriesRepository;

/**
 * Class CategoriesSection
 * @package App\Sections
 */
class CategoriesSection extends AbstractSection {

    /**
     *
     * Array of disabled route actions.
     *
     * @var array
     */
    public $disabled = [
        "edit",
        "create",
        "actions"
    ];

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Category::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return CategoriesRepository::class;

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            "name" => "required|max:255|unique:categories,name,NULL,id,user_id,{$this->user->id}"
        ];

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route("identifier");

        return [
            "name" => "required|max:255|unique:categories,name,$id,id,user_id,{$this->user->id}"
        ];

    }

}
