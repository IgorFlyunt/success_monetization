<?php

namespace App\Sections;

use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\Section\IndexActionTrait;
use App\Traits\Section\CreateActionTrait;
use App\Traits\Section\StoreActionTrait;
use App\Traits\Section\EditActionTrait;
use App\Traits\Section\UpdateActionTrait;
use App\Traits\Section\DestroyActionTrait;
use App\Traits\Section\ActionsActionTrait;
use App\Traits\Section\BuildResponseTrait;
use App\Traits\Section\ValidationTrait;
use App\Traits\Section\MessagesTrait;
use App\Traits\Section\RepositoriesTrait;
use App\Traits\Section\PermissionsTrait;
use App\Traits\Section\AliasTrait;
use App\Interfaces\Section\SectionInterface;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Repository\RepositoryInterface;

/**
 * Class AbstractSection
 * @package App\Sections
 */
abstract class AbstractSection implements SectionInterface {

    use IndexActionTrait;
    use CreateActionTrait;
    use StoreActionTrait;
    use EditActionTrait;
    use UpdateActionTrait;
    use DestroyActionTrait;
    use ActionsActionTrait;
    use BuildResponseTrait;
    use RepositoriesTrait;
    use PermissionsTrait;
    use ValidationTrait;
    use MessagesTrait;
    use AliasTrait;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Request
     */
    public $request;

    /**
     *
     * Route parameters which are not in use while build alias.
     *
     * @var array
     */
    public $unused_route_params = [];

    /**
     *
     * Array of disabled route actions.
     *
     * @var array
     */
    public $disabled = [];

    /**
     *
     * Array of route actions which are must be wrapped in database transaction.
     *
     * @var array
     */
    public $transaction_actions = [ //all transaction_actions must return array
        // "method" => "type" (json/view)
    ];

    /**
     *
     * Namespace of view folder.
     *
     * @var string
     */
    public $views_namespace = "dashboard::views.";

    /**
     *
     * Custom views prefix.
     *
     * @var
     */
    public $views_prefix;

    /**
     *
     * Array of messages which will be added to response.
     *
     * @var array
     */
    protected $messages = [];

    /**
     * @var \App\Interfaces\Repository\RepositoryInterface
     */
    private $repository;

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    private $model;

    /**
     * AbstractSection constructor.
     * @param User|null $user
     * @param Request $request
     */
    function __construct(User $user = null, Request $request) {

        $this->user = $user;

        $this->request = $request;

    }

    /**
     *
     * Check if loaded section has specified action.
     *
     * @param string $action
     * @return bool|int
     */
    public function actionExists(string $action) {

        if(!method_exists($this, $action) || in_array($action, $this->disabled)) {

            return 404;

        }

        if(!$this->checkAccess($action)) {

            return 401;

        }

        return true;

    }

    /**
     *
     * Call action after all checks and build response array.
     *
     * @param string $action
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    public function callAction(string $action, array $parameters) {

        $this->buildParameters($parameters);

        if(key_exists($action, $this->transaction_actions)) {

            $result = transaction_wrapper([
                "class" => $this,
                "method" => $action
            ], $parameters);

            $this->loadResultMessage($action, $result !== FALSE);

            if($this->transaction_actions[$action] === "view") {

                if($result !== FALSE) {

                    $view = null;

                    if(key_exists("view", $result)) {

                        $view = $result["view"];

                        unset($result["view"]);

                    }

                    $result = $this->toView(
                        $result,
                        $view
                    );

                } else {

                    $result = $this->toRedirect(
                        null,
                        true
                    );

                }

            } else {

                $result = $this->toJson(array_merge([
                    "status" => $result !== FALSE
                ], is_array($result) ? $result : []));

            }

        } else {

            $result = $this->$action(...$parameters);

            if($result["type"] !== "view" && empty($this->messages)) {

                $status = null;

                if($result["type"] === "json") {

                    $status = $result["result"]["status"];

                } elseif($result["type"] === "redirect") {

                    $status = !$result["result"]["error"];

                }

                if(!is_null($status)) {

                    $this->loadResultMessage($action, $status);

                }

            }

        }

        return $this->buildResponse($result, $action);

    }

    /**
     *
     * Get repository instance.
     *
     * @return RepositoryInterface
     */
    public function getRepository() : RepositoryInterface {

        $repository_class = $this->getRepositoryClass();

        if(!is_a($this->repository, $repository_class)) {

            $model_class = $this->getModelClass();

            $this->repository = (new $repository_class(
                new $model_class
            ))->setUser($this->user);

        }

        return $this->repository;

    }

    /**
     *
     * Get model instance.
     *
     * @return Model
     */
    public function getModel() : Model {

        $model_class = $this->getModelClass();

        if(!is_($this->model, $model_class)) {

            $this->model = new $model_class;

        }

        return $this->model;

    }

    /**
     *
     * Decorate parameters before request handling.
     *
     * @param array $parameters
     */
    private function buildParameters(array &$parameters) {

        if(sizeof($this->unused_route_params)) {

            foreach($this->request->route()->parameterNames as $param_name) {

                if(in_array($param_name, $this->unused_route_params)) {

                    $index = array_search(
                        $this->request->route($param_name),
                        $parameters
                    );

                    if($index !== false && $index >= 0) {

                        unset($parameters[$index]);

                    }

                }

            }

        }

        if(($data = $this->request->except(["_token"]))) {

            $parameters[] = $data;

        }

    }

}
