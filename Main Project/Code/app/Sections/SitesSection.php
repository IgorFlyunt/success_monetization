<?php

namespace App\Sections;

use App\Models\Site;
use App\Models\Advertisement;
use App\Sections\AbstractSection;
use App\Repositories\SitesRepository;
use App\Decorators\Sites\DataTableDecorator;
use App\Repositories\AdvertisementsRepository;

/**
 * Class SitesSection
 * @package App\Sections
 */
class SitesSection extends AbstractSection {

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Site::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return SitesRepository::class;

    }

    /**
     *
     * Get sub repositories configuration.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            "advertisements" => [
                "repo" => AdvertisementsRepository::class,
                "model" => Advertisement::class
            ]
        ];

    }

    /**
     *
     * Get run route action.
     *
     * @return array
     */
    public function index() {

        return $this->toView([
            "alias" => $this->buildAlias(),
            "config" => $this->getDataTableConfig()
        ]);

    }

    /**
     *
     * Get load route action.
     *
     * @return array
     */
    public function load() {

        $elements = $this->getRepository()->getForTableForUser();

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                DataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            "url" => "required|url|max:255|unique:sites,url,NULL,id,user_id,{$this->user->id}"
        ];

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route("identifier");

        return [
            "url" => "required|url|max:255|unique:sites,url,$id,id,user_id,{$this->user->id}"
        ];

    }

    /**
     *
     * Get DataTable configuration for load route action.
     *
     * @return array
     */
    private function getDataTableConfig() {

        return [
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "url" => [
                    "title" => trans("t.fields.url")
                ],
                "user_id" => [
                    "title" => trans("t.fields.owner")
                ],
                "status" => [
                    "title" => trans("t.fields.advertisements status")
                ],
                "api_status" => [
                    "title" => trans("t.fields.api status")
                ],
                "synchronized_at" => [
                    "title" => trans("t.fields.synchronized at"),
                    "sortable" => false
                ],
                "advertisements_actions" => [
                    "title" => trans("t.fields.advertisements actions"),
                    "sortable" => false
                ]
            ],
            "actions" => [
                "edit",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ],
                "open_in_new_tab" => [
                    "label" => trans("t.open in new tab"),
                    "type" => "info",
                    "text" => trans("t.rows will be opened in new tab"),
                    "handler" => [
                        "name" => "sitesOpenInNewTab"
                    ]
                ]
            ],
            "decorators" => [
                "url" => "link",
                "status" => [
                    "type" => "status",
                    "value" => trans("t.there is no advertisements")
                ],
                "api_status" => [
                    "type" => "status",
                    "value" => trans("t.plugin not installed")
                ],
                "advertisements_actions" => [
                    "type" => "actions",
                    "value" => [
                        "run",
                        "stop",
                        "link"
                    ]
                ],
                "synchronized_at" => "sync"
            ],
            "listeners" => [
                "synchronizationListener"
            ]
        ];

    }

}
