<?php

namespace App\Sections\Advertisements;

use App\Decorators\Advertisements\Context\OriginalDataTableDecorator;
use App\Decorators\Advertisements\Context\DuplicateDataTableDecorator;
use App\Decorators\Advertisements\Context\ChildrenDataTableDecorator;

/**
 * Class ContextsSection
 * @package App\Sections\Advertisements
 */
class ContextsSection extends AbstractSection {

    /**
     *
     * Run index route action.
     *
     * @return array
     */
    public function index() {

        return $this->toView([
            "config" => [
                "originals" => $this->getDataTableConfigForOriginals(),
                "duplications" => $this->getDataTableConfigForDuplications()
            ]
        ]);

    }

    /**
     *
     * Run createTemplate route action.
     *
     * @param string|null $identifier
     * @return array
     */
    public function createTemplate(string $identifier = null) {

        $templates = $this->getRepository()->getTemplatesForSelectForUser();

        $this->setResponseData(
            "not_template",
            !is_null($identifier)
        )->setResponseData(
            "templates",
            $templates
        )->setResponseData(
            "template",
            $identifier
        );

        return $this->create(true);

    }

    /**
     *
     * Run editTemplate route action.
     *
     * @param string $identifier
     * @return array
     */
    public function editTemplate(string $identifier) {

        $object = $this->getRepository()->findForUser($identifier, [
            "file",
            "categories",
            "comments" => function($query) {

                return $query->decorateDate()->dateOrdered();

            }
        ]);

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        return $this->toView([
            "alias" => $this->buildAlias(),
            "object" => $object,
            "categories_list" => $categories_list,
            "config" => $this->getDataTableConfigForChildren()
        ], "edit");

    }

    /**
     *
     * Run copyTemplate route action.
     *
     * @param string $identifier
     * @return array
     */
    public function copyTemplate(string $identifier) {

        $object = $this->getRepository()->findForUser($identifier, [
            "file",
            "categories"
        ]);

        $this->getRepository()->prepareCopy(
            $object,
            true
        );

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        return $this->toView([
            "copy" => true,
            "alias" => $this->buildAlias(),
            "object" => $object,
            "categories_list" => $categories_list
        ], "edit");

    }

    /**
     *
     * Run showTemplates route action.
     *
     * @return array
     */
    public function showTemplates() {

        $elements = $this->getRepository()->getTemplatesForUser();

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        return $this->toView([
            "alias" => $this->buildAlias(),
            "elements" => $elements,
            "categories_list" => $categories_list
        ], "templates");

    }

    /**
     *
     * Run loadOriginal route action.
     *
     * @return array
     */
    public function loadOriginal() {

        $elements = $this->getRepository()->getForTableForUser(
            "context"
        );

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                OriginalDataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Run loadDuplicate route action.
     *
     * @return array
     */
    public function loadDuplicate() {

        $elements = $this->getRepository()->getForTableForUser(
            "context",
            false
        );

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                DuplicateDataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Run loadChildren route action.
     *
     * @param string $identifier
     * @return array
     */
    public function loadChildren(string $identifier) {

        $elements = $this->getRepository()->loadChildrenForUser(
            $identifier
        );

        return $this->toJson([
            "status" => true,
            "elements" => $this->getRepository()->decorate(
                $elements,
                ChildrenDataTableDecorator::class
            )
        ]);

    }

    /**
     *
     * Get DataTable configuration for loadOriginal method.
     *
     * @return array
     */
    private function getDataTableConfigForOriginals() {

        return [
            "id" => "originals",
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "name" => [
                    "title" => trans("t.fields.name")
                ],
                "groups" => [
                    "title" => trans("t.fields.groups"),
                    "sortable" => false
                ],
                "children" => [
                    "title" => trans("t.fields.double"),
                    "sortable" => false
                ],
                "categories" => [
                    "title" => trans("t.fields.categories"),
                    "sortable" => false
                ],
                "status" => [
                    "title" => trans("t.fields.status")
                ],
                "sites" => [
                    "title" => trans("t.fields.sites"),
                    "sortable" => false
                ],
                "comments" => [
                    "title" => trans("t.fields.comments"),
                    "sortable" => false
                ]
            ],
            "actions" => [
                "run",
                "stop",
                "preview",
                "copy",
                "edit",
                [
                    "type" => "delete",
                    "event" => [
                        "name" => "advertisementsParentDeletedEvent",
                        "decorator" => "advertisementsParentDeletedDecorator",
                        "target_id" => "children"
                    ]
                ]
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted"),
                    "event" => [
                        "name" => "advertisementsParentDeletedEvent",
                        "decorator" => "advertisementsParentDeletedDecorator",
                        "target_id" => "children"
                    ]
                ]
            ],
            "decorators" => [
                "status" => "status",
                "groups" => "array",
                "categories" => "array",
                "sites" => "array",
                "children" => "list-modal",
                "comments" => [
                    "type" => "comments",
                    "modelable_type" => $this->getModelClass()
                ]
            ]
        ];

    }

    /**
     *
     * Get DataTable configuration for loadDuplicate method.
     *
     * @return array
     */
    private function getDataTableConfigForDuplications() {

        return [
            "id" => "children",
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "name" => [
                    "title" => trans("t.fields.name")
                ],
                "groups" => [
                    "title" => trans("t.fields.groups"),
                    "sortable" => false
                ],
                "parent" => [
                    "title" => trans("t.fields.parent"),
                    "sortable" => false
                ],
                "categories" => [
                    "title" => trans("t.fields.categories"),
                    "sortable" => false
                ],
                "status" => [
                    "title" => trans("t.fields.status")
                ],
                "sites" => [
                    "title" => trans("t.fields.sites"),
                    "sortable" => false
                ],
                "comments" => [
                    "title" => trans("t.fields.comments"),
                    "sortable" => false
                ]
            ],
            "actions" => [
                "run",
                "stop",
                "preview",
                "edit",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ]
            ],
            "decorators" => [
                "status" => "status",
                "groups" => "array",
                "categories" => "array",
                "sites" => "array",
                "parent" => "link",
                "comments" => [
                    "type" => "comments",
                    "modelable_type" => $this->getModelClass()
                ]
            ]
        ];

    }

    /**
     *
     * Get DataTable configuration for loadChildren method.
     *
     * @return array
     */
    public function getDataTableConfigForChildren() {

        return [
            "alias" => $this->buildAlias(),
            "identifier" => "id",
            "columns" => [
                "id" => [
                    "title" => trans("t.fields.id")
                ],
                "name" => [
                    "title" => trans("t.fields.name")
                ],
                "groups" => [
                    "title" => trans("t.fields.groups"),
                    "sortable" => false
                ],
                "categories" => [
                    "title" => trans("t.fields.categories"),
                    "sortable" => false
                ],
                "status" => [
                    "title" => trans("t.fields.status")
                ],
                "sites" => [
                    "title" => trans("t.fields.sites"),
                    "sortable" => false
                ],
                "comments" => [
                    "title" => trans("t.fields.comments"),
                    "sortable" => false
                ]
            ],
            "actions" => [
                "preview",
                "edit",
                "delete"
            ],
            "list_actions" => [
                "delete" => [
                    "label" => trans("t.delete"),
                    "type" => "warning",
                    "text" => trans("t.rows will be deleted")
                ]
            ],
            "decorators" => [
                "status" => "status",
                "groups" => "array",
                "categories" => "array",
                "sites" => "array",
                "comments" => [
                    "type" => "comments",
                    "modelable_type" => $this->getModelClass()
                ]
            ]

        ];

    }

}
