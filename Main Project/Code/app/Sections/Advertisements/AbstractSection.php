<?php

namespace App\Sections\Advertisements;

use App\Models\Group;
use App\Models\Category;
use App\Models\Advertisement;
use App\Repositories\AdvertisementsRepository;
use App\Repositories\CategoriesRepository;
use App\Repositories\GroupsRepository;
use App\Sections\AbstractSection as BaseAbstractSection;

/**
 * Class AbstractSection
 * @package App\Sections\Advertisements
 */
abstract class AbstractSection extends BaseAbstractSection {

    /**
     * @var string
     */
    public $views_namespace = "dashboard::views.advertisements.";

    /**
     *
     * Get model class.
     *
     * @return string
     */
    public function getModelClass() : string {

        return Advertisement::class;

    }

    /**
     *
     * Get repository class.
     *
     * @return string
     */
    public function getRepositoryClass() : string {

        return AdvertisementsRepository::class;

    }

    /**
     *
     * Get sub repositories configuration.
     *
     * @return array
     */
    public function subRepositories() : array {

        return [
            "categories" => [
                "repo" => CategoriesRepository::class,
                "model" => Category::class
            ],
            "groups" => [
                "repo" => GroupsRepository::class,
                "model" => Group::class
            ]
        ];

    }

    /**
     *
     * Run create route action.
     *
     * @param bool $is_template
     * @return array
     */
    public function create(bool $is_template = false) {

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        if(!$is_template) {

            $this->setResponseData(
                "groups_list",
                $this->getSubRepository(
                    "groups"
                )->pluckForUser(
                    "name",
                    "id"
                )
            );

        }

        return $this->toView([
            "alias" => $this->buildAlias(),
            "categories_list" => $categories_list
        ], "create");

    }

    /**
     *
     * Run edit route action.
     *
     * @param string $identifier
     * @return array
     */
    public function edit(string $identifier) {

        $object = $this->getRepository()->findHasChildrenForUser($identifier, [
            "file",
            "categories",
            "groups",
            "comments" => function($query) {

                return $query->decorateDate()->dateOrdered();

            }
        ]);

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        $groups_list = $this->getSubRepository(
            "groups"
        )->pluckForUser("name", "id");

        return $this->toView([
            "alias" => $this->buildAlias(),
            "object" => $object,
            "groups_list" => $groups_list,
            "categories_list" => $categories_list,
            "config" => $this->getDataTableConfigForChildren()
        ]);

    }

    /**
     *
     * Run showCopy route action.
     *
     * @param string $identifier
     * @return array
     */
    public function showCopy(string $identifier) {

        $object = $this->getRepository()->findForUser($identifier, [
            "file",
            "categories",
            "groups"
        ]);

        $this->getRepository()->prepareCopy($object);

        $categories_list = $this->getSubRepository(
            "categories"
        )->pluckForUser("name", "id");

        $groups_list = $this->getSubRepository(
            "groups"
        )->pluckForUser("name", "id");

        return $this->toView([
            "copy" => true,
            "alias" => $this->buildAlias(),
            "object" => $object,
            "groups_list" => $groups_list,
            "categories_list" => $categories_list
        ], "edit");

    }

    /**
     *
     * Run saveCopy route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function saveCopy(string $identifier, array $data) {

        $object = $this->getRepository()->findForUser($identifier, [
            "file"
        ]);

        $result = $this->getRepository()->copyForUser(
            $object,
            $data
        );

        $this->setMessage(trans("t.copied"));

        return $this->toJson([
            "status" => (bool)$result,
            "id" => $result->id
        ]);

    }

    /**
     *
     * Validate toOriginal route action.
     *
     * @return array
     */
    public function validateToOriginal() {

        return [
            'delete_parent' => 'required|boolean'
        ];

    }

    /**
     *
     * Run toOriginal route action.
     *
     * @param string $identifier
     * @param array $data
     * @return array
     */
    public function toOriginal(string $identifier, array $data) {

        $object = $this->getRepository()->findForUser($identifier);

        $result = $this->getRepository()->toOriginalForUser(
            $object,
            key_exists("delete_parent", $data) && $data["delete_parent"]
        );

        $this->setMessage(trans("t.successfully moved"));

        return $this->toJson([
            "status" => $result
        ]);

    }

    /**
     *
     * Validate store route action.
     *
     * @return array
     */
    public function validateStore() {

        return [
            'name' => "required|max:255|unique:advertisements,name,NULL,id,user_id,{$this->user->id}",
            'html' => 'required|max:1000',
            'css' => 'required|max:1000',
            'categories' => 'nullable|array',
            'groups' => 'nullable|array',
            'image' => 'nullable|image|max:1024',
            'file_id' => 'nullable|numeric|exists:files,id',
            'is_template' => 'boolean'
        ];

    }

    /**
     *
     * Validate saveCopy route action.
     *
     * @return array
     */
    public function validateSaveCopy() {

        return $this->validateStore();

    }

    /**
     *
     * Validate update route action.
     *
     * @return array
     */
    public function validateUpdate() {

        $id = $this->request->route("identifier");

        return [
            'name' => "required|max:255|unique:advertisements,name,$id,id,user_id,{$this->user->id}",
            'html' => 'required|max:1000',
            'css' => 'required|max:1000',
            'categories' => 'nullable|array',
            'groups' => 'nullable|array',
            'image' => 'nullable|image|max:1024',
            'file_id' => 'nullable|numeric|exists:files,id',
            'is_template' => 'boolean'
        ];

    }

}
