<?php

namespace App\Events\User;

/**
 * Class DeletedEvent
 *
 * Event after user is deleted.
 *
 * @package App\Events\User
 */
class DeletedEvent
{

    /**
     * @var array
     */
    public $ids;

    /**
     * DeletedEvent constructor.
     * @param array $ids
     */
    public function __construct(array $ids)
    {

        $this->ids = $ids;

    }

}
