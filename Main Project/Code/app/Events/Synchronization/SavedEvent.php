<?php

namespace App\Events\Synchronization;

use App\Models\Site;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SavedEvent
 *
 * Event after synchronization for site is changed.
 *
 * @package App\Events\Synchronization
 */
class SavedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var string
     */
    public $from;

    /**
     * @var string
     */
    public $to;

    /**
     * @var Site
     */
    public $site;

    /**
     * SavedEvent constructor.
     * @param string $from
     * @param string $to
     * @param Site $site
     */
    public function __construct(string $from, string $to, Site $site)
    {

        $this->from = $from;

        $this->to = $to;

        $this->site = $site;

    }

}
