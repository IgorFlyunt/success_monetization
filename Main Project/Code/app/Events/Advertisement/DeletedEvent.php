<?php

namespace App\Events\Advertisement;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class DeletedEvent
 *
 * Event after deleting advertisements.
 *
 * @package App\Events\Advertisement
 */
class DeletedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var array
     */
    public $ids;

    /**
     * @var User
     */
    public $user;

    /**
     * DeletedEvent constructor.
     * @param array $ids
     * @param User $user
     */
    public function __construct(array $ids, User $user)
    {

        $this->ids = $ids;

        $this->user = $user;

    }

}
