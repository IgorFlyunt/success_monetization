<?php

namespace App\Events\Advertisement;

use App\Models\Advertisement;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SavedEvent
 *
 * Event after saving the advertisement.
 *
 * @package App\Events\Advertisement
 */
class SavedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var Advertisement
     */
    public $model;

    /**
     * @var User
     */
    public $user;

    /**
     * @var mixed
     */
    public $tenant_id;

    /**
     * SavedEvent constructor.
     * @param Advertisement $model
     * @param User $user
     */
    public function __construct(Advertisement $model, User $user)
    {

        $this->model = $model;

        $this->user = $user;

        $this->tenant_id = $user->id;

    }

}
