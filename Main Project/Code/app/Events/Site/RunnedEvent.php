<?php

namespace App\Events\Site;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class RunnedEvent
 *
 * Event which changes status field at App\Models\Site\Advertisement for whole site.
 *
 * @package App\Events\Site
 */
class RunnedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var array
     */
    public $ids;

    /**
     * @var User
     */
    public $user;

    /**
     * RunnedEvent constructor.
     * @param array $ids
     * @param User $user
     */
    public function __construct(array $ids, User $user)
    {

        $this->ids = $ids;

        $this->user = $user;

    }


}
