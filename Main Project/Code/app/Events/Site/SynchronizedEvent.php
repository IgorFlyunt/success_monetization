<?php

namespace App\Events\Site;

use App\Models\Site;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SynchronizedEvent
 *
 * After synchronization(App\Jobs\Site\SynchronizationJob) job event for saving to database.
 *
 * @package App\Events\Site
 */
class SynchronizedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var Site
     */
    public $model;

    /**
     * SynchronizedEvent constructor.
     * @param Site $model
     */
    public function __construct(Site $model)
    {

        $this->model = $model;

    }

}
