<?php

namespace App\Events\Site;

use App\Models\Site;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class SynchronizationEvent
 *
 * After synchronization(App\Jobs\Site\SynchronizationJob) job event for websockets.
 *
 * @package App\Events\Site
 */
class SynchronizationEvent implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * @var Site
     */
    protected $site;

    /**
     * @var bool
     */
    protected $status;

    /**
     * @var array
     */
    protected $data;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Site $site, bool $status, array $data = [])
    {

        $this->site = $site;

        $this->status = $status;

        $this->data = $data;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user.{$this->site->user_id}");
    }

    /**
     * @return array
     */
    public function broadcastWith() {

        if($this->status) {

            $message = "sync successfull";

        } else $message = "sync error";

        return array_merge($this->data, [
            "site_id" => $this->site->id,
            "status" => $this->status,
            "message" => sprintf(
                trans("t.$message"),
                $this->site->url
            )
        ]);

    }

}
