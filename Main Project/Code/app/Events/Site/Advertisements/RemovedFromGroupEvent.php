<?php

namespace App\Events\Site\Advertisements;

use App\Models\Site;
use App\Models\Site\Advertisement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class RemovedFromGroupEvent
 *
 * Event after App\Models\Site\Advertisement removed from group.
 *
 * @package App\Events\Site\Advertisements
 */
class RemovedFromGroupEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var Site
     */
    public $site;

    /**
     * @var int
     */
    public $group_id;

    /**
     * @var Advertisement
     */
    public $model;

    /**
     * @var mixed
     */
    public $tenant_id;

    /**
     * RemovedFromGroupEvent constructor.
     * @param Advertisement $model
     * @param int $group_id
     * @param Site $site
     */
    public function __construct(Advertisement $model, int $group_id, Site $site)
    {

        $this->site = $site;

        $this->group_id = $group_id;

        $this->model = $model;

        $this->tenant_id = $site->user_id;

    }

}
