<?php

namespace App\Events\Site\Advertisements;

use App\Models\Site;
use App\Models\Site\Advertisement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SavedEvent
 *
 * Event after App\Models\Site\Advertisement is saved.
 *
 * @package App\Events\Site\Advertisements
 */
class SavedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var Advertisement
     */
    public $model;

    /**
     * @var Site
     */
    public $site;

    /**
     * @var mixed
     */
    public $tenant_id;

    /**
     * SavedEvent constructor.
     * @param Advertisement $model
     * @param Site $site
     */
    public function __construct(Advertisement $model, Site $site)
    {

        $this->model = $model;

        $this->site = $site;

        $this->tenant_id = $site->user_id;

    }

}
