<?php

namespace App\Events\Site\Advertisements;

use App\Models\Site;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class DeletedEvent
 *
 * Event after deleting or status field is changed to false at App\Models\Site\Advertisement.
 *
 * @package App\Events\Site\Advertisements
 */
class DeletedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var array
     */
    public $ids;

    /**
     * @var Site
     */
    public $site;

    /**
     * @var bool
     */
    public $force_delete;

    /**
     * DeletedEvent constructor.
     * @param array $ids
     * @param Site $site
     * @param bool $force_delete
     */
    public function __construct(array $ids, Site $site, bool $force_delete = false)
    {

        $this->ids = $ids;

        $this->site = $site;

        $this->force_delete = $force_delete;

    }

}
