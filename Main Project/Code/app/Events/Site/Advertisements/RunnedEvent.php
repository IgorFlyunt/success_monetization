<?php

namespace App\Events\Site\Advertisements;

use App\Models\Site;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class RunnedEvent
 *
 * Event after status field is changed to true in App\Models\Site\Advertisement.
 *
 * @package App\Events\Site\Advertisements
 */
class RunnedEvent
{
    use Dispatchable, SerializesModels;

    /**
     * @var array
     */
    public $ids;

    /**
     * @var Site
     */
    public $site;

    /**
     * RunnedEvent constructor.
     * @param array $ids
     * @param Site $site
     */
    public function __construct(array $ids, Site $site)
    {

        $this->ids = $ids;

        $this->site = $site;

    }

}
