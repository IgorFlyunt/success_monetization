<?php

namespace App\Events\Site;

use App\Models\Site;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SavedEvent
 *
 * Event after site is saved.
 *
 * @package App\Events\Site
 */
class SavedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var Site
     */
    public $site;

    /**
     * SavedEvent constructor.
     * @param Site $site
     */
    public function __construct(Site $site)
    {

        $this->site = $site;

    }

}
