<?php

namespace App\Events\Tracker;

use App\Models\User;
use App\Models\Tracker;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class SavedEvent
 *
 * Event after tracker is saved.
 *
 * @package App\Events\Tracker
 */
class SavedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var Tracker
     */
    public $tracker;

    /**
     * SavedEvent constructor.
     * @param Tracker $tracker
     * @param User $user
     */
    public function __construct(Tracker $tracker, User $user)
    {

        $this->user = $user;

        $this->tracker = $tracker;

    }

}
