<?php

namespace App\Events\Tracker;

use App\Models\User;
use App\Models\Tracker;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class DeletedEvent
 *
 * Event after tracker is deleted.
 *
 * @package App\Events\Tracker
 */
class DeletedEvent
{

    use Dispatchable, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var array
     */
    public $ids;

    /**
     * DeletedEvent constructor.
     * @param array $ids
     * @param User $user
     */
    public function __construct(array $ids, User $user)
    {

        $this->user = $user;

        $this->ids = $ids;

    }

}
