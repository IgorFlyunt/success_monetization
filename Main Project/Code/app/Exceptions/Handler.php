<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

/**
 * Class Handler
 *
 * Default Laravel exception handler.
 *
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if($this->isHttpException($exception)) {

            switch ($exception->getStatusCode()) {

                case 401:

                    if($request->expectsJson()) {

                        return response()->json([
                            'error' => 'Permission denied.',
                            'messages' => [
                                trans("t.dont allowed")
                            ]
                        ], 401);

                    }

                    flash_messages(
                        trans("t.dont allowed"),
                        'error'
                    );

                    return redirect(route("login"));

                break;

            }

        }

        return parent::render($request, $exception);

    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json([
                'error' => 'Unauthenticated.',
                'messages' => [
                    trans("t.dont allowed")
                ]
            ], 401);
        }

        return redirect()->guest(route('login'));
    }

    /**
     *
     * Converts validation exception to required response format.
     *
     * @param ValidationException $e
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|null|\Symfony\Component\HttpFoundation\Response
     */
    public function convertValidationExceptionToResponse(ValidationException $e, $request)
    {

        $errors = $e->validator->errors()->getMessages();

        if ($e->response && !$request->expectsJson()) {

            $all_errors = array();

            foreach ($errors as $field_errors) {

                $all_errors = array_merge($all_errors, $field_errors);

            }

            flash_messages([
                [
                    "type" => "error",
                    "title" => trans("t.validation error"),
                    "html" => implode('<br />', $all_errors)
                ]
            ]);

            return $e->response;
        }

        if ($request->expectsJson()) {
            return response()->json(["messages" => $errors], 422);
        }

        return redirect()->back()->withInput(
            $request->input()
        )->withErrors($errors);
    }

}
