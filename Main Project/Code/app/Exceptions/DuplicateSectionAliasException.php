<?php

namespace App\Exceptions;

use Exception;

/**
 * Class DuplicateSectionAliasException
 *
 * Exception used if sections have duplicated alias.
 *
 * @package App\Exceptions
 */
class DuplicateSectionAliasException extends Exception
{

    /**
     * Create a new exception instance.
     */
    public function __construct(string $alias, string $section)
    {
        parent::__construct("Alias [$alias, $section] duplicated!");
    }

}
