<?php

namespace App\Widgets;

use App\Models\User;

/**
 * Class SidebarWidget
 * @package App\Widgets
 */
class SidebarWidget extends AbstractWidget {

    /**
     * @var User
     */
    protected $user;

    /**
     * @var string
     */
    protected $route_parameter;

    /**
     * SidebarWidget constructor.
     * @param User $user
     * @param string $config
     * @param string|null $route_parameter
     */
    public function __construct(
        User $user,
        string $config = "navigation",
        string $route_parameter = null
    ) {

        $this->user = $user;

        $this->config = $config;

        $this->route_parameter = $route_parameter;

    }

    /**
     *
     * Configure view before render.
     *
     * @return string
     * @throws \Exception
     */
    public function render() : string {

        return $this->view("sidebar.index", [
            "elements" => $this->getElements()
        ]);

    }

    /**
     *
     * Decorate sidebar configuration.
     *
     * @return array
     * @throws \Exception
     */
    private function getElements() {

        $elements = [];

        foreach(config($this->config, []) as $record) {

            $elements[] = $this->buildOne($record);

        }

        return $elements;

    }

    /**
     *
     * Decorate sidebar configuration row.
     *
     * @param array $record
     * @param string $icon
     * @return array
     * @throws \Exception
     */
    private function buildOne(array $record, string $icon = "fa fa-folder") {

        $element = [
            "active" => false,
            "title" => trans(
                "{$this->config}.{$record["title"]}"
            )
        ];

        if(key_exists("icon", $record)) {

            $icon = $record["icon"];

        }

        $element["icon"] = $icon;

        if(key_exists("route", $record)) {

            $route = $this->decorateRoute($record);

            if(is_array($route)) {

                $url = route(...$route);

            } else $url = route($route);

            $matcher = null;

            if(key_exists("matcher", $record)) {

                $matcher = $record["matcher"];

            }

            $element["active"] = is_active_url(
                $url,
                $matcher
            );

            $element["url"] = $url;

        };

        if(key_exists("elements", $record)) {

            if(!key_exists("elements", $element)) {

                $element["elements"] = [];

            }

            foreach($record["elements"] as $child) {

                $child = $this->buildOne($child, "fa fa-circle-o");

                $element["elements"][] = $child;

                if(!$element["active"]) {

                    $element["active"] = $child["active"];

                }

            }

        }

        return $element;

    }

    /**
     *
     * Decorate url for sidebar configuration row.
     *
     * @param array $record
     * @return array|mixed
     * @throws \Exception
     */
    private function decorateRoute(array $record) {

        $route = $record["route"];

        if(!key_exists("route_parameter", $record) || $record["route_parameter"] != false) {

            if(!is_null($this->route_parameter)) {

                if(($route_value = request()->route($this->route_parameter)) !== null) {

                    if(is_array($route)) {

                        if(sizeof($route) != 2) {

                            throw new \Exception("Wrong [{$this->config}] configuration.");

                        }

                        if(!is_array($route[1])) {

                            $route[1] = [$route[1]];

                        }

                        array_unshift($route[1], $route_value);

                    } else {

                        $route = [$route, $route_value];

                    }

                }

            }

        }

        return $route;

    }

}
