<?php

namespace App\Widgets;

use App\Interfaces\Widget\WidgetInterface;

/**
 * Class AbstractWidget
 * @package App\Widgets
 */
abstract class AbstractWidget implements WidgetInterface {

    /**
     * @var array
     */
    public $config;

    /**
     * AbstractWidget constructor.
     * @param array $config
     */
    function __construct(array $config) {

        $this->config = $config;

    }

    /**
     *
     * Return ready blade view.
     *
     * @param string $view
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $view, array $data = []) {

        return view("widgets::$view")->with($this->config)->with($data);

    }

}
