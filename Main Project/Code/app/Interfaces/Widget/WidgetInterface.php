<?php

namespace App\Interfaces\Widget;

/**
 * Interface WidgetInterface
 *
 * Need to be implemented in all widgets for type check.
 *
 * @package App\Interfaces\Widget
 */
interface WidgetInterface {

    /**
     * @return string
     */
    public function render() : string;

}
