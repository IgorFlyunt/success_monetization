<?php

namespace App\Interfaces\Decorator;

/**
 * Interface DecoratorInterface
 *
 * Need to be implemented in all decorators for type check.
 *
 * @package App\Interfaces\Decorator
 */
interface DecoratorInterface {

    /**
     * @return mixed
     */
    public function decorate();

}
