<?php

namespace App\Interfaces\Section;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Repository\RepositoryInterface;

/**
 * Interface SectionInterface
 *
 * Need to be implemented in all sections for type check.
 *
 * @package App\Interfaces\Section
 */
interface SectionInterface {

    /**
     * @return string
     */
    public function getModelClass() : string;

    /**
     * @return string
     */
    public function getRepositoryClass() : string;

    /**
     * @return Model
     */
    public function getModel() : Model;

    /**
     * @return RepositoryInterface
     */
    public function getRepository() : RepositoryInterface;

    /**
     *
     * Specify if section is only for authorized users.
     *
     * @return bool
     */
    public function authOnly() : bool;

}
