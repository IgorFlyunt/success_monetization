<?php

namespace App\Interfaces\Repository;

/**
 * Interface RepositoryInterface
 *
 * Need to be implemented in all repositories for type check.
 *
 * @package App\Interfaces\Repository
 */
interface RepositoryInterface {}
