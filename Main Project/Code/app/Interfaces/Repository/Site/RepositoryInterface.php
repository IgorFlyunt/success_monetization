<?php

namespace App\Interfaces\Repository\Site;

/**
 * Interface RepositoryInterface
 *
 * Need to be implemented in all repositories which have App/Traits/Repository/Sites/ForSiteMethodsTrait
 *
 * @package App\Interfaces\Repository\Site
 */
interface RepositoryInterface {

    /**
     * @return string
     */
    public function getUniqueField() : string;

    /**
     * @param array $data
     * @return mixed
     */
    public function saveForSiteBody(array $data);

}
