<?php

namespace App\Interfaces\Policy;

/**
 * Interface PolicyInterface
 *
 * Need to be implemented at all policies for type check.
 *
 * @package App\Interfaces\Policy
 */
interface PolicyInterface {

    /**
     * @return bool
     *
     * Specify if policy must do pre check by condition.
     *
     */
    public function checkExistsForUser() : bool;

}
