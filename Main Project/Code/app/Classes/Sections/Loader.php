<?php

namespace App\Classes\Sections;

use Illuminate\Http\Request;

/**
 * Class Loader
 * @package App\Classes\Sections
 */
class Loader {

    /**
     * @var Request
     */
    protected $request;

    /**
     * Loader constructor.
     * @param Request $request
     */
    function __construct(Request $request) {

        $this->request = $request;

    }

    /**
     * @return int
     */
    public function loadSection() {

        if($this->request->route()->hasParameter("section")) {

            $alias = $this->buildAlias();

        } else $alias = $this->request->segment(1);

        $sections = app()->get("sections");

        if(key_exists($alias, $sections)) {

            $parameters = func_get_args();

            array_push($parameters, $this->request);

            $section = new $sections[$alias](...$parameters);

            $action = $this->request->route()->getActionMethod();

            $result = $section->actionExists($action);

            if($result === true) {

                $section->validateAction($action);

                $section::setAlias($alias);

                return $section;

            }

            unset($section);

            return $result;

        }

        return 404;

    }

    /**
     * @return null|string|string[]
     */
    private function buildAlias() {

        $uri = preg_replace(
            "@(.*\{section\})(.*)@",
            "$1",
            $this->request->route()->uri()
        );

        $parameters = $this->request->route()->parameterNames;

        $index = array_search("section", $parameters);

        if($index > 0) {

            $parameters = array_slice(
                $parameters,
                0,
                $index
            );

            foreach ($parameters as $parameter) {

                $uri = preg_replace(
                    "@/?\{$parameter\}@",
                    '',
                    $uri
                );

            }

        }

        return preg_replace(
            "@\{section\}@",
            $this->request->route("section"),
            $uri
        );

    }

}
