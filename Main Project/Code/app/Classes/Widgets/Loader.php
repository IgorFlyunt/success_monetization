<?php

namespace App\Classes\Widgets;

use App\Interfaces\Widget\WidgetInterface;

/**
 * Class Loader
 * @package App\Classes\Widgets
 */
class Loader {

    /**
     * @return string
     * @throws \Exception
     */
    public static function load() {

        $args = func_get_args();

        $widget = array_shift($args);

        if(is_string($widget)) {

            $widget = ucfirst($widget);

            $class = config("widget.namespace", '') . "\\{$widget}Widget";

            if(class_exists($class)) {

                $widget = new $class(...$args);

                if($widget instanceof WidgetInterface) {

                    return $widget->render();

                }

                unset($widget);

            }

        }

        throw new \Exception("Widget [$class] doesn't exists!");

    }

}
