<?php

namespace App\Classes\Redis;

use Predis\Collection\Iterator\Keyspace;

/**
 * Class ConfigStorage
 * @package App\Classes\Redis
 */
class ConfigStorage extends AbstractStorage
{

    /**
     * @var string
     */
    protected $connection = "config";

    /**
     * @param string $prefix
     * @param string $value
     * @param array $data
     */
    public function store(string $prefix, string $value, array $data)
    {

        $include = [];

        $exclude = [];

        $this->redis->pipeline(function ($pipe) use ($prefix, $data, $value, &$include, &$exclude) {

            foreach ($data as $type => $urls) {

                if ($type == "include") {

                    foreach ($urls as $url) {

                        $key = "$prefix:i:$url";

                        $pipe->sadd($key, $value);

                        $include[] = $key;

                    }

                } elseif ($type == "exclude") {

                    $key = "$prefix:e:$value";

                    foreach ($urls as $url) {

                        $pipe->sadd($key, $url);

                        $exclude[] = $url;

                    }

                }

            }

        });

        $this->clearIncludeWhileSaving(
            $prefix,
            $value,
            $include
        );

        $this->clearExcludeWhileSaving(
            $prefix,
            $value,
            $exclude
        );

    }

    /**
     * @param string $prefix
     * @param string $value
     * @param array $data
     */
    public function delete(string $prefix, string $value, array $data)
    {

        $this->redis->pipeline(function ($pipe) use ($prefix, $value, $data) {

            foreach ($data["include"] as $url) {

                $pipe->srem("$prefix:i:$url", $value);

            }

            foreach ($data["exclude"] as $url) {

                $pipe->srem("$prefix:e:$value", $url);

            }

        });

    }

    /**
     * @param string $prefix
     */
    public function deleteAll(string $prefix)
    {

        $this->redis->pipeline(function ($pipe) use ($prefix) {

            $pipe->del($prefix);

            foreach (new Keyspace($this->redis, "$prefix:*") as $key) {

                $pipe->del($key);

            }

        });

    }

    /**
     * @param string $prefix
     * @param array $urls
     */
    public function deleteDiff(string $prefix, array $urls)
    {

        $this->redis->pipeline(function ($pipe) use ($prefix, $urls) {

            foreach (new Keyspace($this->redis, "$prefix:i:*") as $key) {

                if (!in_array(preg_replace("@^.+:i:(.+)$@", "$1", $key), $urls)) {

                    $pipe->del($key);

                }

            }

            foreach (new Keyspace($this->redis, "$prefix:e:*") as $key) {

                $diff = array_diff(
                    $this->redis->smembers(
                        $key
                    ),
                    $urls
                );

                if (sizeof($diff)) {

                    $pipe->srem($key, $diff);

                }

            }

        });

    }

    /**
     * @param string $from
     * @param string $to
     */
    public function rename(string $from, string $to)
    {

        $this->redis->pipeline(function ($pipe) use ($from, $to) {

            foreach (new Keyspace($this->redis, "$from*") as $key) {

                $pipe->rename(
                    $key,
                    preg_replace(
                        "@^($from)(:.+)?$@",
                        "{$to}$2",
                        $key
                    )
                );

            }

        });

    }

    /**
     * @param string $prefix
     * @param array $values
     */
    public function deleteByValues(string $prefix, array $values)
    {

        $this->redis->pipeline(function ($pipe) use ($prefix, $values) {

            foreach (new Keyspace($this->redis, "$prefix:i:*") as $key) {

                $pipe->srem($key, $values);

            }

            foreach ($values as $value) {

                $pipe->del("$prefix:e:$value");

            }

        });

    }

    /**
     * @param string $prefix
     * @param string $domain
     */
    public function setSite(string $prefix, string $domain)
    {

        if ($this->redis->get($prefix) != $domain) {

            $this->redis->set($prefix, $domain);

        }

    }

    /**
     * @param string $prefix
     * @param string $value
     * @param array $keys
     */
    private function clearIncludeWhileSaving(string $prefix, string $value, array $keys)
    {

        $this->redis->pipeline(function ($pipe) use ($prefix, $value, $keys) {

            foreach (new Keyspace($this->redis, "$prefix:i:*") as $key) {

                if (!in_array($key, $keys)) {

                    $pipe->srem($key, $value);

                }

            }

        });

    }

    /**
     * @param string $prefix
     * @param string $value
     * @param array $urls
     */
    private function clearExcludeWhileSaving(string $prefix, string $value, array $urls)
    {

        $key = "$prefix:e:$value";

        $all_urls = $this->redis->smembers($key);

        $this->redis->pipeline(function ($pipe) use ($key, $all_urls, $urls) {

            foreach ($all_urls as $url) {

                if (!in_array($url, $urls)) {

                    $pipe->srem($key, $url);

                }

            }

        });

    }

}
