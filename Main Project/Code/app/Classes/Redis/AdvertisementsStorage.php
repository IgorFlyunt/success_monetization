<?php

namespace App\Classes\Redis;

use Predis\Collection\Iterator\Keyspace;

/**
 * Class AdvertisementsStorage
 * @package App\Classes\Redis
 */
class AdvertisementsStorage extends AbstractStorage
{

    /**
     * @var string
     */
    protected $connection = "advertisements";

    /**
     * @param string $prefix
     * @param int $advertisement
     * @param string $json
     * @param int|null $group
     */
    public function store(string $prefix, int $advertisement, string $json, int $group = null)
    {

        $this->redis->set("$prefix:a:$advertisement", $json);

        if(!is_null($group)) {

            $this->redis->sadd("$prefix:g:$group", $advertisement);

        }

    }

    /**
     * @param string $prefix
     * @param int $advertisement
     * @param int|null $group
     */
    public function delete(string $prefix, int $advertisement, int $group = null)
    {

        $this->redis->del("$prefix:a:$advertisement");

        if(!is_null($group)) {

            $this->redis->del("$prefix:q:$group");

            $this->redis->srem("$prefix:g:$group", $advertisement);

        }

    }

    /**
     * @param string $prefix
     * @param array $values
     */
    public function deleteByValues(string $prefix, array $values)
    {

        $this->redis->pipeline(function ($pipe) use ($prefix, $values) {

            foreach ($values as $advertisement => $group) {

                $pipe->del("$prefix:a:$advertisement");

                if(!is_null($group)) {

                    $this->redis->del("$prefix:q:$group");

                    $pipe->srem("$prefix:g:$group", $advertisement);

                }

            }

        });

    }

    /**
     * @param string $prefix
     */
    public function deleteAll(string $prefix) {

        $this->redis->pipeline(function($pipe) use ($prefix) {

            foreach(new Keyspace($this->redis, "$prefix:*") as $key) {

                $pipe->del($key);

            }

        });

    }

    /**
     * @param string $from
     * @param string $to
     */
    public function rename(string $from, string $to)
    {

        $this->redis->pipeline(function ($pipe) use ($from, $to) {

            foreach (new Keyspace($this->redis, "$from:*") as $key) {

                $pipe->rename(
                    $key,
                    preg_replace(
                        "@^($from)(:.+)$@",
                        "{$to}$2",
                        $key
                    )
                );

            }

        });

    }

    /**
     * @param string $prefix
     * @param int $advertisement
     * @param string $json
     */
    public function changeJson(string $prefix, int $advertisement, string $json) {

        $this->redis->set("$prefix:a:$advertisement", $json);

    }

}
