<?php

namespace App\Classes\Redis;

use Predis\Client;

/**
 * Class AbstractStorage
 * @package App\Classes\Redis
 */
abstract class AbstractStorage {

    /**
     * @var Client
     */
    protected $redis;

    /**
     * @var
     */
    protected $connection;

    /**
     * AbstractStorage constructor.
     */
    function __construct() {

        $this->redis = new Client(
            config("database.redis.{$this->connection}", [
                'host' => env('REDIS_HOST', '127.0.0.1'),
                'password' => env('REDIS_PASSWORD', null),
                'port' => env('REDIS_PORT', 6379),
                'database' => $this->connection == "config" ? 1 : 2
            ])
        );

    }

    /**
     *
     * Get active redis client.
     *
     * @return Client
     */
    public function getClient() {

        return $this->redis;

    }

}
