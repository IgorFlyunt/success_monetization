<?php
/**
 * Global project helpers. Need to be registered in composer autoload.
 */

use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

if(!function_exists("transaction_wrapper")) {

    /**
     * Allows to execute callbacks in MySql transactions.
     *
     * @param $callable
     * @param array $parameters
     * @param bool $throw
     * @return bool|mixed
     * @throws Exception
     */
    function transaction_wrapper($callable, array $parameters = [], bool $throw = true) {

        DB::beginTransaction();

        try {

            if(is_array($callable)) {

                $result = call_user_func_array([
                    $callable["class"],
                    $callable["method"]
                ], $parameters);

            } elseif(is_callable($callable)) {

                $result = $callable($parameters);

            }

            DB::commit();

            return $result;

        } catch(Exception $e) {

            DB::rollback();

            if($throw) {

                throw $e;

            }

        }

        return false;

    }

}

if(!function_exists("flash_messages")) {

    /**
     * Add string or array of strings to flash messages array in session.
     *
     * @param $messages
     * @param string $type
     */
    function flash_messages($messages, string $type = 'success') {

        $all = session("flash_messages", []);

        if(is_string($messages)) {

            if(!key_exists($type, $all)) {

                $all[$type] = [];

            }

            $all[$type][] = $messages;

        } elseif(is_array($messages)) {

            foreach($messages as $type => $group) {

                if(!key_exists($type, $all)) {

                    $all[$type] = [];

                }

                $all[$type] = array_merge($group, $all[$type]);

            }

        }

        session()->put("flash_messages", $all);

    }

}

if(!function_exists("upload_file")) {

    /**
     * Upload file to the server.
     *
     * @param UploadedFile $file
     * @param string $folder
     * @param string $subfolder
     * @return string
     */
    function upload_file(UploadedFile $file, string $folder, string $subfolder = '') {

        $url = "uploads/$folder" . (!empty($subfolder) ? "/$subfolder" : '');

        $path = public_path($url);

        if(!file_exists($path)) {

            File::makeDirectory($path, 0777, true);

        }

        $name = preg_replace("@\s+@", '_', $file->getClientOriginalName());

        $file->move($path, $name);

        return "$url/$name";

    }

}

if(!function_exists("delete_file")) {

    /**
     * Delete file from server if file exists.
     *
     * @param string $path
     */
    function delete_file(string $path) {

        $path = public_path($path);

        if(file_exists($path)) {

            @unlink($path);

        }

    }

}

if(!function_exists("is_active_url")) {

    /**
     * Check if passed url is equal with current server path.
     *
     * @param string $url
     * @param string|null $matcher
     * @return bool|false|int
     */
    function is_active_url(string $url, string $matcher = null) {

        $current = url()->current();

        if($current == $url) {

            return true;

        }

        if(!is_null($matcher)) {

            $path = str_replace(request()->root(), '', $current);

            return preg_match(
                "@/$matcher/\d+.*@",
                str_replace(
                    request()->root(),
                    '',
                    $current
                )
            );

        }

        return false;

    }

}

if(!function_exists("get_main_db_name")) {

    /**
     * Return main database name with prefix.
     *
     * @param string|null $table
     * @return string
     */
    function get_main_db_name(string  $table = null) {

        $table = !is_null($table) ? ".$table" : '';

        return env("DB_PREFIX", '') . env("DB_DATABASE") . $table;

    }

}

if(!function_exists("get_tenant_db_name")) {

    /**
     * Return tenant database name with prefix.
     *
     * @param string $table
     * @return string
     */
    function get_tenant_db_name(string $table) {

        $user_id = app("tenant")->getUserId();

        return env("DB_PREFIX", '') . "user_{$user_id}_data.$table";

    }

}
