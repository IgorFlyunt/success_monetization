<?php

namespace App\Classes\Helpers;


/**
 * Class Meta(allow's to add html meta tags). Need to be registered in a service container.
 * @package App\Classes\Helpers
 */
class Meta
{

    /**
     * @var string
     */
    private $titleDelimiter = " | ";

    /**
     * @var array
     */
    private $tags = [
        "title" => "<title>%s</title>\n",
        "description" => "<meta content=\"%s\" name=\"description\" />\n",
        "keywords" => "<meta content=\"%s\" name=\"keywords\" />\n",
        "base" => "<base href=\"%s\"/>\n",
        "canonical" => "<link rel=\"canonical\" href=\"%s\"/>\n",
        "social" => "<meta property=\"%s\" content=\"%s\" />\n"
    ];

    /**
     * @var array
     */
    private $data = [
        "title" => [],
        "site_name" => "",
        "description" => "",
        "keywords" => "",
        "base" => "",
        "canonical" => "",
        "url" => "",
        "locale" => "",
        "image" => "",
        "social" => [
            "og:title" => "",
            "og:site_name" => "",
            "og:description" => "",
            "og:url" => "",
            "og:image" => "",
            "og:type" => "website",
            "og:locale" => "",
            "vk:image" => "",
            "vk:title" => "",
            "vk:description" => "",
        ]
    ];

    /**
     * @param string $property
     * @return mixed
     */
    function __get(string $property)
    {

        if (key_exists($property, $this->data)) return $this->data[$property];

        if (key_exists($property, $this->data["social"])) return $this->data["social"][$property];

    }

    /**
     * @param string $delimiter
     */
    public function setTitleDelimiter(string $delimiter)
    {

        $this->titleDelimiter = $delimiter;

    }

    /**
     * @param string $property
     * @param $arguments
     */
    public function set(string $property, $arguments)
    {

        if (key_exists($property, $this->data)) {

            if (is_array($this->data[$property])) {

                $this->data[$property][] = $arguments;

            } else {

                $this->data[$property] = $arguments;

            }

        }

        if (key_exists($property, $this->data["social"])) $this->data["social"][$property] = $arguments;

    }

    /**
     * Return html
     * @return string
     */
    public function render()
    {

        // $this->data['title'][] = config('app.name');

        $output = '';

        foreach($this->data as $property => $value) {

            if($property === "social") continue;

            if($property === "title") {

                $output .= sprintf($this->tags[$property], implode($this->titleDelimiter, $value));

                continue;

            }

            if (!empty($this->data[$property])) {

                $output .= sprintf($this->tags[$property], $value);

            }

        }

        foreach ($this->data['social'] as $property => $value) {

            $value = !empty($value) ? $value : $this->_getSocialMeta($property);

            if (!empty($value)) $output .= sprintf($this->tags['social'], $property, $value);

        }

        return $output;

    }

    /**
     * @param string $property
     * @return mixed|null|string
     */
    private function _getSocialMeta(string $property)
    {

        $property = explode(":", $property);

        $property = end($property);

        $content = key_exists($property, $this->data) ? $this->data[$property] : null;

        if (is_array($content)) $content = implode($this->titleDelimiter, $content);

        return $content;

    }

    /**
     * @return mixed
     */
    public function getTitle() {

        if(sizeof($this->title)) {

            return $this->title[0];

        }

    }


}
