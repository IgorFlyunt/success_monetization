<?php

namespace App\Classes\MultiTenant;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;

/**
 * Class Loader
 * @package App\Classes\MultiTenant
 */
class Loader {

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @return mixed
     */
    public function getUserId() {

        return $this->user_id;

    }

    /**
     * @param $user
     */
    public function initializeDatabase($user) {

        $this->setUser($user);

        $this->setDatabase();

        $this->createDatabase();

        $this->checkMigration();

    }

    /**
     * @param $user
     * @return $this
     */
    private function setUser($user) {

        if(is_a($user, User::class)) {

            $this->user_id = $user->id;

        } else $this->user_id = $user;

        return $this;

    }

    /**
     *
     * Get database name
     *
     * @return string
     */
    private function getDbName() {

        return env("DB_PREFIX", '') . "user_{$this->user_id}_data";

    }

    /**
     *
     * Set database name
     *
     */
    private function setDatabase() {

        $connection = config("database.connections.mysql", []);

        $connection["database"] = $this->getDbName();

        config([
            "database.connections.site_data" => $connection
        ]);

    }

    /**
     *
     * Create database.
     *
     */
    private function createDatabase() {

        DB::statement("CREATE DATABASE IF NOT EXISTS `{$this->getDbName()}`");

    }

    /**
     *
     * Check if all migrations were launched.
     *
     */
    private function checkMigration() {

        $table_exists = Schema::connection("site_data")->hasTable(
            "comments"
        );

        if(!$table_exists) {

            Artisan::call("migrate", [
                "--force" => true,
                "--database" => "site_data",
                "--path" => "/database/migrations/site"
            ]);

            config(["database.default" => "mysql"]);

        }

    }

}
