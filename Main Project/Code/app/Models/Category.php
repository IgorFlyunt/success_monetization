<?php

namespace App\Models;

use App\Traits\Model\UserTrait;

/**
 * Class Category
 * @package App\Models
 */
class Category extends Model
{

    use UserTrait;

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "user_id"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function advertisements()
    {
        return $this->belongsToMany(Advertisement::class, 'advertisements_categories');
    }

}
