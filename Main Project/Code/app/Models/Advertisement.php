<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Model\CommentsTrait;
use App\Traits\Model\StatusTrait;
use App\Traits\Model\FileTrait;
use App\Traits\Model\UserTrait;

/**
 * Class Advertisement
 * @package App\Models
 */
class Advertisement extends Model
{

    use CommentsTrait;
    use StatusTrait;
    use SoftDeletes;
    use FileTrait;
    use UserTrait;

    /**
     * @var array
     */
    public $dates = ["deleted_at"];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'type',
        'css',
        'html',
        'file_id',
        'status',
        'parent_id',
        'is_template'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent() {

        return $this->belongsTo(Advertisement::class);

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {

        return $this->hasMany(Advertisement::class, "parent_id");

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories() {

        return $this->belongsToMany(Category::class, "advertisements_categories");

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'advertisements_groups');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sites() {

        $table = get_tenant_db_name("advertisements");

        return $this->belongsToMany(
            Site::class,
            $table,
            "advertisement_id",
            "site_id"
        );

    }

    /**
     *
     * Check if record is parent.
     *
     * @return bool
     */
    public function isOriginal()
    {
        return !(bool)$this->parent_id;
    }

    /**
     *
     * Check if record is template.
     *
     * @return bool
     */
    public function isTemplate() {

        return (bool)$this->is_template;

    }

    /**
     *
     * Return html for redis
     *
     * @return null|string|string[]
     */
    public function buildHtml() {

        $picture = '#';

        if($this->hasFile()) {

            $picture = $this->getFileUrl();

        }

        return preg_replace(
            '/{{\s*picture\s*}}/',
            $picture,
            $this->html
        );

    }

}
