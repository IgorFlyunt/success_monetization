<?php

namespace App\Models;

use App\Traits\Model\DecorateDateTrait;

/**
 * Class Comment
 * @package App\Models
 */
class Comment extends Model
{

    use DecorateDateTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'message',
        'modelable_id',
        'modelable_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function modelable() {

        return $this->morphTo();

    }

    /**
     *
     * Query parameter to order result by creation date.
     *
     * @param $query
     * @return mixed
     */
    public function scopeDateOrdered($query) {

        return $query->orderBy("id", "DESC");

    }

}
