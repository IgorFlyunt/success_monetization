<?php

namespace App\Models;

/**
 * Class File
 * @package App\Models
 */
class File extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'path'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function advertisements() {

        return $this->hasMany(Advertisement::class);

    }

}
