<?php

namespace App\Models;

use App\Traits\Model\UserTrait;

/**
 * Class Group
 * @package App\Models
 */
class Group extends Model
{

    use UserTrait;

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "type",
        "user_id"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function advertisements()
    {

        return $this->belongsToMany(Advertisement::class, 'advertisements_groups');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sites() {

        $table = get_tenant_db_name(
            "advertisements"
        );

        return $this->belongsToMany(
            Site::class,
            $table,
            "group_id",
            "site_id"
        );

    }

}
