<?php

namespace App\Models;

/**
 * Class Synchronization
 * @package App\Models
 */
class Synchronization extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = ["synchronized_at"];

    /**
     * @var array
     */
    protected $fillable = [
        "key",
        "error",
        "status",
        "site_id",
        "periodicity",
        "synchronized_at"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site() {

        return $this->belongsTo(Site::class);

    }

}
