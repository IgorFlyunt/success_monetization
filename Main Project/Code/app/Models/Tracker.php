<?php

namespace App\Models;

use App\Traits\Model\UserTrait;

/**
 * Class Tracker
 * @package App\Models
 */
class Tracker extends Model
{

    use UserTrait;

    /**
     * @var array
     */
    protected $fillable = [
        "url",
        "user_id"
    ];

}
