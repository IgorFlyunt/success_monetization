<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Class Model
 * @package App\Models
 */
abstract class Model extends BaseModel {

    /**
     * @var string
     */
    protected $connection = "mysql";

    /**
     *
     * Return model instance for relations with database prefix.
     *
     * @return Model
     */
    public static function getForMainDatabaseRelation() {

        $object = new static;

        return $object->setTable(
            get_main_db_name(
                $object->getTable()
            )
        );

    }

}
