<?php

namespace App\Models\Site;

/**
 * Class Category
 * @package App\Models\Site
 */
class Category extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        "site_id",
        "original_id",
        "title",
        "url"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts() {

        return $this->belongsToMany(
            Post::class,
            "posts_categories",
            null,
            null,
            "original_id",
            "id"
        );

    }

}
