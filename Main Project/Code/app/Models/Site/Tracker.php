<?php

namespace App\Models\Site;

use App\Models\Tracker as BaseTracker;

/**
 * Class Tracker
 * @package App\Models\Site
 */
class Tracker extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        "advertisement_id",
        "utm_campaign",
        "tracker_id",
        "utm_medium",
        "utm_source",
        "sid1",
        "sid2",
        "sid3"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tracker() {

        return $this->belongsTo(BaseTracker::class);

    }

    /**
     *
     * Return url string for redis record.
     *
     * @return string
     */
    public function buildUrl() {

        $tracker = $this->tracker;

        return trim(
            $tracker->url, '/'
        ) . '?' . http_build_query([
            "utm_campaign" => $this->utm_campaign,
            "utm_medium" => $this->utm_medium,
            "utm_source" => $this->utm_source,
            "utm_content" => "{replace_me}", //TODO - key for replacement at js plugin
            "sid1" => $this->sid1,
            "sid2" => $this->sid2,
            "sid3" => $this->sid3
        ]);

    }

}
