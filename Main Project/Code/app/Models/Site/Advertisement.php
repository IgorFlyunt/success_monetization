<?php

namespace App\Models\Site;

use App\Models\Group;
use App\Models\Site;
use App\Models\Target;
use App\Traits\Model\StatusTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Traits\Model\CommentsTrait;
use App\Models\Advertisement as BaseAdvertisement;

/**
 * Class Advertisement
 * @package App\Models\Site
 */
class Advertisement extends Model
{

    use CommentsTrait;
    use StatusTrait;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        "advertisement_id",
        "group_id",
        "site_id",
        "country",
        "status",
        "type",
        "url"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function urls() {

        return $this->hasMany(Url::class);

    }

    /**
     * @return mixed
     */
    public function advertisement() {

        return $this->belongsTo(
            BaseAdvertisement::class
        )->withTrashed();

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group() {

        return $this->belongsTo(Group::class);

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts() {

        return $this->belongsToMany(Post::class, "posts_advertisements")->withPivot("include");

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories() {

        return $this->belongsToMany(Category::class, "categories_advertisements")->withPivot("exact");

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function targets() {

        $table = get_tenant_db_name(
            "advertisements_targets"
        );

        return $this->belongsToMany(
            Target::getForMainDatabaseRelation(),
            $table
        );

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site() {

        return $this->belongsTo(
            Site::getForMainDatabaseRelation()
        );

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function position() {

        return $this->hasOne(Position::class);

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tracker() {

        return $this->hasOne(Tracker::class);

    }

    /**
     *
     * Query selector to get records which have active advertisements.
     *
     * @param $query
     * @return mixed
     */
    public function scopeWhereAdvertisementIsActive($query) {

        return $query->whereExists(function($query) {

            return $query->selectRaw('main_advertisements.id')->from(
                get_main_db_name("advertisements") . ' as main_advertisements'
            )->whereRaw(
                "{$this->getTable()}.advertisement_id = main_advertisements.id"
            )->whereRaw(
                "main_advertisements.status = 1"
            )->whereRaw(
                "ISNULL(main_advertisements.deleted_at)"
            );

        })->where(function($query) {

            return $query->has(
                "tracker"
            )->orWhereNotNull(
                "url"
            );

        });

    }

    /**
     *
     * Query selector to get parent_id(check if record is duplicate).
     *
     * @param $query
     * @return mixed
     */
    public function scopeGetParentId($query) {

        $subquery = DB::table(
            get_tenant_db_name(
                "advertisements"
            ) . " as parent"
        )->selectRaw(
            "parent.id as parent_id"
        )->whereRaw(
            "parent.advertisement_id = advertisements.id"
        )->whereNull(
            "parent.group_id"
        )->whereNull(
            "advertisements.group_id"
        )->whereRaw(
            "parent.site_id = advertisements.site_id"
        )->whereRaw(
            "parent.id < advertisements.id"
        )->orderBy(
            "parent.id",
            "ASC"
        )->limit(
            1
        )->toSql();

        return $query->selectRaw(
            "advertisements.*, ($subquery) as parent_id"
        );

    }

    /**
     *
     * Query selector to get records for remove from redis by condition.
     *
     * @param $query
     * @param string|null $field
     * @param array $ids
     * @return mixed
     */
    public function scopeForRemoveFromRedis($query, string $field = null, $ids = []) {

        $ids = is_array($ids) ? $ids : [$ids];

        return $query->where(function($query) use ($field, $ids) {

            if(!is_null($field)) {

                $method = "orWhere";

                $query->whereIn(
                    $field,
                    $ids
                );

            } else $method = "where";

            return $query->$method(function($query) {

                return $query->whereNull(
                    "group_id"
                )->whereNull(
                    "advertisement_id"
                );

            })->orWhere(function($query) {

                return $query->whereNull(
                    "url"
                )->whereDoesntHave(
                    "tracker"
                );

            });

        });

    }

    /**
     *
     * Query selector to check if related tracker exists.
     *
     * @param $query
     * @return mixed
     */
    public function scopeHasTracker($query) {

        $subquery = DB::table(
            get_tenant_db_name(
                "trackers"
            )
        )->selectRaw(
            "trackers.id"
        )->whereRaw(
            "trackers.advertisement_id = advertisements.id"
        )->limit(
            1
        )->toSql();

        return $query->selectRaw(
            "advertisements.*, ($subquery) as has_tracker"
        );

    }

    /**
     *
     * Return identifier for redis.
     *
     * @param bool $advertisement
     * @return string
     */
    public function getRedisKey(bool $advertisement = false) {

        if(!$advertisement && $this->group_id) {

            return "g:{$this->group_id}";

        }

        return "a:{$this->id}";

    }

    /**
     *
     * Check if related App\Models\Advertisement is active
     *
     * @return bool
     */
    public function checkAdvertisementStatus() {

        if(($advertisement = $this->advertisement)) {

            return $advertisement->status;

        }

        return false;

    }

}
