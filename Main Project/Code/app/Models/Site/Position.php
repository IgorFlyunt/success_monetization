<?php

namespace App\Models\Site;

/**
 * Class Position
 * @package App\Models\Site
 */
class Position extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        "main",
        "sub",
        "advertisement_id"
    ];

    /**
     *
     * Return position number from configs.
     *
     * @param string $type
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getPosition(string $type) {

        return config("advertisement.positions.$type.{$this->main}");

    }

}
