<?php

namespace App\Models\Site;

/**
 * Class Url
 * @package App\Models\Site
 */
class Url extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        "advertisement_id",
        "url",
        "exact",
        "exclude"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertisement() {

        return $this->belongsTo(Advertisement::class);

    }

}
