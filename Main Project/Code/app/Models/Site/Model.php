<?php

namespace App\Models\Site;

use App\Models\Model as BaseModel;

/**
 * Class Model
 * @package App\Models\Site
 */
class Model extends BaseModel {

    /**
     * @var string
     */
    protected $connection = "site_data";

    /**
     *
     * Return model instance for relations with database prefix.
     *
     * @param string $class
     * @return mixed|string
     */
    public function newRelatedInstance($class)
    {

        if(!is_object($class)) {

            return parent::newRelatedInstance($class);

        }

        return $class;

    }

}
