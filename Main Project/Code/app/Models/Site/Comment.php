<?php

namespace App\Models\Site;

use App\Models\Comment as BaseComment;

/**
 * Class Comment
 * @package App\Models\Site
 */
class Comment extends BaseComment {

    /**
     * @var string
     */
    protected $connection = "site_data";

}
