<?php

namespace App\Models;

use App\Traits\Model\Site\SynchronizationTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Traits\Model\StatusTrait;
use App\Traits\Model\UserTrait;

/**
 * Class Site
 * @package App\Models
 */
class Site extends Model
{

    use UserTrait;
    use SoftDeletes;
    use StatusTrait;
    use SynchronizationTrait;

    /**
     * @var array
     */
    public $dates = ["deleted_at"];

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'url'
    ];

    /**
     *
     * Query selector to get "status" field from advertisements table.
     * If one or more App/Model/Sites/Advertisement is active "true" will be returned.
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithStatus($query) {

        $table = get_tenant_db_name(
            "advertisements"
        );

        $subquery = DB::table(
            $table
        )->selectRaw(
            "COUNT($table.id)"
        )->whereRaw(
            "$table.site_id = sites.id"
        )->whereRaw(
            "$table.status = 1"
        )->limit(1)->toSql();

        return $query->selectRaw("{$this->getTable()}.*, ($subquery) as status");

    }

}
