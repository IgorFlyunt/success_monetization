<?php

namespace App\Models;

/**
 * Class Target
 * @package App\Models
 */
class Target extends Model
{

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        "key",
        "value"
    ];

}
