<?php

namespace App\Listeners\Advertisement;

use App\Models\Advertisement;
use App\Events\Advertisement\SavedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\Redis\AdvertisementsStorage;
use App\Repositories\Site\AdvertisementsRepository;
use App\Repositories\AdvertisementsRepository as BaseAdvertisementsRepository;

/**
 * Class SavedListener
 *
 * Handler for App\Events\Advertisement\SavedEvent.
 *
 * Change redis data for all App\Models\Site\Advertisement which are related to passed advertisement.
 *
 * @package App\Listeners\Advertisement
 */
class SavedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var AdvertisementsStorage
     */
    protected $advertisementsStorage;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * @var BaseAdvertisementsRepository
     */
    protected $baseAdvertisementsRepository;

    /**
     * SavedListener constructor.
     * @param AdvertisementsStorage $advertisementsStorage
     * @param AdvertisementsRepository $advertisementsRepository
     * @param BaseAdvertisementsRepository $baseAdvertisementsRepository
     */
    public function __construct(
        advertisementsStorage $advertisementsStorage,
        AdvertisementsRepository $advertisementsRepository,
        BaseAdvertisementsRepository $baseAdvertisementsRepository
    )
    {

        $this->advertisementsStorage = $advertisementsStorage;

        $this->advertisementsRepository = $advertisementsRepository;

        $this->baseAdvertisementsRepository = $baseAdvertisementsRepository;

    }

    /**
     * @param SavedEvent $event
     */
    public function handle(SavedEvent $event)
    {

        $this->load(($model = $event->model));

        foreach ($model->sites as $site) {

            if (($prefix = $site->getSyncronization("key"))) {

                $objects = $this->advertisementsRepository->setSite(
                    $site
                )->getForSiteByAdvertisement(
                    $model->id
                );

                foreach ($objects as $object) {

                    $object->advertisement = $model;

                    $this->advertisementsStorage->store(
                        $prefix,
                        $object->id,
                        $this->advertisementsRepository->buildJson(
                            $object
                        ),
                        $object->group_id
                    );

                }

            }

        }

    }

    /**
     * @param Advertisement $advertisement
     */
    private function load(Advertisement $advertisement)
    {

        $this->baseAdvertisementsRepository->loadRelation(
            $advertisement, [
                "sites.synchronization"
            ]
        );

    }

}
