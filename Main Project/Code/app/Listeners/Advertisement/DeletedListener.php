<?php

namespace App\Listeners\Advertisement;

use Illuminate\Queue\InteractsWithQueue;
use App\Events\Advertisement\DeletedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\Site\AdvertisementsRepository;
use App\Repositories\AdvertisementsRepository as BaseAdvertisementsRepository;

/**
 * Class DeletedListener
 *
 * Handler for App\Events\Advertisement\DeletedEvent.
 *
 * Disable and from redis all App\Models\Site\Advertisement which are related to deleted advertisement.
 *
 * @package App\Listeners\Advertisement
 */
class DeletedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * @var BaseAdvertisementsRepository
     */
    protected $baseAdvertisementsRepository;

    /**
     * DeletedListener constructor.
     * @param AdvertisementsRepository $advertisementsRepository
     * @param BaseAdvertisementsRepository $baseAdvertisementsRepository
     */
    public function __construct(
        AdvertisementsRepository $advertisementsRepository,
        BaseAdvertisementsRepository $baseAdvertisementsRepository
    )
    {

        $this->advertisementsRepository = $advertisementsRepository;

        $this->baseAdvertisementsRepository = $baseAdvertisementsRepository;

    }

    /**
     * @param DeletedEvent $event
     */
    public function handle(DeletedEvent $event)
    {

        $this->baseAdvertisementsRepository->setUser(
            $event->user
        )->getForUserForDelete(
            $event->ids
        )->each(function($advertisement) {

            foreach($advertisement->sites as $site) {

                $this->advertisementsRepository->stopActionForSite(
                    $this->advertisementsRepository->setSite(
                        $site
                    )->pluckForSite(
                        "id",
                        null,
                        function($query) use ($advertisement) {

                            return $query->forRemoveFromRedis(
                                "advertisement_id",
                                $advertisement->id
                            );

                        }
                    )->toArray()
                );

            }

        });

    }

}
