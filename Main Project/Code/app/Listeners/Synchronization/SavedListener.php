<?php

namespace App\Listeners\Synchronization;

use App\Classes\Redis\AdvertisementsStorage;
use App\Classes\Redis\ConfigStorage;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\Synchronization\SavedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SavedListener
 *
 * Handler for App\Events\Synchronization\SavedEvent.
 *
 * Change site prefix in redis after synchronization is saved.
 *
 * @package App\Listeners\Synchronization
 */
class SavedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * @var AdvertisementsStorage
     */
    protected $advertisementsStorage;

    /**
     * SavedListener constructor.
     * @param ConfigStorage $configStorage
     * @param AdvertisementsStorage $advertisementsStorage
     */
    public function __construct(
        ConfigStorage $configStorage,
        AdvertisementsStorage $advertisementsStorage
    )
    {

        $this->configStorage = $configStorage;

        $this->advertisementsStorage = $advertisementsStorage;

    }

    /**
     * @param SavedEvent $event
     */
    public function handle(SavedEvent $event)
    {

        $this->configStorage->rename(
            $event->from,
            $event->to
        );

        $this->configStorage->setSite(
            $event->to,
            $event->site->url
        );

        $this->advertisementsStorage->rename(
            $event->from,
            $event->to
        );

    }

}
