<?php

namespace App\Listeners\Tracker;

use App\Events\Tracker\DeletedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\Site\AdvertisementsRepository;

/**
 * Class DeletedListener
 *
 * Handler For App\Events\Tracker\DeletedEvent.
 *
 * Disable and remove from redis data for all App\Models\Site\Advertisement which are related to passed tracker.
 *
 * @package App\Listeners\Tracker
 */
class DeletedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * DeletedListener constructor.
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(
        AdvertisementsRepository $advertisementsRepository
    )
    {

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param DeletedEvent $event
     */
    public function handle(DeletedEvent $event)
    {

        $chunks = $this->advertisementsRepository->setUser(
            $event->user
        )->getForStopForUser()->groupBy(
            "site_id"
        );

        foreach($chunks as $advertisements) {

            $this->advertisementsRepository->setSite(
                $advertisements->first()->site
            )->stopActionForSite(
                $advertisements->pluck(
                    "id"
                )->toArray()
            );

        }

    }

}
