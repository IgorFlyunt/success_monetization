<?php

namespace App\Listeners\Group;

use App\Events\Group\DeletedEvent;
use App\Repositories\Site\AdvertisementsRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class DeletedListener
 *
 * Handler for App\Events\Group\DeletedEvent.
 *
 * Disable and remove from redis all App\Models\Site\Advertisement which are related to group.
 *
 * @package App\Listeners\Group
 */
class DeletedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * DeletedListener constructor.
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(AdvertisementsRepository $advertisementsRepository)
    {

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param DeletedEvent $event
     */
    public function handle(DeletedEvent $event)
    {

        $chunks = $event->advertisements->groupBy(
            "site_id"
        );

        foreach ($chunks as $advertisements) {

            $this->advertisementsRepository->setSite(
                $advertisements->first()->site
            )->stopActionForSite(
                $advertisements->pluck(
                    "id"
                )->toArray()
            );

        }

    }

}
