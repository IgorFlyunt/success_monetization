<?php

namespace App\Listeners\User;

use App\Events\User\DeletedEvent;
use Illuminate\Support\Facades\DB;

/**
 * Class DeletedListener
 *
 * Handler for App\Events\User\DeletedEvent.
 *
 * Drop tenant database for user when is deleted.
 *
 * @package App\Listeners\User
 */
class DeletedListener
{

    /**
     * @param DeletedEvent $event
     */
    public function handle(DeletedEvent $event)
    { //TODO - видалення всіх даних з редіс для користувача

        foreach($event->ids as $user_id) {

            DB::statement("DROP DATABASE IF EXISTS `user_{$user_id}_data`");

        }

    }

}
