<?php

namespace App\Listeners\Site;

use App\Events\Site\DeletedEvent;
use App\Classes\Redis\ConfigStorage;
use App\Repositories\SitesRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\Redis\AdvertisementsStorage;

/**
 * Class DeletedListener
 *
 * Handler for App\Events\Site\DeletedEvent.
 *
 * Delete all in redis data for site.
 *
 * @package App\Listeners\Site
 */
class DeletedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * @var SitesRepository
     */
    protected $sitesRepository;

    /**
     * @var AdvertisementsStorage
     */
    protected $advertisementsStorage;

    /**
     * DeletedListener constructor.
     * @param ConfigStorage $configStorage
     * @param SitesRepository $sitesRepository
     * @param AdvertisementsStorage $advertisementsStorage
     */
    public function __construct(
        ConfigStorage $configStorage,
        SitesRepository $sitesRepository,
        AdvertisementsStorage $advertisementsStorage
    )
    {

        $this->configStorage = $configStorage;

        $this->sitesRepository = $sitesRepository;

        $this->advertisementsStorage = $advertisementsStorage;

    }

    /**
     * @param DeletedEvent $event
     */
    public function handle(DeletedEvent $event)
    {

        $this->sitesRepository->setUser(
            $event->user
        )->getForUserForDelete(
            $event->ids
        )->each(function($site) {

            if(($prefix = $site->getSyncronization("key"))) {

                $this->configStorage->deleteAll(
                    $prefix
                );

                $this->advertisementsStorage->deleteAll(
                    $prefix
                );

            }

        });

    }

}
