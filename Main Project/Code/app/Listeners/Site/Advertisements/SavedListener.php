<?php

namespace App\Listeners\Site\Advertisements;

use App\Models\Site;
use App\Models\Site\Advertisement;
use App\Events\Site\Advertisements\SavedEvent;

/**
 * Class SavedListener
 *
 * Handler for App\Events\Site\Advertisements\SavedEvent.
 *
 * Change data in redis for passed App\Models\Site\Advertisement.
 *
 * @package App\Listeners\Site\Advertisements
 */
class SavedListener extends AbstractListener
{

    /**
     * @param SavedEvent $event
     */
    public function handle(SavedEvent $event)
    {

        if($event->model->checkAdvertisementStatus()) {

            $this->load(
                ($site = $event->site)
            );

            if(($prefix = $site->getSyncronization("key"))) {

                $this->loadRelations(
                    ($model = $event->model),
                    $site
                );

                $this->configStorage->store(
                    $prefix,
                    $model->getRedisKey(),
                    $this->decorateData(
                        $model
                    )
                );

                $this->advertisementsStorage->store(
                    $prefix,
                    $model->id,
                    $this->advertisementsRepository->buildJson(
                        $model
                    ),
                    $model->group_id
                );

            }

        }

    }

    /**
     *
     * Load relations for App\Models\Site\Advertisement.
     *
     * @param Advertisement $model
     * @param Site $site
     */
    private function loadRelations(Advertisement $model, Site $site) {

        $this->advertisementsRepository->setSite(
            $site
        )->loadRelation($model, [
            "categories",
            "posts",
            "urls"
        ]);

    }

}
