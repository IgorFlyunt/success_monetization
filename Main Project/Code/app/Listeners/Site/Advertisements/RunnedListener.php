<?php

namespace App\Listeners\Site\Advertisements;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Site\Advertisements\RunnedEvent;
use App\Repositories\Site\AdvertisementsRepository;

/**
 * Class RunnedListener
 *
 * Handler for App\Events\Site\Advertisements\RunnedEvent.
 *
 * Enable and add to redis data for all passed App\Models\Site\Advertisement.
 *
 * @package App\Listeners\Site\Advertisements
 */
class RunnedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * RunnedListener constructor.
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(AdvertisementsRepository $advertisementsRepository)
    {

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param RunnedEvent $event
     */
    public function handle(RunnedEvent $event)
    {

        $advertisements = $this->advertisementsRepository->setSite(
            $event->site
        )->getForSiteForRunned(
            $event->ids
        );

        foreach($advertisements as $advertisement) {

            $this->advertisementsRepository->dispatchEvent(
                "saved",
                $advertisement,
                $event->site
            );

        }

    }

}
