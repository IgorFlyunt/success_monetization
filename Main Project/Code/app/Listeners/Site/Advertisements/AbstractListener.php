<?php

namespace App\Listeners\Site\Advertisements;

use App\Models\Site;
use App\Models\Site\Advertisement;
use App\Classes\Redis\ConfigStorage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\Redis\AdvertisementsStorage;
use App\Repositories\Site\AdvertisementsRepository;
use App\Decorators\Site\Advertisements\Redis\UrlDecorator;

/**
 * Class AbstractListener
 *
 * Abstract decorator which implements data decoration and tenant database loading.
 *
 * @package App\Listeners\Site\Advertisements
 */
abstract class AbstractListener implements ShouldQueue {

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * @var AdvertisementsStorage
     */
    protected $advertisementsStorage;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * AbstractListener constructor.
     * @param ConfigStorage $configStorage
     * @param AdvertisementsStorage $advertisementsStorage
     * @param AdvertisementsRepository $advertisementsRepository
     */
    function __construct(
        ConfigStorage $configStorage,
        AdvertisementsStorage $advertisementsStorage,
        AdvertisementsRepository $advertisementsRepository
    ) {

        $this->configStorage = $configStorage;

        $this->advertisementsStorage = $advertisementsStorage;

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     *
     * Load tenant database
     *
     * @param Site $site
     */
    protected function load(Site $site) {

        app("tenant")->initializeDatabase(
            $site->user_id
        );

    }

    /**
     *
     * Decorate data before handling.
     *
     * @param Advertisement $model
     * @return mixed
     */
    protected function decorateData(Advertisement $model) {

        return $this->advertisementsRepository->decorate([
            "categories" => $model->categories,
            "posts" => $model->posts,
            "urls" => $model->urls
        ], UrlDecorator::class);

    }

}
