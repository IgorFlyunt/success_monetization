<?php

namespace App\Listeners\Site\Advertisements;

use App\Classes\Redis\ConfigStorage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Classes\Redis\AdvertisementsStorage;
use App\Repositories\Site\AdvertisementsRepository;
use App\Events\Site\Advertisements\RemovedFromGroupEvent;

/**
 * Class RemovedFromGroupListener
 *
 * Handler for App\Events\Site\Advertisements\RemovedFromGroupEvent.
 *
 * Change data in redis for passed App\Models\Site\Advertisement.
 *
 * @package App\Listeners\Site\Advertisements
 */
class RemovedFromGroupListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * @var AdvertisementsStorage
     */
    protected $advertisementsStorage;

    /**
     * RemovedFromGroupListener constructor.
     * @param ConfigStorage $configStorage
     * @param AdvertisementsStorage $advertisementsStorage
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(
        ConfigStorage $configStorage,
        AdvertisementsStorage $advertisementsStorage,
        AdvertisementsRepository $advertisementsRepository
    )
    {

        $this->configStorage = $configStorage;

        $this->advertisementsStorage = $advertisementsStorage;

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param RemovedFromGroupEvent $event
     */
    public function handle(RemovedFromGroupEvent $event)
    {

        if(($prefix = $event->site->getSyncronization("key"))) {

            $exists = $this->advertisementsRepository->setSite(
                $event->site
            )->checkInGroupExists(
                $event->group_id,
                $event->model->id
            );

            $this->advertisementsStorage->deleteByValues(
                $prefix, [
                    $event->model->id => $event->group_id
                ]
            );

            if(!$exists) {

                $event->model->group_id = $event->group_id;

                $this->configStorage->deleteByValues(
                    $prefix, [
                        $event->model->getRedisKey()
                    ]
                );

            }

        }

    }

}
