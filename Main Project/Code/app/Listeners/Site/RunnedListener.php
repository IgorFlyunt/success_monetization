<?php

namespace App\Listeners\Site;

use App\Classes\Redis\ConfigStorage;
use App\Events\Site\RunnedEvent;
use App\Repositories\Site\AdvertisementsRepository;
use App\Repositories\SitesRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class RunnedListener
 *
 * Handler for App\Events\Site\RunnedEvent.
 *
 * Enable and add to redis data for all site App\Models\Site\Advertisement.
 *
 * @package App\Listeners\Site
 */
class RunnedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * @var SitesRepository
     */
    protected $sitesRepository;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * RunnedListener constructor.
     * @param ConfigStorage $configStorage
     * @param SitesRepository $sitesRepository
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(
        ConfigStorage $configStorage,
        SitesRepository $sitesRepository,
        AdvertisementsRepository $advertisementsRepository
    )
    {

        $this->configStorage = $configStorage;

        $this->sitesRepository = $sitesRepository;

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param RunnedEvent $event
     */
    public function handle(RunnedEvent $event)
    {

        $this->sitesRepository->setUser(
            $event->user
        )->getForUserForRunned(
            $event->ids
        )->each(function($site) {

            if(($prefix = $site->getSyncronization("key"))) {

                $this->configStorage->setSite(
                    $prefix,
                    $site->url
                );

                $advertisements = $this->advertisementsRepository->setSite(
                    $site
                )->getForSiteForRunned();

                foreach($advertisements as $advertisement) {

                    $this->advertisementsRepository->dispatchEvent(
                        "saved",
                        $advertisement,
                        $site
                    );

                }

            }

        });

    }

}
