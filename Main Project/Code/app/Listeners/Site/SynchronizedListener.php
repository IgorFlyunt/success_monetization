<?php

namespace App\Listeners\Site;

use App\Models\Site;
use App\Classes\Redis\ConfigStorage;
use App\Events\Site\SynchronizedEvent;
use Illuminate\Queue\InteractsWithQueue;
use App\Repositories\Site\PostsRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\Site\CategoriesRepository;
use App\Repositories\Site\AdvertisementsRepository;
use App\Decorators\Site\Advertisements\Redis\SyncCategoriesDecorator;

/**
 * Class SynchronizedListener
 *
 * Handler for App\Events\Site\SynchronizedEvent.
 *
 * Change in redis data for site after synchronization(remove data for disappeared pages).
 *
 * @package App\Listeners\Site
 */
class SynchronizedListener implements ShouldQueue
{

    use InteractsWithQueue;

    /**
     * @var ConfigStorage
     */
    protected $configStorage;

    /**
     * @var PostsRepository
     */
    protected $postsRepository;

    /**
     * @var CategoriesRepository
     */
    protected $categoriesRepository;

    /**
     * @var AdvertisementsRepository
     */
    protected $advertisementsRepository;

    /**
     * SynchronizedListener constructor.
     * @param ConfigStorage $configStorage
     * @param PostsRepository $postsRepository
     * @param CategoriesRepository $categoriesRepository
     * @param AdvertisementsRepository $advertisementsRepository
     */
    public function __construct(
        ConfigStorage $configStorage,
        PostsRepository $postsRepository,
        CategoriesRepository $categoriesRepository,
        AdvertisementsRepository $advertisementsRepository
    )
    {

        $this->configStorage = $configStorage;

        $this->postsRepository = $postsRepository;

        $this->categoriesRepository = $categoriesRepository;

        $this->advertisementsRepository = $advertisementsRepository;

    }

    /**
     * @param SynchronizedEvent $event
     */
    public function handle(SynchronizedEvent $event)
    {

        if(($prefix = $event->model->getSyncronization("key"))) {

            $this->load($event->model);

            $this->configStorage->deleteDiff(
                $prefix,
                array_merge(
                    $this->postsRepository->setSite(
                        $event->model
                    )->pluckForSite(
                        "url"
                    )->toArray(),
                    $this->categoriesRepository->decorate(
                        $this->categoriesRepository->setSite(
                            $event->model
                        )->pluckForSite(
                            "url"
                        )->toArray(),
                        SyncCategoriesDecorator::class
                    )
                )
            );

            $ids = $this->advertisementsRepository->setSite(
                $event->model
            )->getEmptyAdvertisementsIdsForSite();

            $this->advertisementsRepository->stopActionForSite(
                $ids
            );

        }

    }

    /**
     *
     * Load tenant database.
     *
     * @param Site $site
     */
    private function load(Site $site) {

        app("tenant")->initializeDatabase(
            $site->user_id
        );

    }

}
