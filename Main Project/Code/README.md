### Installation

*meaning that you have already installed composer and set up environment for php projects (php 7.0/7.1, nginx, mysql)*

Clone git repository:

    git clone https://lichinet@bitbucket.org/lichinet/success-monetization.git [new_folder] or ./ - current folder
  
Install dependencies:

    composer install
    
Rename config file for environment:

    .env.example -> .env
*change default settings for application and mysql connection*
    
Generate application code

    php artisan key:generate

Apply database migrations:

*meaning that you have already mysql connection set up and created database `success-monetization`*

    php artisan migrate