<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements_groups', function (Blueprint $table) {

            $table->unsignedInteger("advertisement_id");

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->unsignedInteger("group_id");

            $table->foreign("group_id")->references("id")->on("groups")->onDelete("cascade");

            $table->index(["advertisement_id", "group_id"]);

            $table->unique(["advertisement_id", "group_id"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements_groups');
    }
}
