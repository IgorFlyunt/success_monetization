<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('user_id')->index();

            $table->foreign('user_id')->references('id')->on('users')->onDelete("cascade");

            $table->string('name');

            $table->unique(["user_id", "name"]);

            $table->enum('type', config("advertisement.types", [
                "context",
                "teaser",
                "banner",
                "popup",
                "other"
            ]));

            $table->string('css', 1000);

            $table->string('html', 1000);

            $table->boolean("status")->default(true);

            $table->unsignedInteger("parent_id")->nullable();

            $table->foreign("parent_id")->references("id")->on("advertisements")->onDelete("set null");

            $table->timestamps();

            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
