<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements_categories', function (Blueprint $table) {

            $table->unsignedInteger("advertisement_id");

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->unsignedInteger("category_id");

            $table->foreign("category_id")->references("id")->on("categories")->onDelete("cascade");

            $table->index(["advertisement_id", "category_id"]);

            $table->unique(["advertisement_id", "category_id"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements_categories');
    }
}
