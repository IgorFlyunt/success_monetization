<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSynchronizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('synchronizations', function (Blueprint $table) {

            $table->increments('id');

            $table->string("key", 20);

            $table->boolean("status")->default(false);

            $table->string("periodicity", 20)->default(
                config("synchronization.default_periodicity", 'handle')
            );

            $table->unsignedInteger("site_id")->index()->unique();

            $table->foreign("site_id")->references("id")->on("sites")->onDelete("cascade");

            $table->dateTime("synchronized_at")->nullable();

            $table->string("error")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('synchronizations');
    }
}
