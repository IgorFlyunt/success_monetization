<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackers', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger("advertisement_id")->index();

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->unsignedInteger("tracker_id");

            $table->foreign("tracker_id")->references("id")->on(
                get_main_db_name("trackers")
            )->onDelete("cascade");

            $table->string("utm_medium");

            $table->string("utm_campaign");

            $table->string("utm_source");

            $table->string("sid1");

            $table->string("sid2");

            $table->string("sid3");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackers');
    }
}
