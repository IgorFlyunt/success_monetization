<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger("site_id")->index();

            $table->foreign("site_id")->references("id")->on(
                get_main_db_name() . ".sites"
            )->onDelete("cascade");

            $table->unsignedInteger("advertisement_id")->index();

            $table->foreign("advertisement_id")->references("id")->on(
                get_main_db_name() . ".advertisements"
            )->onDelete("cascade");

            $table->unsignedInteger("group_id")->nullable();

            $table->foreign("group_id")->references("id")->on(
                get_main_db_name() . ".groups"
            )->onDelete("set null");

            $table->index(["site_id", "advertisement_id"]);

            $table->index(["site_id", "group_id"]);

            $table->enum('type', config("advertisement.types", [
                "context",
                "teaser",
                "banner",
                "popup",
                "other"
            ]));

            $table->unique(["site_id", "group_id", "advertisement_id", "type"]);

            $table->boolean("status")->default(true);

            $table->string("url")->nullable();

            $table->softDeletes();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
