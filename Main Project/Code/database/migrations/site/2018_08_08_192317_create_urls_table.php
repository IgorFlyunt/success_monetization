<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('urls', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger("advertisement_id")->index();

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->index(["advertisement_id", "url"]);

            $table->unique(["advertisement_id", "url", "exact"]);

            $table->string("url")->index();

            $table->boolean("exact");

            $table->boolean("exclude");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('urls');
    }
}
