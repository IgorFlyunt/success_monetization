<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger("site_id")->index();

            $table->foreign("site_id")->references("id")->on(
                get_main_db_name() . ".sites"
            )->onDelete("cascade");

            $table->unsignedInteger("original_id");

            $table->string("title");

            $table->string("url")->index();

            $table->unique(["original_id", "site_id"]);

            $table->index(["original_id", "site_id"]);

            $table->index(["url", "site_id"]);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
