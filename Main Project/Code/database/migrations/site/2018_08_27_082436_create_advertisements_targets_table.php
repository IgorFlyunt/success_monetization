<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements_targets', function (Blueprint $table) {

            $table->unsignedInteger("target_id");

            $table->foreign("target_id")->references("id")->on(
                get_main_db_name() . ".targets"
            )->onDelete("cascade");

            $table->unsignedInteger("advertisement_id");

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->index(["target_id", "advertisement_id"]);

            $table->unique(["target_id", "advertisement_id"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements_targets');
    }
}
