<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_advertisements', function (Blueprint $table) {

            $table->boolean("exact");

            $table->unsignedInteger("category_id");

            $table->foreign("category_id")->references("id")->on("categories")->onDelete("cascade");

            $table->unsignedInteger("advertisement_id");

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->index(["category_id", "advertisement_id"]);

            $table->unique([
                "category_id",
                "advertisement_id",
                "exact"
            ], "c_a_c_id_a_id_e_u");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_advertisements');
    }
}
