<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_advertisements', function (Blueprint $table) {

            $table->boolean("include");

            $table->unsignedInteger("post_id");

            $table->foreign("post_id")->references("id")->on("posts")->onDelete("cascade");

            $table->unsignedInteger("advertisement_id");

            $table->foreign("advertisement_id")->references("id")->on("advertisements")->onDelete("cascade");

            $table->index(["post_id", "advertisement_id"]);

            $table->unique(["post_id", "advertisement_id"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_advertisements');
    }
}
