<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_categories', function (Blueprint $table) {

            $table->unsignedInteger("post_id");

            $table->foreign("post_id")->references("id")->on("posts")->onDelete("cascade");

            $table->unsignedInteger("category_id");

            $table->foreign("category_id")->references("original_id")->on("categories")->onDelete("cascade");

            $table->unique(["post_id", "category_id"]);

            $table->index(["post_id", "category_id"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_categories');
    }
}
