<?php

use App\Models\Target;
use Illuminate\Database\Seeder;

class TargetsTableSeeder extends Seeder
{

    public function run()
    {

        foreach(config("countries", []) as $key => $value) {

            Target::updateOrCreate([
                "value" => $value,
                "key" => $key
            ]);

        }

    }
}
