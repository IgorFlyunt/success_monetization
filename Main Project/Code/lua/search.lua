ngx.req.read_body()

function decode(s)

    s = s:gsub('+', ' ')

    s = s:gsub('%%(%x%x)', function(h) return string.char(tonumber(h, 16)) end)

    return s
end

function parse_url(s)

    local ans = {}

    for k, v in s:gmatch('([^&=]+)=([^&=]*)&?') do

        ans[k] = decode(v)
    end

    return ans
end

local params = parse_url(ngx.req.get_body_data())

if (type(params['token']) == 'string' and params['token'] ~= '' and type(params['page_url']) == 'string' and params['page_url'] ~= '') then

    local config = {
        port = 6379,
        host = "127.0.0.1",
        password = "12345"
    }

    local redis = require "resty.redis"

    local client = redis:new()

    local ok, err = client:connect(config["host"], config["port"])

    if ok and not err then

        ok, err = client:auth(config["password"])

        if ok then

            local function has_value(tab, val)

                for index, value in ipairs(tab) do

                    if value == val then

                        return index
                    end
                end

                return nil
            end

            client:select(1) --select config database here

            local domain = client:get(params['token'])

            local referrer = ngx.req.get_headers()["Origin"]

            if type(domain) == "string" and domain ~= nil and domain ~= "" and type(referrer) == "string" and referrer ~= nil and referrer ~= "" then

                if (referrer:gsub("^http[s]?://([^/]+).*$", '%1')) == (domain:gsub("^http[s]?://([^/]+).*$", '%1')) then

                    local json = {}

                    local chunks = {}

                    local variants = {
                        params['page_url']
                    }

                    for match in (params['page_url']):gsub("^/%s*(.-)%s*$", "%1"):gmatch("(.-)/") do

                        if match ~= '' then

                            table.insert(chunks, match);

                            table.insert(variants, '/' .. table.concat(chunks, "/") .. "*")
                        end
                    end

                    local ids = {}

                    for iterator = #variants, 1, -1 do

                        local result, err = client:smembers(params['token'] .. ':i:' .. variants[iterator])

                        if not err and table.getn(result) > 0 then

                            for sub_iterator = #result, 1, -1 do

                                if has_value(ids, result[sub_iterator]) == nil then

                                    table.insert(ids, result[sub_iterator])
                                end
                            end
                        end
                    end

                    if not err and table.getn(ids) > 0 then

                        for iterator = #ids, 1, -1 do

                            if client:sismember(params['token'] .. ':e:' .. ids[iterator], params['page_url']) ~= 0 then

                                table.remove(ids, iterator)
                            end
                        end

                        if table.getn(ids) > 0 then

                            client:select(2) --select advertisements database here

                            local function get_first_index(values)

                                if values[0] ~= nil then

                                    return 0
                                end

                                return 1
                            end

                            for iterator = #ids, 1, -1 do

                                local adv

                                local adv_type = ids[iterator]:match("^(%S+):.*$")

                                if adv_type == "a" then

                                    adv = client:get(params['token'] .. ":" .. ids[iterator])

                                    if type(adv) == "string" and adv ~= "" then

                                        table.insert(json, adv)
                                    end

                                elseif adv_type == "g" then

                                    local members = client:smembers(params["token"] .. ":" .. ids[iterator])

                                    if table.getn(members) > 0 then

                                        local current_index

                                        local group_id = ids[iterator]:match("^%S+:(%d+)$")

                                        local next = client:get(params["token"] .. ":q:" .. group_id)

                                        if type(next) == "string" and next ~= "" then

                                            current_index = has_value(members, next)

                                            if current_index == nil then

                                                next = nil
                                            end

                                        else

                                            next = nil
                                        end

                                        if current_index == nil then

                                            current_index = get_first_index(members)
                                        end

                                        if next == nil then

                                            next = members[current_index]
                                        end

                                        adv = client:get(params['token'] .. ":a:" .. next)

                                        if type(adv) == "string" and adv ~= "" then

                                            table.insert(json, adv)
                                        end

                                        current_index = current_index + 1

                                        if members[current_index] == nil then

                                            current_index = get_first_index(members)
                                        end

                                        client:set(params["token"] .. ":q:" .. group_id, members[current_index])
                                    end
                                end
                            end
                        end

                        ngx.status = ngx.HTTP_OK

                        ngx.header["Content-Type"] = "application/json; charset=utf-8"

                        ngx.header["Access-Control-Allow-Origin"] = ngx.req.get_headers()["Origin"]

                        ngx.header["Access-Control-Allow-Methods"] = "POST"

                        ngx.say("[" .. table.concat(json, ',') .. "]")

                        return ngx.exit(ngx.HTTP_OK)
                    end
                end
            end
        end
    end
end

return ngx.exit(404)
