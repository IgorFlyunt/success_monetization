const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.extract([
    "vue",
    "axios",
    "lodash",
    "sweetalert2",
    "jquery",
    "moment",
    "bootstrap-sass",
    "bootstrap-select",
    "inputmask/dist/jquery.inputmask.bundle.js"
], "public/themes/success-monetization/assets/js/base.js").version()
  .autoload({
      jquery: ['$', 'jQuery', 'jquery'],
      vue: 'Vue',
      lodash: '_',
      axios: ["axios", "window.axios"],
      sweetalert2: "swal",
      moment: "moment"
  })
  .js(
      "resources/themes/success-monetization/assets/js/datatables.js",
      "public/themes/success-monetization/assets/js/datatables.js"
  ).version()
  .js(
      "resources/themes/success-monetization/assets/js/daterangepicker.js",
      "public/themes/success-monetization/assets/js/daterangepicker.js"
  ).version()
  .js(
      "resources/themes/success-monetization/assets/js/main-script.js",
      "public/themes/success-monetization/assets/js/main-script.js"
  ).version()
  .js(
      "resources/themes/success-monetization/assets/js/app.js",
      "public/themes/success-monetization/assets/js/app.js"
  ).version()
  .js(
      "resources/themes/success-monetization/assets/js/theme.js",
      "public/themes/success-monetization/assets/js/theme.js"
  )
  .js(
    "resources/themes/success-monetization/assets/js/realtime.js",
    "public/themes/success-monetization/assets/js/realtime.js"
  )
  .sass(
      "resources/themes/success-monetization/assets/sass/base.scss",
      "public/themes/success-monetization/assets/css/base.css"
  ).version()
  .sass(
      "resources/themes/success-monetization/assets/sass/datatables.scss",
      "public/themes/success-monetization/assets/css/datatables.css"
  ).version()
  .styles(
      "node_modules/daterangepicker/daterangepicker.css",
      "public/themes/success-monetization/assets/css/daterangepicker.css"
  ).version()
  .styles([
      "resources/themes/success-monetization/assets/css/skin/skin-purple.min.css",
      "resources/themes/success-monetization/assets/css/style.css"
  ], "public/themes/success-monetization/assets/css/style.css").version()
  .copyDirectory(
      "resources/themes/success-monetization/assets/img",
      "public/themes/success-monetization/assets/img"
  );
