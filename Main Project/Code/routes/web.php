<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*
|--------------------------------------------------------------------------
| All Landing Routes
|--------------------------------------------------------------------------
*/
Route::match(['get','post'],'/',['uses'=>'Landing\IndexLandingController@show','as'=>'index-landing']);
/*
* --------------------------------------------------------------------------
*/

Route::match(['get','post'], '/dashboard', ['middleware'=>['auth'], 'uses'=>'IndexController@show', 'as'=>'dashboard']);

$router->group(["prefix" => "contexts", "as" => "contexts."], function($router) {

    $router->group(["prefix" => "templates", "as" => "templates."], function($router) {

        $router->get("/", "ResolverController@showTemplates")->name("index");

        $router->get("create/{identifier?}", "ResolverController@createTemplate")->name("create");

        $router->group(["prefix" => "{identifier}"], function($router) {

            $router->get("edit", "ResolverController@editTemplate")->name("edit");

            $router->get("copy", "ResolverController@copyTemplate")->name("copy");

        });

    });

    $router->group(["prefix" => "{identifier}"], function($router) {

        $router->group(["prefix" => "copy"], function($router) {

            $router->get("/", "ResolverController@showCopy");

            $router->post("/", "ResolverController@saveCopy");

        });

        $router->post("children", "ResolverController@loadChildren");

        $router->post("original", "ResolverController@toOriginal");

    });

    $router->group(["prefix" => "load"], function($router) {

        $router->post("/", "ResolverController@loadOriginal");

        $router->post("duplicate", "ResolverController@loadDuplicate");

    });

});

$router->group(["prefix" => "groups", "as" => "groups."], function($router) {

    $router->post("advertisements", "ResolverController@advertisements");

    $router->post("load", "ResolverController@load");

});

$router->group(["prefix" => "site/{site_id}", "as" => "site."], function($router) {

    $router->get("/", "ResolverController@index")->name("index");

    $router->group(["prefix" => "sync", "as" => "sync."], function($router) {

        $router->match(["put", "patch", "post"], "update", "ResolverController@updateSync");

        $router->post("/", "ResolverController@syncAction");

    });

    $router->group(["prefix" => "{section}", "as" => "advertisements."], function($router) {

        $router->post("positions", "ResolverController@loadPositions");

        $router->post("groups", "ResolverController@loadGroups");

        $router->post("targets", "ResolverController@loadTargets");

        $router->post("categories", "ResolverController@loadCategories");

        $router->post("posts", "ResolverController@loadPosts");

        $router->post("trackers", "ResolverController@loadTrackers");

        $router->post("select", "ResolverController@loadForSelect");

        $router->post("load", "ResolverController@load");

        $router->get("/", "ResolverController@index")->name("index");

        $router->get("create", "ResolverController@create")->name("create");

        $router->post("/", "ResolverController@store");

        $router->post('actions', "ResolverController@actions");

        $router->delete("{identifier}", "ResolverController@destroy");

        $router->group(["prefix" => "{identifier}", "where" => ["identifier" => "[0-9]+"]], function($router) {

            $router->get("edit", "ResolverController@edit")->name("edit");

            $router->match(["put", "patch", "post"], "/", "ResolverController@update")->name("update");

            $router->post("group", "ResolverController@changeGroup")->name("group");

        });

    });

    $router->group(["prefix" => "comments", "as" => "comments."], function($router) {

        $router->post("load", "ResolverController@load");

        $router->post("destroy/{identifier}", "ResolverController@destroy");

    });

});

$router->group(["prefix" => "sites", "as" => "sites."], function($router) {

    $router->post("load", "ResolverController@load");

});

$router->group(["prefix" => "comments", "as" => "comments."], function($router) {

    $router->post("load", "ResolverController@load");

    $router->post("destroy/{identifier}", "ResolverController@destroy");

});

$router->group(["prefix" => "trash", "as" => "trash."], function($router) {

    $router->group(["prefix" => "{section}"], function($router) {

        $router->get("/", "ResolverController@index")->name("index");

        $router->post("load/{identifier?}", "ResolverController@load");

        $router->post("empty", "ResolverController@empty");

        $router->post('actions', "ResolverController@actions")->name("actions");

        $router->delete("{identifier}", "ResolverController@destroy")->name("destroy");

    });

});

/*
  |--------------------------------------------------------------------------
  | Resolver Controller
  |--------------------------------------------------------------------------
*/

$router->group(["prefix" => "{section}", "as" => "resolver."], function($router) {

    $router->get('/', "ResolverController@index")->name("index");

    $router->get("create", "ResolverController@create")->name("create");

    $router->post('/', "ResolverController@store")->name("store");

    $router->post('actions', "ResolverController@actions")->name("actions");

    $router->get("{identifier}/edit", "ResolverController@edit")->name("edit");

    $router->match(["put", "patch", "post"], "{identifier}", "ResolverController@update")->name("update");

    $router->delete("{identifier}", "ResolverController@destroy")->name("destroy");

});

/*
 * --------------------------------------------------------------------------
 */
