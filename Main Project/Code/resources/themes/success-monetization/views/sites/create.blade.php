@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <sites-form alias="{{ $alias }}"></sites-form>
    </section>
@endsection
