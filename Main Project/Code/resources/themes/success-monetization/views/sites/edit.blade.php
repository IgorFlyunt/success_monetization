@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <sites-form alias="{{ $alias }}"
                    :model="{{ $object }}">
        </sites-form>
    </section>
@endsection
