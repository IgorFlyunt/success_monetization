@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <categories-create alias="{{ $alias }}"></categories-create>
        <categories-list :elements="{{ $elements }}" alias="{{ $alias }}"></categories-list>
    </section>
@endsection
