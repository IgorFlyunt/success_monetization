@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <trackers-create alias="{{ $alias }}"></trackers-create>
        <trackers-list :elements="{{ $elements }}" alias="{{ $alias }}"></trackers-list>
    </section>
@endsection
