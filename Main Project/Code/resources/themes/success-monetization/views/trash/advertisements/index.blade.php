@php($key = key($tabs))

@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @foreach($tabs as $tab => $label)
                    <li @if($tab == $key) class="active" @endif>
                        <a href="#{{ $tab }}" data-toggle="tab">
                            {{ $label }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(array_keys($tabs) as $tab)
                    <div class="tab-pane @if($tab == $key) active @endif" id="{{ $tab }}">
                         @widget("table", "load/{$tab}", $config)
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@push("header-title")
<trash-clear-button alias="{{ $alias }}" />
@endpush
