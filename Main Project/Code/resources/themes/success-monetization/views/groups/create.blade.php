@extends("dashboard::layouts.app")

@section("content")
    <groups-form alias="{{ $alias }}"
                 :types="{{ json_encode($types) }}">
    </groups-form>
@endsection
