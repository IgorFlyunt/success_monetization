@extends("dashboard::layouts.app")

@section("content")
    <groups-form alias="{{ $alias }}"
                 :model="{{ json_encode($object->toArray()) }}"
                 @if(sizeof($advertisements)) :advertisements_list="{{ json_encode($advertisements) }}" @endif
                 :types="{{ json_encode($types) }}">
    </groups-form>
@endsection
