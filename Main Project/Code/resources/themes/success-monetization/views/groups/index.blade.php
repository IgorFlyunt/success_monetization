@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <div class="box box-solid">
            <div class="box-body">
                @widget("table", "load", $config)
            </div>
        </div>
    </section>
@endsection
