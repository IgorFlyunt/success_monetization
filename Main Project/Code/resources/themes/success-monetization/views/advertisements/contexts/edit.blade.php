@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <contexts-form alias="{{ $alias }}"
                    @isset($copy) :copy="true" @endisset
                    :model="{{ json_encode($object->toArray()) }}"
                       @isset($groups_list) :groups_list="{{ $groups_list }}" @endisset
                    :categories_list="{{ $categories_list }}">
        </contexts-form>

        @if(!isset($copy) && !$object->parent_id && $object->has_children)
            <div class="box box-solid box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Дубли объявления
                    </h3>
                </div>
                <div class="box-body">
                    @widget("table", "{$object->id}/children", $config)
                </div>
            </div>
        @endif
    </section>
@endsection

@push("header-title")
    @if(!isset($copy) && $object->parent_id)
        <contexts-to-original-button alias="{{ $alias }}"
                                     :id="{{ $object->id }}" />
    @endif
@endpush
