@extends("dashboard::layouts.app")

@section("content")
    <contexts-templates alias="{{ $alias }}"
                        :categories_list="{{ $categories_list }}"
                        :elements="{{ json_encode($elements) }}">
    </contexts-templates>
@endsection

@push("header-title")
<a href="{{ route("contexts.templates.create") }}" class="btn btn-success btn-flat pull-right">
    Создать новое
</a>
@endpush
