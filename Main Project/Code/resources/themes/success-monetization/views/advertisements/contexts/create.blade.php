@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <contexts-form alias="{{ $alias }}"
                        @if(isset($not_template) && $not_template) :not_template="true" @endisset
                        @isset($templates) :templates="{{ $templates }}" @endisset
                        @isset($template) template="{{ $template }}" @endisset
                       @isset($groups_list) :groups_list="{{ $groups_list }}" @endisset
                        :categories_list="{{ $categories_list }}">
        </contexts-form>
    </section>
@endsection
