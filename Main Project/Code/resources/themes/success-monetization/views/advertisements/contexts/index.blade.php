@extends("dashboard::layouts.app")

@section("content")
    <section class="content">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Оригинальные объявления
                </h3>
            </div>
            <div class="box-body">
                @widget("table", "load", $config["originals"])
            </div>
        </div>
    </section>
    <section class="content">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Дубли объявлений
                </h3>
            </div>
            <div class="box-body">
                @widget("table", "load/duplicate", $config["duplications"])
            </div>
        </div>
    </section>
@endsection
