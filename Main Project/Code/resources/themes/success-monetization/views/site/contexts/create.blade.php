@extends("dashboard::layouts.site")

@section("content")
    <section class="content">
        <site-advertisements-contexts-create alias="{{ $alias }}"
                                             :positions="{{ json_encode($positions) }}">
        </site-advertisements-contexts-create>
    </section>
@endsection
