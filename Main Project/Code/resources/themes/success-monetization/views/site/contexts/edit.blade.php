@extends("dashboard::layouts.site")

@section("content")
    <section class="content">
        <site-advertisements-contexts-edit alias="{{ $alias }}"
                                           :positions="{{ json_encode($positions) }}"
                                           :model="{{ json_encode($object) }}">
        </site-advertisements-contexts-edit>
    </section>
@endsection
