@extends("dashboard::layouts.site")

@section("content")
    <section class="content">
        <div class="box box-solid">
            <site-advertisements-filter-component alias="{{ $alias }}"></site-advertisements-filter-component>
            <div class="box-body">
                @widget("table", "load", $config)
            </div>
        </div>
    </section>
@endsection

@push("header-title")
<a href="{{ route("site.advertisements.create", [$site_id, "contexts"]) }}" class="btn btn-success btn-flat pull-right">
    Создать новое
</a>
@endpush
