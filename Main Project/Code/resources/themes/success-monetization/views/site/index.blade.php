@extends("dashboard::layouts.site")

@section("content")
    <section class="content">
        <site-synchronization
            alias="{{ $alias }}"
            default_periodicity="{{ $default_periodicity }}"
            :periodicities="{{ json_encode($periodicities) }}"
            :model="{{ $object }}">
        </site-synchronization>
    </section>
@endsection

@push("scripts")
    <script src="{{ mix('themes/success-monetization/assets/js/realtime.js') }}" type="text/javascript"></script>
@endpush
