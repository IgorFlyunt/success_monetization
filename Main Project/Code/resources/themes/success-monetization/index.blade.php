@extends(env('THEME_BACKEND').'.layouts.site')

@section('sidebar')
    {!! $sidebar !!}
@endsection

@section('content')
    {!! $content !!}
@endsection