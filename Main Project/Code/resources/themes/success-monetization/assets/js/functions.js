window.trans = (label) => {

    let chunks = label.split('.'),
        step = window.translations;

    for(let chunk in chunks) {

        if(chunks[chunk] in step) {

            step = step[chunks[chunk]];

        } else break;

    }

    return typeof step === 'string' ? step : label;

};

window.buildFormData = (data, fields) => {

    let form_data = new FormData(),
        value,
        field;

    for(let index in fields) {

        field = fields[index];

        value = data[field];

        if(typeof value === 'object') {

            if(value !== null) {

                if(Array.isArray(value) && value.length) {

                    if(value.length) {

                        field = `${field}[]`;

                        for(let value_index in value) {

                            form_data.append(
                                field,
                                value[value_index]
                            );

                        }

                        continue;

                    }

                }

            } else value = '';

        } else if(typeof value === 'boolean') {

            value = value ? 1 : 0;

        }

        form_data.append(
            field,
            value
        );

    }

    return form_data;

};

window.capitalize = (string) => {

    return string.charAt(0).toUpperCase() + string.slice(1);

}

window.loadModelData = (model, field, default_value, decorator) => {

    if(model === null) {

        return default_value;

    }

    if(field in model) {

        if(model[field] !== null ||
           typeof model[field] === 'string' &&
           model[field].length) {

            if(typeof decorator === 'undefined') {

                return model[field];

            }

            return decorator(model[field]);

        }

    }

    return default_value;

};
