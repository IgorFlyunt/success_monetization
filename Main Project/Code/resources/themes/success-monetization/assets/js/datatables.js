import config from "./datatables-config";
import {ClientTable} from 'vue-tables-2';

Vue.use(ClientTable, config);

Vue.component("datatable-component", require("./components/datatable/datatable"));
