const config = {
    locale: 'ru',
    dictionary:
        {
            ru: {
                messages: {
                    _default: (field) => `Значение поля '${field}' не валидно.`,
                    after: (field, [target]) => `Поле '${field}' должно быть после '${target}'.`,
                    alpha_dash: (field) => `Поле '${field}' могут содержать буквенно-цифровые символы, а также тире и символы подчеркивания.`,
                    alpha_num: (field) => `Поле '${field}' могут содержать только буквенно-цифровые символы.`,
                    alpha_spaces: (field) => `Поле '${field}' могут содержать только буквенные символы, а также пробелы.`,
                    alpha: (field) => `Поле '${field}' могут содержать только буквенные символы.`,
                    before: (field, [target]) => `Поле '${field}' должен быть до '${target}'.`,
                    between: (field, [min, max]) => `Поле '${field}' должно быть между ${min} и ${max}.`,
                    confirmed: (field) => `Поле '${field}' подтверждение не соответствует.`,
                    credit_card: (field) => `Поле '${field}' является недействительным.`,
                    date_between: (field, [min, max]) => `Поле '${field}' должно быть между ${min} и ${max}.`,
                    date_format: (field, [format]) => `Поле '${field}' должен быть в формате ${format}.`,
                    decimal: (field, [decimals] = ['*']) => `Поле '${field}' должно быть числовым и содержать ${decimals === '*' ? '' : decimals} десятичные точки.`,
                    digits: (field, [length]) => `Поле '${field}' должно быть числовым и точно содержать ${length} цифры.`,
                    dimensions: (field, [width, height]) => `Поле '${field}' must be ${width} пикселей на ${height} пикселей.`,
                    email: (field) => `Поле '${field}' должно быть валидным Email-ом.`,
                    ext: (field) => `Поле '${field}' должен быть действительным файлом.`,
                    image: (field) => `Поле '${field}' должно быть изображением.`,
                    in: (field) => `Поле '${field}' должно быть действительным значением.`,
                    ip: (field) => `Поле '${field}' должен быть действительным IP-адресом.`,
                    max: (field, [length]) => `Поле '${field}' может быть не больше ${length} символов.`,
                    max_value: (field, [max]) => `Поле '${field}' должно быть ${max} или меньше.`,
                    mimes: (field) => `Поле '${field}' должен иметь действительный тип файла.`,
                    min: (field, [length]) => `Поле '${field}' должен быть не менее ${length} символов.`,
                    min_value: (field, [min]) => `Поле '${field}' должно быть ${min} или больше.`,
                    not_in: (field) => `Поле '${field}' должно быть действительным значением.`,
                    numeric: (field) => `Поле '${field}' могут содержать только числовые символы.`,
                    regex: (field) => `Поле '${field}' формат недействителен.`,
                    required: (field) => `Поле ''${field}'' обязательно.`,
                    size: (field, [size]) => `Поле '${field}' должно быть меньше ${size} KB.`,
                    url: (field) => `Поле '${field}' не является допустимым URL-адресом.`
                },
                attributes: {
                  email: "Email",
                  password: "Пароль",
                  name: "Название",
                  list_action: "Действие с выделенными",
                  css: 'CSS',
                  html: 'HTML',
                  image: 'Изображение',
                  type: "Тип",
                  url: "URL",
                  message: "Сообщение",
                  key: "Ключ",
                  main: "Местоположение",
                  sub: "Номер позиции",
                  tracker_id: "Трекер"
                }
            }
        }
};

export default config;
