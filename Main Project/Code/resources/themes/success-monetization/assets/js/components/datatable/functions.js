window.DataTableFunctions = {
  sitesOpenInNewTab: (selected) => {

      selected.forEach((item) => {

          window.open(
              item['url']['url'],
              '_blank'
          );

      });

  },
  advertisementsParentDeletedDecorator: (selected) => {

      if(Array.isArray(selected)) {

          return selected.map((item) => {

              return item['id'];

          });

      }

      return [selected['id']];

  },
  advertisementsParentDeletedEvent: (data, parent_ids) => {

      data.forEach((item, index) => { //TODO - протестити

          if(item['parent_id'] != null && parent_ids.indexOf(item['parent_id']) >= 0) {

              eventHub.$emit("datatable-originals-event", {
                  name: "advertisementsAddToList",
                  data: Object.assign(item, {
                      children: []
                  })
              });

              Vue.delete(
                  data,
                  index
              );

          }

      });

  },
  advertisementsAddToList: (data, element) => {

      data.push(element);

  },
  clearAll: (data) => {

      data.splice(0, data.length);

  },
  synchronizationListener: (table) => {

    addUserListener('Site.SynchronizationEvent', (e) => {

      let index = table['data'].findIndex((item) => {

        return item[table['identifier']] == e.site_id;

      });

      if(index >= 0) {

        let status = e.status == true;

        swal({
          type: status ? "success" : "error",
          title: e.message
        }).then(() => {

          Vue.set(
            table['data'][index],
            'api_status',
            status
          );

          if(typeof e.synchronized_at === 'string' && e.synchronized_at.length) {

            Vue.set(
              table['data'][index],
              'synchronized_at',
              moment(
                e.synchronized_at
              ).format(
                "DD-MM-YYYY HH:mm:ss"
              )
            );

          }

        });

      }

    });

  },
  refreshSiteAdvertisements: (data, config) => {

    let set_data = (data, values) => {

      if(data.length > values.length) {

        data.splice(values.length);

      }

      for(let index in values) {

        Vue.set(
          data,
          index,
          values[index]
        );

      }

    };

    if(config['value'].length) {

      axios.post(`/${config['alias']}/load`, {
        type: config['type'],
        value: config['value']
      }).then((response) => {

        if(response.data.status) {

          set_data(
            data,
            response.data.elements
          );

        } else defaultErrorMessage();

      }).catch((error) => responseErrorWorker(error));

    } else set_data(data, []);

  }

};
