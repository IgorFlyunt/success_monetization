require("daterangepicker");

$('#daterange-btn').daterangepicker(
    {
        locale: {
            "format": 'DD-MM-YYYY',
            "separator": " - ",
            "applyLabel": "Применить",
            "cancelLabel": "Отмена",
            "fromLabel": "З",
            "toLabel": "По",
            "customRangeLabel": "Выбор периода",
            "daysOfWeek": [
                "ПН",
                "ВТ",
                "СР",
                "ЧТ",
                "ПТ",
                "СБ",
                "ВС"
            ],
            "monthNames": [
                "Янв",
                "Фев",
                "Мрт",
                "Апр",
                "Май ",
                "Июн",
                "Июл",
                "Авг",
                "Сен",
                "Окт",
                "Нбр",
                "Дек"
            ],
            "firstDay": 0
        },
        ranges: {
            'Сегодня': [moment(), moment()],
            'Вчера': [moment().subtract('days', 1), moment().subtract('days', 1)],
            'Последние 7 дней': [moment().subtract('days', 6), moment()],
            'Последние 30 дней': [moment().subtract('days', 29), moment()],
            'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
            'Прошлый месяц': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        },
        startDate: moment().subtract('days', 29),
        endDate: moment()
    },
    function (start, end) {
        $('.reportrange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
    }
);
