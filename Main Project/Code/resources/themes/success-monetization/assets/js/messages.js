window.defaultErrorMessage = () => {

    swal({
        type: "error",
        title: trans('error'),
        confirmButtonText: trans('ok')
    });

};

window.showMessages = (messages, redirect = {}) => {

    let typed = [],
        message;

    for(let type in messages) {

        message = {
            type: type,
            confirmButtonText: trans('ok')
        };

        if(messages[type].length === 1) {

            message['title'] = messages[type][0];

        } else {

            message['title'] = trans("messages." + type);

            message['html'] = messages[type].join('<br />');

        }

        typed.push(message);

        if(typeof redirect['url'] === 'string') {

            if(redirect['type'] === type) {

                typed.push({
                    redirect: redirect['url']
                });

            }

        }

    }

    if(typed.length && typeof redirect['url'] === 'string') {

        if(typeof redirect['type'] === 'undefined') {

            typed.push({
                redirect: redirect['url']
            });

        }

    }

    let displayMessages = (typed, index = 0) => {

        if(index in typed) {

            if('redirect' in typed[index]) {

                window.location.href = typed[index]['redirect'];

            } else {

                swal(typed[index]).then(() => {

                    displayMessages(typed, index + 1);

                });

            }

        }

    };

    displayMessages(typed);

};

window.responseErrorWorker = (error) => {

    let response = error.response;

    if('data' in response &&
       typeof response.data === 'object' &&
       'messages' in response.data) {

        let messages = [];

        for(let field in response.data['messages']) {

            messages = messages.concat(response.data['messages'][field]);

        }

        showMessages({error: messages});

    } else defaultErrorMessage();

};

window.responseMessagesWorker = (response, redirect = {}) => {

    if(typeof response.data === 'object' &&
       'messages' in response.data) {

        showMessages(response.data['messages'], redirect);

    }

};

window.showMessage = (title, type = 'success', text = '', is_html = false) => {

    let data = {
        type: type,
        title: title
    };

    if(text.length) {

        data[is_html ? 'html' : 'text'] = text;

    }

    swal(data);

};

window.closureQuestion = (closure, type = 'warning', title, text = '', return_result = false) => {

    if(typeof text != 'string' || !text.length) {

        text = trans("are you sure?");

    }

    swal({
        type: type,
        title: title,
        text: text,
        confirmButtonText: trans("yes"),
        showCancelButton: true,
        cancelButtonText: trans("no")
    }).then((result) => {

        if(return_result) {

            closure(result);

        } else if(result.value) {

            closure();

        }

    });

};

showMessages(window.flash_messages || []);

window.oneFieldEditingModal = (url, field, closure, default_value = '') => {

    swal({
        type: 'info',
        input: 'text',
        inputValue: default_value,
        title: trans("edit"),
        text: trans("attention, data will be changed"),
        showCancelButton: true,
        cancelButtonText: trans("cancel"),
        allowOutsideClick: () => !swal.isLoading(),
        preConfirm: (value) => {

            swal.showLoading();

            let data = {};

            data[field] = value;

            return axios.put(url, data).then((response) => {

                return {
                    response: response,
                    new_value: value
                };

            }).catch((error) => {

                let msg = trans("error");

                if('data' in error.response && 'messages' in error.response['data']) {

                    msg = Object.values(
                        error.response['data']['messages']
                    ).join('<br />');

                }

                swal.showValidationError(msg);

            });

            swal.hideLoading();

        }

    }).then((result) => {

        if('value' in result) {

            closure(
                result.value['response'],
                result.value['new_value']
            );

        }

    });

};
