let decorateToast = (title, type, timer) => {

    return {
        type: type,
        toast: true,
        timer: timer,
        title: title,
        position: 'bottom-end',
        confirmButtonText: trans("close")
    };

};

window.showToast = (title, type = 'success', timer = 5000) => {

    swal(
        decorateToast(
            title,
            type,
            timer
        )
    );

};

window.showToasts = (toasts, type = 'success', timer = 5000) => {

    let local_type, local_timer, show_recursive = (index, toasts) => {

        if(index in toasts) {

            if('type' in toasts[index]) {

                local_type = toasts[index]['type'];

            } else local_type = type;

            if('timer' in toasts[index]) {

                local_timer = toasts[index]['timer'];

            } else local_timer = timer;

            swal(
                decorateToast(
                    toasts[index]['title'],
                    local_type,
                    local_timer
                )
            ).then(() => {

                show_recursive(index + 1, toasts);

            });

        }

    };

    show_recursive(0, toasts);

};
