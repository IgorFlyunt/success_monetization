/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

window.io = require('socket.io-client');

window.Echo = new (require("laravel-echo"))({
  broadcaster: 'socket.io',
  host: window.location.hostname,
  path: "/ws/socket.io"
});

window.addUserListener = (event, callback) => {

  if(typeof user_id === 'number') {

    if(typeof window.userListener === 'undefined') {

      window.userListener = Echo.private(`user.${user_id}`);

    }

    userListener.listen(event, callback);

  }

};
