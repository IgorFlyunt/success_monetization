/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.mixin({
    methods: {

        recursiveCheck(object, closure) {

            let promises = [];

            let recursive_check = (element, promises) => {

                promises.push(element.$validator.validateAll());

                for(let child in element.$children) {

                    recursive_check(element.$children[child], promises);

                }

            };

            recursive_check(object, promises);

            Promise.all(promises).then((result) => {

                if (result.indexOf(false) < 0) {

                    closure();

                }

            }).catch((error) => {

                console.log(error);

            });

        },

        trans(label) {

            return window.trans(label);

        }

    }
});

window.eventHub = new Vue();

Vue.component("select-component", require("./components/inputs/select"));
Vue.component("categories-create", require("./components/categories/create"));
Vue.component("categories-list", require("./components/categories/list"));
Vue.component("contexts-form", require("./components/advertisements/contexts/form"));
Vue.component("contexts-to-original-button", require("./components/advertisements/contexts/to-original-button"));
Vue.component("contexts-templates", require("./components/advertisements/contexts/templates"));
Vue.component("comments-form", require("./components/comments/form"));
Vue.component("groups-form", require("./components/groups/form"));
Vue.component("trackers-create", require("./components/trackers/create"));
Vue.component("trackers-list", require("./components/trackers/list"));
Vue.component("sites-form", require("./components/sites/form"));
Vue.component("site-synchronization", require("./components/site/synchronization"));
Vue.component("trash-clear-button", require("./components/trash/clear-button"));
Vue.component("site-advertisements-contexts-create", require("./components/site/advertisements/contexts/create"));
Vue.component("site-advertisements-contexts-edit", require("./components/site/advertisements/contexts/edit"));
Vue.component("site-advertisements-filter-component", require("./components/site/advertisements/partials/filter"));

const app = new Vue({
    el: '#app'
});
