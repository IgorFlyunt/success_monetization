const config = {
    template: 'footerPagination',
    perPage: 10,
    // perPageValues: [10,20,50,100],
    texts:{
        count:"Показано с {from} по {to} из {count} записей|Показано {count} записей|",
        first:'Первая',
        last:'Последняя',
        filter:"",
        filterPlaceholder:"Поиск",
        limit:"Показывать:",
        page:"Страница:",
        noResults:"Нет соответствующих записей",
        filterBy:"Сортировать по {column}",
        loading:'Загрузка...',
        defaultOption:'Выбрать {column}',
        columns:'Колонки'
    },
};

export default config;
