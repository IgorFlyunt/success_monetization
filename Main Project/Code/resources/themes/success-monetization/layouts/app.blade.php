<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {!! app("meta")->render() !!}
        @include("dashboard::partials.styles")
    </head>
    <body class="skin-purple layout-boxed">

        <div class="wrapper" id="app">

            @section("header")
                @include("dashboard::partials.header")
            @show

            @section("sidebar")
                @widget("sidebar", $user)
            @show

            <div class="content-wrapper">
                @include("dashboard::partials.global.title")
                @yield('content')
            </div>

            @section("footer")
                @include("dashboard::partials.footer")
            @show

            <modals-container/>

        </div><!-- /.wrapper -->
        @include("dashboard::partials.scripts")
    </body>
</html>
