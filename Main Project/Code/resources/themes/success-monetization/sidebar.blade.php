<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel success-monetization-user">
            <div class="pull-left image">
                <img src="{{ asset(env('THEME_FRONTEND')) }}/assets/img/avatar.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p class="margin-0">Alexander Pierce</p>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">МЕНЮ</li>
            <li {{ (URL::current() == route('dashboard') ? "class=active" : '') }}>
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Дашбоард</span>
                </a>
            </li>
            <li {{ (URL::current() == route('adv-category') ? "class=active" : '') }}>
                <a href="{{ route('resolver.index', 'categories') }}">
                    <i class="fa fa-list-ul"></i> <span>Рекламные рубрики</span>
                </a>
            </li>
            <li @if (URL::current() == route('adv-groups')
                  or URL::current() == route('add-adv-group')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif
            >
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Рекламные группы</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (URL::current() == route('adv-groups') ? "class=active" : '') }}>
                        <a href="{{ route('adv-groups') }}"><i class="fa fa-circle-o"></i> Мои группы</a>
                    </li>
                    <li {{ (URL::current() == route('add-adv-group') ? "class=active" : '') }}>
                        <a href="{{ route('add-adv-group') }}"><i class="fa fa-circle-o"></i> Создать группу</a>
                    </li>
                </ul>
            </li>
            <li @if (URL::current() == route('all-context-adv')
                        or URL::current() == route('add-context-adv-new')
                        or URL::current() == route('add-context-adv-from-template')
                        or URL::current() == route('context-adv-preview')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif
            >
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Контекстные объявления</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (URL::current() == route('all-context-adv') ? "class=active" : '') }}>
                        <a href="{{ route('all-context-adv') }}"><i class="fa fa-circle-o"></i> Мои обьявления</a>
                    </li>
                    <li {{ (URL::current() == route('add-context-adv') ? "class=active" : '') }}>
                        <a href="{{ route('add-context-adv') }}"><i class="fa fa-circle-o"></i> Создать новое</a>
                    </li>
                    <li {{ (URL::current() == route('add-context-adv-from-template') ? "class=active" : '') }}>
                        <a href="{{ route('add-context-adv-from-template') }}"><i class="fa fa-circle-o"></i> Создать шаблонное</a>
                    </li>
                    <li {{ (URL::current() == route('context-adv-preview') ? "class=active" : '') }}>
                        <a href="{{ route('context-adv-preview') }}"><i class="fa fa-circle-o"></i> Шаблоны</a>
                    </li>
                </ul>
            </li>
            <li @if (URL::current() == route('all-tizer-adv') or URL::current() == route('add-tizer-adv')) class="active" @endif>
                <a href="{{ route('all-tizer-adv') }}">
                    <i class="fa fa-folder"></i> <span>Тизеры</span>
                </a>
            </li>
            <li @if (URL::current() == route('all-banner-adv') or URL::current() == route('add-banner-adv')) class="active" @endif>
                <a href="{{ route('all-banner-adv') }}">
                    <i class="fa fa-folder"></i> <span>Баннеры</span>
                </a>
            </li>
            <li @if (URL::current() == route('all-popup-adv')
                        or URL::current() == route('add-popup-adv')
                        or URL::current() == route('add-popup-adv-from-template')
                        or URL::current() == route('popup-adv-preview')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif>
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Попапы</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (URL::current() == route('all-popup-adv') ? "class=active" : '') }}>
                        <a href="{{ route('all-popup-adv') }}"><i class="fa fa-circle-o"></i> Мои попапы</a>
                    </li>
                    <li {{ (URL::current() == route('add-popup-adv') ? "class=active" : '') }}>
                        <a href="{{ route('add-popup-adv') }}"><i class="fa fa-circle-o"></i> Создать новый</a>
                    </li>
                    <li {{ (URL::current() == route('add-popup-adv-from-template') ? "class=active" : '') }}>
                        <a href="{{ route('add-popup-adv-from-template') }}"><i class="fa fa-circle-o"></i> Создать шаблонный</a>
                    </li>
                    <li {{ (URL::current() == route('popup-adv-preview') ? "class=active" : '') }}>
                        <a href="{{ route('popup-adv-preview') }}"><i class="fa fa-circle-o"></i> Шаблоны</a>
                    </li>
                </ul>
            </li>
            <li @if (URL::current() == route('all-side-adv')
                        or URL::current() == route('add-side-adv')
                        or URL::current() == route('add-side-adv-from-template')
                        or URL::current() == route('side-adv-preview')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif>
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Плашки</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (URL::current() == route('all-side-adv') ? "class=active" : '') }}>
                        <a href="{{ route('all-side-adv') }}"><i class="fa fa-circle-o"></i> Мои плашки</a>
                    </li>
                    <li {{ (URL::current() == route('add-side-adv') ? "class=active" : '') }}>
                        <a href="{{ route('add-side-adv') }}"><i class="fa fa-circle-o"></i> Создать новою</a>
                    </li>
                    <li {{ (URL::current() == route('add-side-adv-from-template') ? "class=active" : '') }}>
                        <a href="{{ route('add-side-adv-from-template') }}"><i class="fa fa-circle-o"></i> Создать шаблонную</a>
                    </li>
                    <li {{ (URL::current() == route('side-adv-preview') ? "class=active" : '') }}>
                        <a href="{{ route('side-adv-preview') }}"><i class="fa fa-circle-o"></i> Шаблоны</a>
                    </li>
                </ul>
            </li>
            <li @if (URL::current() == route('all-text-adv')
                        or URL::current() == route('add-text-adv')
                        or URL::current() == route('add-text-adv-from-template')
                        or URL::current() == route('text-adv-preview')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif>
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Текстовки</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (URL::current() == route('all-text-adv') ? "class=active" : '') }}>
                        <a href="{{ route('all-text-adv') }}"><i class="fa fa-circle-o"></i> Мои текстовки</a>
                    </li>
                    <li {{ (URL::current() == route('add-text-adv') ? "class=active" : '') }}>
                        <a href="{{ route('add-text-adv') }}"><i class="fa fa-circle-o"></i> Создать новою</a>
                    </li>
                    <li {{ (URL::current() == route('add-text-adv-from-template') ? "class=active" : '') }}>
                        <a href="{{ route('add-text-adv-from-template') }}"><i class="fa fa-circle-o"></i> Создать шаблонную</a>
                    </li>
                    <li {{ (URL::current() == route('text-adv-preview') ? "class=active" : '') }}>
                        <a href="{{ route('text-adv-preview') }}"><i class="fa fa-circle-o"></i> Шаблоны</a>
                    </li>
                </ul>
            </li>
            <li @if (URL::current() == route('all-different-adv') or URL::current() == route('add-different-adv')) class="active" @endif>
                <a href="{{ route('all-different-adv') }}">
                    <i class="fa fa-folder"></i> <span>Разное</span>
                </a>
            </li>
            <li @if (URL::current() == route('all-adv-stat')
                        or URL::current() == route('all-adv-article-stat')
                        or URL::current() == route('context-adv-stat')
                        or URL::current() == route('context-adv-article-stat')
                        or URL::current() == route('context-adv-single-stat')
                        or URL::current() == route('tizer-adv-stat')
                        or URL::current() == route('tizer-adv-article-stat')
                        or URL::current() == route('tizer-adv-single-stat')
                        or URL::current() == route('banner-adv-stat')
                        or URL::current() == route('banner-adv-article-stat')
                        or URL::current() == route('banner-adv-single-stat')
                        or URL::current() == route('popup-adv-stat')
                        or URL::current() == route('popup-adv-article-stat')
                        or URL::current() == route('popup-adv-single-stat')
                        or URL::current() == route('side-adv-stat')
                        or URL::current() == route('side-adv-article-stat')
                        or URL::current() == route('side-adv-single-stat')
                        or URL::current() == route('text-adv-stat')
                        or URL::current() == route('text-adv-article-stat')
                        or URL::current() == route('text-adv-single-stat')
                        or URL::current() == route('group-adv-stat')
                        or URL::current() == route('different-adv-stat')
                        or URL::current() == route('different-adv-article-stat')
                        or URL::current() == route('different-adv-single-stat')
                        or URL::current() == route('income-stat')
                        or URL::current() == route('custom-stat')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif>
                <a href="#">
                    <i class="fa fa-bar-chart"></i> <span>Статистика</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li @if (URL::current() == route('all-adv-stat') or URL::current() == route('all-adv-article-stat')) class="active" @endif>
                        <a href="{{ route('all-adv-stat') }}"><i class="fa fa-circle-o"></i> Общая</a>
                    </li>
                    <li @if (URL::current() == route('context-adv-stat') or URL::current() == route('context-adv-article-stat') or URL::current() == route('context-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('context-adv-stat') }}"><i class="fa fa-circle-o"></i> Контекстные объявления</a>
                    </li>
                    <li @if (URL::current() == route('tizer-adv-stat') or URL::current() == route('tizer-adv-article-stat') or URL::current() == route('tizer-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('tizer-adv-stat') }}"><i class="fa fa-circle-o"></i> Тизеры</a>
                    </li>
                    <li @if (URL::current() == route('banner-adv-stat') or URL::current() == route('banner-adv-article-stat') or URL::current() == route('banner-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('banner-adv-stat') }}"><i class="fa fa-circle-o"></i> Баннеры</a>
                    </li>
                    <li @if (URL::current() == route('popup-adv-stat') or URL::current() == route('popup-adv-article-stat') or URL::current() == route('popup-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('popup-adv-stat') }}"><i class="fa fa-circle-o"></i> Попапы</a>
                    </li>
                    <li @if (URL::current() == route('side-adv-stat') or URL::current() == route('side-adv-article-stat') or URL::current() == route('side-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('side-adv-stat') }}"><i class="fa fa-circle-o"></i> Плашки</a>
                    </li>
                    <li @if (URL::current() == route('text-adv-stat') or URL::current() == route('text-adv-article-stat') or URL::current() == route('text-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('text-adv-stat') }}"><i class="fa fa-circle-o"></i> Текстовки</a>
                    </li>
                    <li {{ (URL::current() == route('group-adv-stat') ? "class=active" : '') }}>
                        <a href="{{ route('group-adv-stat') }}"><i class="fa fa-circle-o"></i> Группы</a>
                    </li>
                    <li @if (URL::current() == route('different-adv-stat') or URL::current() == route('different-adv-article-stat') or URL::current() == route('different-adv-single-stat')) class="active" @endif>
                        <a href="{{ route('different-adv-stat') }}"><i class="fa fa-circle-o"></i> Разное</a>
                    </li>
                    <li {{ (URL::current() == route('income-stat') ? "class=active" : '') }}>
                        <a href="{{ route('income-stat') }}"><i class="fa fa-circle-o"></i> По доходу</a>
                    </li>
                    <li {{ (URL::current() == route('custom-stat') ? "class=active" : '') }}>
                        <a href="{{ route('custom-stat') }}"><i class="fa fa-circle-o"></i> Ручная</a>
                    </li>
                </ul>
            </li>
            <li {{ (URL::current() == route('moderation') ? "class=active" : '') }}>
                <a href="{{ route('moderation') }}"><i class="fa fa-cog"></i> <span>Модерация</span></a>
            </li>
            <li {{ (URL::current() == route('sites') ? "class=active" : '') }}>
                <a href="{{ route('sites') }}"><i class="fa fa-diamond"></i> <span>Сайты</span></a>
            </li>
            <li {{ (URL::current() == route('adv-tracker-link') ? "class=active" : '') }}>
                <a href="{{ route('adv-tracker-link') }}">
                    <i class="fa fa-pie-chart"></i> <span>Трекеры</span>
                </a>
            </li>
            <li @if (URL::current() == route('showTrashSites')
                  or URL::current() == route('adv-trash')
                    )
                class="treeview active"
                @else
                class="treeview"
                    @endif
            >
                <a href="#">
                    <i class="fa fa-trash"></i> <span>Корзина</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li {{ (URL::current() == route('showTrashSites') ? "class=active" : '') }}>
                        <a href="{{ route('showTrashSites') }}"><i class="fa fa-circle-o"></i> Сайты</a>
                    </li>
                    <li {{ (URL::current() == route('adv-trash') ? "class=active" : '') }}>
                        <a href="{{ route('adv-trash') }}"><i class="fa fa-circle-o"></i> Реклама</a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
