<script>
 @auth
    window.user_id = {{ $user->id }};
 @endauth
 window.translations = {!! json_encode(trans("t")) !!};
 window.flash_messages = {!! json_encode(session()->pull("flash_messages", [])) !!};
</script>

<script src="{{ mix('themes/success-monetization/assets/js/manifest.js') }}" type="text/javascript"></script>
<script src="{{ mix('themes/success-monetization/assets/js/base.js') }}" type="text/javascript"></script>
<script src="{{ asset('themes/success-monetization/assets/js/theme.js') }}" type="text/javascript"></script>
@stack("scripts")
<script src="{{ mix('themes/success-monetization/assets/js/main-script.js') }}" type="text/javascript"></script>
<script src="{{ mix('themes/success-monetization/assets/js/app.js') }}" type="text/javascript"></script>
