@if(($title = app("meta")->getTitle()))
    <section class="content-header">
        <span>{{ $title }}</span>
        @stack("header-title")
    </section>
@endif
