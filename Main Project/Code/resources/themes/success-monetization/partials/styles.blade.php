<link href="{{ mix("themes/success-monetization/assets/css/base.css") }}" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet">
@stack("styles")
<link href="{{ mix("themes/success-monetization/assets/css/style.css") }}" rel="stylesheet" type="text/css" />
