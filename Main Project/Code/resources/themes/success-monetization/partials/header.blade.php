<header class="main-header">
    <!-- Logo -->
    <a href="{{ route("dashboard") }}" class="logo">
        <b>{{ config("app.name") }}</b>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle superadmin-sidebar" data-toggle="offcanvas" role="button"></a>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
           class="btn btn-flat btn-warning pull-right log-out">
            Выход
        </a>
        <a href="#"  class="btn btn-flat btn-info pull-right log-out">
            Профиль
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </nav>
</header>
