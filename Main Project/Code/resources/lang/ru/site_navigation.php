<?php

return [
    "synchronization" => "Синхронизация",
    "admin system" => "Админ система",
    "advertisements" => [
        "context" => [
            "templates" => "Шаблоны",
            "name" => "Контекстные объявления",
            "new" => "Добавить новое",
            "my" => "Мои объявления"
        ]
    ]
];
