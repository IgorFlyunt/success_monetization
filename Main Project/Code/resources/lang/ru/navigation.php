<?php

return [
    "dashboard" => "Главная",
    "categories" => "Рекламные рубрики",
    "groups" => "Рекламные группы",
    "my groups" => "Мои группы",
    "create group" => "Создать группу",
    "contexts" => "Контекстные объявления",
    "my contexts" => "Мои объявления",
    "create context" => "Создать объявление",
    "my templates" => "Мои шаблоны",
    "create template" => "Создать шаблонное",
    "sites" => "Сайты",
    "my sites" => "Мои сайты",
    "create site" => "Создать сайт",
    "trackers" => "Трекеры",
    "trash" => "Корзина",
    "advertisements" => "Реклама"
];
