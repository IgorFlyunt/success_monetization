<?php
    return [
        'add_new_help' => '+ Добавить',
        'delete_help' => 'Удалить',
        'edit_help' => 'Редактировать',
        'question' => 'Вопрос',
        'question_answer' => 'Ответ на вопросы...',
    ];