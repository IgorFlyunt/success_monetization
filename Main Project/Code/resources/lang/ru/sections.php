<?php

return [
    "CategoriesSection" => [
        "index" => [
            "title" => "Категории объявлений"
        ]
    ],
    "Advertisements" => [
        "ContextsSection" => [
            "index" => [
                "title" => "Контекстные объявления"
            ],
            "create" => [
                "title" => "Создание контекстного объявления"
            ],
            "edit" => [
                "title" => "Редактирование контекстного объявления"
            ],
            "editTemplate" => [
                "title" => "Редактирование шаблона контекстного объявления"
            ],
            "showCopy" => [
                "title" => "Копирование контекстного объявления"
            ],
            "copyTemplate" => [
                "title" => "Копирование шаблона контекстного объявления"
            ],
            "showTemplates" => [
                "title" => "Шаблоны контекстных объявлений"
            ],
            "createTemplate" => [
                "title" => "Создание шаблонного контекстного объявления"
            ]
        ]
    ],
    "GroupsSection" => [
        "index" => [
            "title" => "Рекламные группы"
        ],
        "create" => [
            "title" => "Создание рекламной группы"
        ],
        "edit" => [
            "title" => "Редактирование рекламной группы"
        ]
    ],
    "TrackersSection" => [
        "index" => [
            "title" => "Трекеры"
        ]
    ],
    "SitesSection" => [
        "index" => [
            "title" => "Сайты"
        ],
        "create" => [
            "title" => "Создание сайта"
        ],
        "edit" => [
            "title" => "Редактирование сайта"
        ]
    ],
    "Site" => [
        "SynchronizationSection" => [
            "index" => [
                "title" => "Сайт \"%s\""
            ]
        ],
        "Advertisements" => [
            "ContextsSection" => [
                "index" => [
                    "title" => "Контекстные объявления сайта \"%s\""
                ],
                "create" => [
                    "title" => "Добавить контекстное объявление к сайту \"%s\""
                ],
                "edit" => [
                    "title" => "Редактировать контекстное объявление сайта \"%s\""
                ]
            ]
        ]
    ],
    "Trash" => [
        "AdvertisementsSection" => [
            "index" => [
                "title" => "Рекламные объявления"
            ]
        ],
        "SitesSection" => [
            "index" => [
                "title" => "Сайты"
            ]
        ]
    ]
];
