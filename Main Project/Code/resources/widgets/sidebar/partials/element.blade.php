<li class="@if($element["active"]) active @endif @if(key_exists("elements", $element)) treeview @endif">
    <a @if(key_exists("url", $element) && $element["url"] != url()->current()) href="{{ $element["url"] }}" @endif>
        <i class="{{ $element["icon"] }}"></i>
        <span>
            {{ $element["title"] }}
        </span>
        @if(key_exists("elements", $element))
            <i class="fa fa-angle-left pull-right"></i>
        @endif
    </a>
    @if(key_exists("elements", $element))
        <ul class="treeview-menu">
            @foreach($element["elements"] as $child)
                @include("widgets::sidebar.partials.element", ["element" => $child])
            @endforeach
        </ul>
    @endif
</li>
