<aside class="main-sidebar">
    <section class="sidebar">

        <div class="user-panel success-monetization-user">
            <div class="pull-left image">
                <img src="{{ asset(env('THEME_FRONTEND')) }}/assets/img/avatar.png"
                     class="img-circle"
                     alt="{{ $user->name }}" />
            </div>
            <div class="pull-left info">
                <p class="margin-0">
                    {{ $user->name }}
                </p>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header">МЕНЮ</li>
            @foreach($elements as $element)
                @include("widgets::sidebar.partials.element", ["element" => $element])
            @endforeach
        </ul>

    </section>
</aside>
