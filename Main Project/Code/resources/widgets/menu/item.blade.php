<li @if($element->isActive()) class="active" @endif>
    <a @if($element->hasChildren()) class="menu-toggle" @endif @if(!$element->isActive(false) && $element->getUrl()) href="{!! $element->getUrl() !!}" @endif>
        @if(($icon = $element->getIcon()))
            <i class="material-icons">{{$icon}}</i>
        @endif
        <span>{{$element->getTitle()}}</span>
    </a>
    @if($element->hasChildren())
    <ul class="ml-menu">
        @foreach($element->getChildren() as $child)
            {!! $child !!}
        @endforeach
    </ul>
    @endif
</li>
