<div class="menu">
    <ul class="list">
        @foreach($menu->where("position", "default") as $element)
            {!! $element !!}
        @endforeach
        <folders-component></folders-component>
        @foreach($menu->where("position", "footer") as $element)
                {!! $element !!}
        @endforeach
    </ul>
</div>
