<datatable-component
    url="{{ $url }}"
    alias="{{ $alias }}"
    :columns="{{ json_encode($columns) }}"
    @isset($id) id="{{ $id }}" @endisset
    @isset($decorators) :decorators="{{ json_encode($decorators) }}" @endisset
    @isset($identifier) identifier="{{ $identifier }}" @endisset
    @isset($actions) :actions="{{ json_encode($actions) }}" @endisset
    @isset($list_actions) :list_actions="{{ json_encode($list_actions) }}" @endisset
    @isset($listeners) :listeners="{{ json_encode($listeners) }}" @endisset>
</datatable-component>

@push("styles")
<link href="{{ mix("themes/success-monetization/assets/css/datatables.css") }}" rel="stylesheet" type="text/css" />
@endpush
@push("scripts")
<script src="{{ mix('themes/success-monetization/assets/js/datatables.js') }}" type="text/javascript"></script>
@endpush
