<div class="body table-responsive">
    <table class="table table-bordered table-striped table-hover dataTable">
        <thead>
        <tr>
            @foreach($columns as $column => $title)
          <th @if(key_exists($column, $calculate_columns)) data-calculate="{{$calculate_columns[$column]}}" @endif
                @if($column === $id_column || $column === $action_column || in_array($column, $nosort)) class="nosort" @endif>
                    @if($id_column === $column && key_exists($column, $replacers))
                        <input type="checkbox" id="id-column-cheker"/>
                        <label class="margin-0" for="id-column-cheker">{!!trans("translations.$title")!!}</label>
                    @elseif($title)
                        {!!trans("translations.$title")!!}
                    @endif
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($collection as $element)
            <tr>
                @foreach($columns as $column => $title)
                    @php($class = $column === $id_column ? "td-min-width" : ($column == $action_column ? "align-center" : "align-middle"))
                    <td class="{{$class}}">
                        @if(key_exists($column, $replacers))
                            @if(is_callable($replacers[$column]))
                                {!! $replacers[$column]($element, $column) !!}
                            @else
                                {!! $replacers[$column] !!}
                            @endif
                        @else
                            {{$element->$column}}
                        @endif
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
        @if(sizeof($calculate_columns))
            <tfoot>
            <tr>
                @php($iterator = 0)
                @foreach($columns as $title)
                    <td>
                        <b>
                            @if($iterator === 0) {{trans("translations.$calculate_columns_title")}} @endif
                        </b>
                    </td>
                    @php($iterator++)
                @endforeach
            </tr>
            </tfoot>
        @endif
    </table>
</div>
@if($footer["action_url"] && sizeof($footer["actions"]))
    <table-footer-component
            :url="'{{$footer["action_url"]}}'"
            :actions="{{collect($footer["actions"])}}"
            :deletable="{{collect($deletable_actions)}}"
    >

    </table-footer-component>
@endif
