<?php

return [
    [
        "icon" => "fa fa-dashboard",
        "title" => "dashboard",
        "route" => "dashboard"
    ],
    [
        "icon" => "fa fa-list-ul",
        "title" => "categories",
        "route" => ["resolver.index", "categories"]
    ],
    [
        "title" => "groups",
        "elements" => [
            [
                "title" => "my groups",
                "route" => ["resolver.index", "groups"],
                "matcher" => "groups"
            ],
            [
                "title" => "create group",
                "route" => ["resolver.create", "groups"]
            ]
        ]
    ],
    [
        "title" => "contexts",
        "elements" => [
            [
                "title" => "my contexts",
                "route" => ["resolver.index", "contexts"],
                "matcher" => "contexts"
            ],
            [
                "title" => "create context",
                "route" => ["resolver.create", "contexts"]
            ],
            [
                "title" => "my templates",
                "route" => "contexts.templates.index",
                "matcher" => "contexts/templates(?:/create)?"
            ],
            [
                "title" => "create template",
                "route" => "contexts.templates.create"
            ]
        ]
    ],
    [
        "icon" => "fa fa-diamond",
        "title" => "sites",
        "elements" => [
            [
                "title" => "my sites",
                "route" => ["resolver.index", "sites"],
                "matcher" => "sites"
            ],
            [
                "title" => "create site",
                "route" => ["resolver.create", "sites"]
            ]
        ]
    ],
    [
        "icon" => "fa fa-pie-chart",
        "title" => "trackers",
        "route" => ["resolver.index", "trackers"]
    ],
    [
        "icon" => "fa fa-trash",
        "title" => "trash",
        "elements" => [
            [
                "title" => "sites",
                "route" => ["trash.index", "sites"]
            ],
            [
                "title" => "advertisements",
                "route" => ["trash.index", "advertisements"]
            ]
        ]
    ]
];
