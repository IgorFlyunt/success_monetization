<?php

return [
    "types" => [
        "context",
        "teaser",
        "banner",
        "popup",
        "other"
    ],
    "positions" => [
        "context" => [
            "top" => 1, //on top
            "h1" => 2, //after h1
            "h2" => 3, //after h2
            "p" => 4, //after p
            "h3" => 5, //after h3
            "block" => 6, //after user block
            "bottom" => 7 //on bottom
        ]
    ]
];
