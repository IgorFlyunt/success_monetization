<?php

return [
    [
        "icon" => "fa fa-refresh",
        "title" => "synchronization",
        "route" => "site.index"
    ],
    [
        "icon" => "fa fa-folder",
        "title" => "advertisements.context.name",
        "elements" => [
            [
                "title" => "advertisements.context.my",
                "route" => ["site.advertisements.index", ["contexts"]],
                "matcher" => "site/\d+/contexts"
            ],
            [
                "title" => "advertisements.context.new",
                "route" => ["site.advertisements.create", ["contexts"]]
            ]
        ]
    ],
    [
        "icon" => "fa fa-diamond",
        "title" => "admin system",
        "route" => "dashboard",
        "route_parameter" => false
    ],
];
