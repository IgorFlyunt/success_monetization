<?php

return [

    "default_periodicity" => "handle",

    "periodicities" => [
        "handle",
        "daily",
        "weekly",
        "monthly"
    ],

    "query_intervals" => [
        "daily" => "1 DAY",
        "weekly" => "1 WEEK",
        "monthly" => "1 MONTH"
    ]

];
