<?php

return [

    "namespace" => "App\\Widgets",

    "blade_directive" => "widget",

    "views" => resource_path("widgets")

];
